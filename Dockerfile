# STAGE BUILD PROJECT
FROM node:14-alpine AS builder
RUN apk --no-cache add ca-certificates

# Install global modules
WORKDIR /usr/src/app
WORKDIR /tmp/wealth

# Install production dependencies
COPY package*.json ./
RUN npm install --production

# Copy production packages to working dir
RUN cp -r /tmp/wealth/* /usr/src/app

# Install dev dependencies
RUN npm install 

# Copy sourcecode for building
COPY . .
RUN npm run build

# Copy built contents
RUN mkdir /usr/src/app/build && mv ./build/* /usr/src/app/build

# Copy serve.js script for bootstraping express server
RUN mkdir /usr/src/app/scripts && cp ./scripts/* /usr/src/app/scripts

# Clean building stuff
WORKDIR /usr/src/app

# STAGE FINAL IMAGE
FROM node:14-alpine 
RUN apk --no-cache add ca-certificates
WORKDIR /usr/src/app

RUN npm i -g pm2

COPY --from=builder /usr/src/app .

RUN ls -l /usr/src/app

# DONE
EXPOSE 3000/tcp
CMD ["pm2-runtime", "./scripts/serve.js"]
