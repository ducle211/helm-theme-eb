import React from 'react';

import { PostAdd } from '@material-ui/icons';
import CmtHorizontal from '../../../../../@coremat/CmtNavigation/Horizontal';
import IntlMessages from '../../../../utils/IntlMessages';
import AppSidebar from '../../../../../../base/components/layout/app-sidebar';

const HeaderMenu = () => <AppSidebar />;

export default HeaderMenu;
