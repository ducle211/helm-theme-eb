import React from 'react';
import DefaultUserDropDown from '../../../../../base/components/layout/user-dropdown';

const UserDropDown = () => <DefaultUserDropDown />;

export default UserDropDown;
