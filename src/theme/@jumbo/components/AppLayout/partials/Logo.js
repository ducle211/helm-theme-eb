import React, { useMemo, useRef } from 'react';
import Hidden from '@material-ui/core/Hidden';
import { Box } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import CmtImage from '../../../../@coremat/CmtImage';
import useSetting from '../../../../../base/hooks/useSetting';
import useAccessToken from '../../../../../base/hooks/useAccessToken';

const LOGO_AFFILIATE = '/images/logo-affiliate.png';
const LOGO_GROUP = '/images/logo-group.png';
const LOGO_SYSTEM = '/images/logo-system.png';
const LOGO_SYSTEM_SYMBOL = '/images/logo-symbol.png';
const LOGO_JUMBO_WHITE = '/images/logo-white.png';
const LOGO_JUMBO_SYMBOL_WHITE = '/images/logo-white-symbol.png';

const Logo = ({
  color, ...props
}) => {
  const settings = useSetting();
  const accessTokenData = useAccessToken();
  const logoRef = useRef();

  const logoUrlData = useMemo(() => {
    const logoResult = {
      logoSymbol: color === 'white' ? LOGO_JUMBO_SYMBOL_WHITE : LOGO_SYSTEM_SYMBOL,
      logoSetting: '',
      logoFallback: '',
    };

    const getLogoFallback = logo => (color === 'white' ? LOGO_JUMBO_WHITE : logo);

    if (accessTokenData.affiliate) {
      logoResult.logoSetting = settings?.site_affiliate_logo || '';
      logoResult.logoFallback = getLogoFallback(LOGO_AFFILIATE);
    } else if (accessTokenData.group?.code) {
      logoResult.logoSetting = settings?.site_group_logo || '';
      logoResult.logoFallback = getLogoFallback(LOGO_GROUP);
    } else {
      logoResult.logoSetting = settings?.site_logo || '';
      logoResult.logoFallback = getLogoFallback(LOGO_SYSTEM);
    }
    return logoResult;
  }, [settings, accessTokenData]);

  const handleLogoFallback = logoFallback => {
    logoRef.current.onerror = null;
    logoRef.current.src = logoFallback;
  };

  return (
    <Box
      className="pointer"
      {...props}
    >
      <Hidden xsDown>
        <NavLink to="/">
          <CmtImage
            ref={logoRef}
            src={logoUrlData.logoSetting}
            alt="logo"
            height={40}
            onError={() => handleLogoFallback(logoUrlData.logoFallback)}
          />
        </NavLink>
      </Hidden>
      <Hidden smUp>
        <NavLink to="/">
          <CmtImage
            src={logoUrlData.logoSymbol}
            alt="logo"
            height={40}
          />
        </NavLink>
      </Hidden>
    </Box>
  );
};

export default Logo;
