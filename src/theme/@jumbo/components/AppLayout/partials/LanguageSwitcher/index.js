import React, { useContext, useState } from 'react';
import DefaultLanguageSwitcher from '../../../../../../base/components/layout/right-toolbar/language-switcher';

const LanguageSwitcher = () => <DefaultLanguageSwitcher />;

export default LanguageSwitcher;
