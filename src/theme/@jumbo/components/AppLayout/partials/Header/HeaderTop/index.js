import { Box, darken, fade, IconButton, Typography } from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import { RefreshSharp } from "@material-ui/icons";
import { makeStyles } from "@material-ui/styles";
import clsx from "clsx";
import React, { useCallback, useEffect } from "react";
import { useNotify, useRefresh, useTranslate } from "react-admin";
import { useDispatch, useSelector } from "react-redux";
import Announcement from "../../../../../../../base/components/layout/announcement";
import HeaderInfo from "../../../../../../../base/components/layout/headerInfo";
import { useAuthUser } from "../../../../../../../base/hooks";
import SidebarToggleHandler from "../../../../../../@coremat/CmtLayouts/Horizontal/SidebarToggleHandler";
import LanguageSwitcher from "../../LanguageSwitcher";
import Logo from "../../Logo";
import UserDropDown from "../../UserDropDown";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    minHeight: 10,
    padding: 0,
    "& .Cmt-appIcon": {
      color: theme.palette.text.secondary,
      "&:hover, &:focus": {
        color: darken(theme.palette.text.secondary, 0.2),
      },
      [theme.breakpoints.down("xs")]: {
        padding: 7,
      },
    },
  },
  langRoot: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: 5,
    paddingRight: 5,
    position: "relative",
    [theme.breakpoints.up("md")]: {
      paddingLeft: 15,
      paddingRight: 15,
    },
    "&:before": {
      content: '""',
      position: "absolute",
      left: 0,
      top: -3,
      zIndex: 1,
      height: 35,
      width: 1,
      backgroundColor: fade(theme.palette.common.dark, 0.15),
    },
  },
  searchIcon: {
    [theme.breakpoints.down("xs")]: {
      padding: 9,
    },
  },
  transactionWrapper: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
}));

const HeaderTop = () => {
  const classes = useStyles();
  const refresh = useRefresh();
  const notify = useNotify();
  const translate = useTranslate();
  const user = useAuthUser();
  const dispatch = useDispatch();

  const transactionStore = useSelector(
    ({ admin }) => admin?.resources.transaction?.data
  );
  const inboxStore = useSelector(({ admin }) => admin?.resources.inbox?.data);

  const handleRefresh = useCallback(() => {
    refresh();
    notify(translate("ra.notification.refreshed", "success"));
  }, [notify, refresh, translate]);

  return (
    <Toolbar className={classes.root}>
      <SidebarToggleHandler edge="start" color="inherit" aria-label="menu" />
      <Logo
        mr={{
          xs: 2,
          sm: 4,
          lg: 6,
          xl: 8,
        }}
        // color="white"
      />
      {/* <Hidden smDown>
        <GlobalSearchForm />
      </Hidden> */}
      <Box display="flex" alignItems="center" ml="auto">
        <HeaderInfo />
        {/* <Hidden mdUp>
          <SearchPopover
            iconClassName={clsx(classes.searchIcon, "Cmt-searchIcon")}
          />
        </Hidden>
        <AppsMenu /> */}
        <IconButton
          aria-label="Refresh"
          aria-haspopup="true"
          onClick={handleRefresh}
          color="default"
        >
          <RefreshSharp />
        </IconButton>
        {/* <HeaderNotifications /> */}
        <Box className={clsx(classes.langRoot, "Cmt-i18n-switch")}>
          <LanguageSwitcher />
        </Box>
        <UserDropDown />
      </Box>
    </Toolbar>
  );
};

export default HeaderTop;
