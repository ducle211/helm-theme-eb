import React, { useMemo } from 'react';
import { Box, makeStyles, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { useLocation } from 'react-router-dom';
import { useTranslate } from 'react-admin';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
  pageHeaderRoot: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  titleRoot: {
    flex: 1,
    fontSize: '20px',
    fontWeight: 'bold',
    lineHeight: '1.167',
    paddingRight: theme.spacing(1),
    [theme.breakpoints.up(960)]: {
      fontSize: '22px',
    },
    '& span': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      display: '-webkit-box',
      WebkitLineClamp: 1,
      WebkitBoxOrient: 'vertical',
    },
  },
}));

const PageHeader = ({
  breadcrumbComponent, children, ...rest
}) => {
  const classes = useStyles();
  const { pathname } = useLocation();
  const t = useTranslate();

  const specialRoutes = ['profile'];
  const defaultHeaderTitle = useMemo(() => {
    const paths = _.without(pathname?.split('/') || [], '');
    return paths?.length && specialRoutes.includes(paths[0])
      ? t(`ra.page.${paths[0]}`)
      : '';
  }, [pathname, specialRoutes]);

  return (
    <Box
      className={clsx(classes.pageHeaderRoot, 'page-header')}
      mt={{
        xs: 4,
        md: 5,
        lg: 6,
      }}
      mb={{
        xs: 4,
        md: 5,
        lg: 6,
      }}
      {...rest}
    >
      {!defaultHeaderTitle ? (
        <Typography
          component="div"
          variant="h1"
          id="react-admin-title"
          className={clsx(classes.titleRoot, 'title')}
        />
      ) : (
        <div className={clsx(classes.titleRoot, 'title')}>
          {defaultHeaderTitle}
        </div>
      )}

      <Box
        flex={1}
        pl={1}
        display="flex"
        flexDirection="row"
        justifyContent="flex-end"
      >
        {breadcrumbComponent}
      </Box>

      {children}
    </Box>
  );
};

export default PageHeader;
