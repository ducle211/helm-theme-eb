import React, { useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Box, Collapse, fade, IconButton } from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import { Alert } from "@material-ui/lab";
import CloseIcon from "@material-ui/icons/Close";
import { useRedirect, useNotify } from "react-admin";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Field, Form } from "react-final-form";
import ContentLoader from "../../ContentLoader";
import CmtImage from "../../../../@coremat/CmtImage";
import AuthWrapper from "./AuthWrapper";

const useStyles = makeStyles((theme) => ({
  authThumb: {
    backgroundColor: fade(theme.palette.primary.main, 0.12),
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    [theme.breakpoints.up("md")]: {
      width: "50%",
      order: 2,
    },
  },
  authContent: {
    fontFamily: theme.typography.fontFamily,
    padding: 30,
    [theme.breakpoints.up("md")]: {
      order: 1,
      width: (props) => (props.variant === "default" ? "50%" : "100%"),
    },
    [theme.breakpoints.up("xl")]: {
      padding: 50,
    },
  },
  titleRoot: {
    marginBottom: 14,
    color: theme.palette.text.primary,
  },
  alertRoot: {
    marginBottom: 10,
  },
}));

const ResetPassword = ({ variant = "default", wrapperVariant = "default" }) => {
  const [successOpen, setSuccessOpen] = useState(false);
  const [errorResponse, setErrorResponse] = useState("");
  const [counter, setCounter] = useState(10);
  const [tokenValid, setTokenValid] = useState(true);
  const classes = useStyles({
    variant,
  });
  const redirect = useRedirect();
  const notify = useNotify();
  const { token } = useParams();

  // Validate token
  const validateToken = async (token) => {
    try {
      await axios.post("api/auth/password/verify-token", {
        passwordToken: token,
      });
      setTokenValid(true);
    } catch (error) {
      if (!error?.response) {
        setTokenValid(false);
      }
      setTokenValid(false);
    }
  };
  useEffect(() => {
    if (!token) {
      notify("error");
    }
    validateToken(token);
  }, [token]);

  useEffect(() => {
    if (successOpen) {
      setCounter(counter - 1);
    }
  }, [successOpen]);

  useEffect(() => {
    if (counter === 0) {
      redirect("/");
    }
    const timer =
      successOpen &&
      counter > 0 &&
      setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter, successOpen]);

  const onSubmit = async ({ newPassword, confirmPassword }) => {
    try {
      await axios.post("api/auth/password/update", {
        newPassword,
        confirmPassword,
        passwordToken: token,
      });
      setSuccessOpen(true);
    } catch (error) {
      console.log("error :>> ", error);
      setErrorResponse(error);
    }
  };

  const handleValidation = (values) => {
    const { newPassword, confirmPassword } = values;
    const errors = {};
    if (newPassword?.length < 6) {
      errors.newPassword = "Must longer than 6 characters";
    }
    if (confirmPassword?.length < 6) {
      errors.confirmPassword = "Must longer than 6 characters";
    }
    if (!newPassword)
      if (!newPassword) {
        errors.newPassword = "This field is required";
      }
    if (!confirmPassword) {
      errors.confirmPassword = "This field is required";
    }
    if (confirmPassword !== newPassword) {
      errors.confirmPassword = "Confirm password must match new password";
    }
    return errors;
  };

  return (
    <AuthWrapper variant={wrapperVariant}>
      {variant === "default" ? (
        <Box className={classes.authThumb}>
          <CmtImage src="/images/auth/forgot-img.png" />
        </Box>
      ) : null}
      <Box className={classes.authContent}>
        <Box mb={2}>
          <CmtImage src="/images/logo-system.png" style={{ width: "100%" }} />
        </Box>
        <Typography component="div" variant="h4" className={classes.titleRoot}>
          Reset Password
        </Typography>
        <Collapse in={!successOpen && tokenValid && !errorResponse}>
          <Form
            onSubmit={onSubmit}
            validate={handleValidation}
            render={({ handleSubmit, saving, pristine, valid }) => (
              <form onSubmit={handleSubmit}>
                <Field name="newPassword" component={TextField}>
                  {({ input, meta }) => (
                    <TextField
                      {...input}
                      label="New password"
                      type="password"
                      fullWidth
                      margin="normal"
                      variant="outlined"
                      className={classes.textFieldRoot}
                      helperText={meta.error && meta.touched && meta.error}
                      error={meta.error && meta.touched}
                    />
                  )}
                </Field>
                <Field name="confirmPassword" component={TextField}>
                  {({ input, meta }) => (
                    <TextField
                      {...input}
                      label="Confirm password"
                      type="password"
                      fullWidth
                      margin="normal"
                      variant="outlined"
                      className={classes.textFieldRoot}
                      helperText={meta.error && meta.touched && meta.error}
                      error={meta.error && meta.touched}
                    />
                  )}
                </Field>
                <Box mt={2.5}>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                    className={classes.btnLogin}
                    disabled={pristine || saving || !valid}
                  >
                    Save
                  </Button>
                </Box>
              </form>
            )}
          />
        </Collapse>
        <Box mt={2}>
          {/* Error Response alert*/}
          <Collapse in={errorResponse}>
            <Alert
              variant="outlined"
              severity="error"
              className={classes.alertRoot}
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    setErrorResponse("");
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
            >
              <Typography>{errorResponse.toString()}</Typography>
            </Alert>
          </Collapse>
          {/* Invalid token alert */}
          <Collapse in={!tokenValid}>
            <Alert
              variant="outlined"
              severity="error"
              className={classes.alertRoot}
            >
              <Typography>{`Sorry, this reset link is invalid or expired, please contact your admin for help.`}</Typography>
            </Alert>
          </Collapse>
          {/* Success alert */}
          <Collapse in={successOpen}>
            <Alert
              variant="outlined"
              severity="success"
              className={classes.alertRoot}
              action={
                <IconButton
                  aria-label="close"
                  color="inherit"
                  size="small"
                  onClick={() => {
                    redirect("/");
                  }}
                >
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              }
            >
              <Typography>
                {`Password has been reset, redirect to login page in ${counter}`}
              </Typography>
            </Alert>
          </Collapse>
        </Box>
        <ContentLoader />
      </Box>
    </AuthWrapper>
  );
};

export default ResetPassword;
