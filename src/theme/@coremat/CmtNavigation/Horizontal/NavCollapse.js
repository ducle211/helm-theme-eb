import { Badge, List, ListItem } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import clsx from "clsx";
import React, {
  cloneElement,
  isValidElement,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
} from "react";
import { useHistory } from "react-router-dom";
import useStyles from "./NavCollapse.style";
import NavMenuItem from "./NavMenuItem";
import { useResizeDetector } from "react-resize-detector";

const NavCollapse = (props) => {
  const history = useHistory();
  const classes = useStyles();

  const { name, icon, className, children = [], count } = props;
  const isExpandable = useMemo(() => children.length, [children]);
  const [open, setOpen] = React.useState(false);
  const { width: containerWidth, ref: containerRef } = useResizeDetector();

  useEffect(() => {
    if (isUrlInChildren(props, history.location.pathname)) {
      setOpen(true);
    } else {
      setOpen(false);
    }

    history.listen((location, action) => {
      if (isUrlInChildren(props, location.pathname)) {
        setOpen(true);
      } else {
        setOpen(false);
      }
    });
  }, []);

  // check and update position of popup if needed
  const updateNavCollapseItemPosition = () => {
    try {
      if (isExpandable) {
        const element = containerRef.current;
        const { width: bodyWidth } = document.body.getBoundingClientRect();
        const { x: navOffsetX } = element.getBoundingClientRect();

        // should align to left side to prevent pane is clipped by browser
        const navCollapedItemContainerElement =
          element.children[0].children[0].children[1];
        const isRight = navOffsetX + 300 > bodyWidth;
        const removedClassName = isRight
          ? "absolute-left-align"
          : "absolute-right-align";
        const addedClassName = isRight
          ? "absolute-right-align"
          : "absolute-left-align";
        navCollapedItemContainerElement.classList.remove(removedClassName);
        navCollapedItemContainerElement.classList.add(addedClassName);
      }
    } catch (ex) {
      console.error(ex);
    }
  };

  useLayoutEffect(() => {
    updateNavCollapseItemPosition();
  }, [isExpandable, containerWidth]);

  useLayoutEffect(() => {
    window.addEventListener("resize", updateNavCollapseItemPosition);
    return () =>
      window.removeEventListener("resize", updateNavCollapseItemPosition);
  }, []);

  /**
   * Check if the given url can be found
   * in one of the given parent's children
   *
   * @param parent
   * @param url
   * @returns {boolean}
   */
  function isUrlInChildren(parent, url) {
    if (!parent.children) {
      return false;
    }

    for (let i = 0; i < parent.children.length; i++) {
      if (parent.children[i].children) {
        if (isUrlInChildren(parent.children[i], url)) {
          return true;
        }
      }

      if (
        parent.children[i].link === url ||
        url.includes(parent.children[i].link)
      ) {
        return true;
      }
    }

    return false;
  }

  const renderIcon = () => {
    if (icon && isValidElement(icon)) {
      return cloneElement(icon, {
        className: clsx(classes.iconRoot, "Cmt-icon-root"),
      });
    }

    return null;
  };

  const renderBadge = () => {
    if (count > 0) {
      return (
        <Box ml={-1} mt={-4} mr={2}>
          <Badge color="secondary" variant="dot" />
        </Box>
      );
    }
    return null;
  };

  const MenuItemChildren = isExpandable ? (
    <List component="div" disablePadding className={classes.navCollapseItem}>
      {children.map((item, index) => {
        switch (item.type) {
          case "collapse":
            return (
              <NavCollapse
                {...item}
                key={index}
                className={classes.subCollapse}
              />
            );
          case "item":
            return <NavMenuItem {...item} key={index} />;
          default:
            return null;
        }
      })}
    </List>
  ) : null;

  const MenuCollapse = (
    <div>
      <ListItem
        component="div"
        disableGutters
        className={clsx(
          classes.navCollapseBtn,
          "Cmt-navCollapseBtn",
          `${open ? "active" : ""}`
        )}
      >
        <Box
          component="span"
          className={classes.navCollapseBtnInner}
          onClick={() => history.push(children?.[0]?.link || "/")}
        >
          {renderIcon()}
          {renderBadge()}
          <Box component="span" className={classes.navText}>
            {name}
          </Box>
          {/* Display the expand menu if the item has children */}
          {/* {isExpandable && !open && <ArrowDropDownIcon className={classes.navArrow} />}
        {isExpandable && open && <ArrowDropUpIcon className={classes.navArrow} />} */}
          {/* Display an icon if any */}
        </Box>
        {MenuItemChildren}
      </ListItem>
    </div>
  );

  return (
    <div
      className={clsx(
        classes.navCollapse,
        "Cmt-navCollapse",
        className,
        `${open ? "active" : ""}`
      )}
      ref={containerRef}
    >
      {MenuCollapse}
    </div>
  );
};

export default NavCollapse;
