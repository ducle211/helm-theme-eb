import React from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';

const breadcrumbs = [
  {
    label: 'Home',
    link: '/',
  },
  {
    label: 'Sample Page',
    isActive: true,
  },
];

const SamplePage = () => (
  <PageContainer
    heading={<IntlMessages id="pages.samplePage" />}
    breadcrumbs={breadcrumbs}
  >
    <GridContainer>
      <Grid
        item
        xs={12}
      >
        <Box>
          <IntlMessages id="pages.samplePage.description" />
        </Box>
      </Grid>
    </GridContainer>
  </PageContainer>
);

export default SamplePage;
