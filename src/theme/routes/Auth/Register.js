import React from "react";
import SignUp from "../../@jumbo/components/Common/authComponents/SignUp";

const Register = () => <SignUp variant="outlined" wrapperVariant="bgColor" />;

export default Register;
