import React from "react";
import SignIn from "../../@jumbo/components/Common/authComponents/SignIn";

const Login = () => <SignIn variant="outlined" wrapperVariant="bgColor" />;

export default Login;
