/* eslint-disable import/no-dynamic-require, global-require, no-console, no-lone-blocks, no-unused-vars */
import axios from 'axios';
import * as React from 'react';
import { Resource } from 'react-admin';
import authProvider from './services/provider/authProvider';
import WealthCreateGuesser from './base/components/guesser/wealth-create.guesser';
import WealthEditGuesser from './base/components/guesser/wealth-edit.guesser';
import WealthListGuesser from './base/components/guesser/wealth-list.guesser';
import WealthShowGuesser from './base/components/guesser/wealth-show.guesser';
import Layout from './theme/App';
import LoginPage from './base/components/layout/loginPage';
import { customRoutes, publicCustomRoutes } from './routes/customRoutes';
import Dashboard from './routes/dashboard/index';
import dataProvider from './services/provider/dataProvider';
import { i18nProvider } from './services/provider/i18n/i18nProvider';
import { canI, RbacAction } from './services/provider/rbacProvider';
import rootReducer from './services/redux/root.reducer';
import { rootSagas } from './services/redux/root.sagas';
import theme from './theme';
import CustomCoreAdmin from './custom-core-admin/CustomCoreAdmin';
import { formatResources } from './services/util';

const fetchResources = async permissions => {
  let resources = [];

  try {
    const permittedResource = await axios.get('api/resource/permitted');
    resources = formatResources(permittedResource?.data);
  } catch (error) {
    // Do nothing!
  }

  const noCustomComponents = [];

  const resourceComponents = resources.map(
    ({
      id,
      name,
      list,
      update,
      read,
      icon,
      create,
      delete: hasDelete,
      group: isGroup,
      children,
      submittable,
      approvable,
      cancelAfterApproval,
      isChild,
      prefix,
      reportInterface,
      translatable,
      index,
    }) => {
      let resourceLoad;
      try {
        resourceLoad = require(`./routes/${name}`).default;
      } catch {
        noCustomComponents.push(name);
      }
      return (
        <Resource
          name={name}
          icon={icon && (() => icon)}
          list={
            list
            && canI(RbacAction.READ, name, permissions)
            && (resourceLoad?.list || WealthListGuesser)
          }
          edit={
            update
            && canI(RbacAction.UPDATE, name, permissions)
            && (resourceLoad?.edit || WealthEditGuesser)
          }
          create={
            create
            && canI(RbacAction.CREATE, name, permissions)
            && (resourceLoad?.create || WealthCreateGuesser)
          }
          show={
            read
            && canI(RbacAction.READ, name, permissions)
            && (resourceLoad?.show || WealthShowGuesser)
          }
          options={{
            hasDelete: hasDelete && canI(RbacAction.DELETE, name, permissions),
            isGroup,
            resources,
            prefix,
            children,
            isChild,
            submittable:
              submittable && canI(RbacAction.SUBMIT, name, permissions),
            approvable:
              approvable && canI(RbacAction.APPROVE, name, permissions),
            cancelAfterApproval,
            resourceId: id,
            reportInterface,
            translatable,
            index,
          }}
        />
      );
    },
  );
  console.warn(
    `The following resource got no Customized Components ${noCustomComponents}`,
  );
  return resourceComponents;
};

const App = () => (
  <CustomCoreAdmin
    title="Gaming Backoffice"
    dashboard={Dashboard}
    dataProvider={dataProvider}
    authProvider={authProvider}
    i18nProvider={i18nProvider}
    customReducers={rootReducer}
    customSagas={rootSagas}
    customRoutes={customRoutes}
    layout={Layout}
    theme={theme}
    loginPage={LoginPage}
    publicCustomRoutes={publicCustomRoutes}
  >
    {fetchResources}
  </CustomCoreAdmin>
);

export default App;
