/* eslint-disable  */
import { required } from "ra-core";
import { TextInput } from "ra-ui-materialui";
import React, { useEffect, useState } from "react";
import { SelectInput } from "../../../base/components/ra/inputs";
import { useAuthUser, useEnumOptions } from "../../../base/hooks";
import { useForm, useFormState } from "react-final-form";

export const RoleLevelInput = ({ resource, source, isEdit, record }) => {
  const user = useAuthUser();
  const enumRoleLevel = useEnumOptions(resource, source);
  const [roleLevel, setRoleLevel] = useState([]);

  const form = useForm();
  const { values } = useFormState();

  useEffect(() => {
    const isTypeGroup = values.type === "GROUP";
    const tmpRoleLevel = (enumRoleLevel || [])
      .filter((level) =>
        record?.level === 10 ? level.id : Number(level.id) < 10
      )
      .filter((level) => Number(level.id) <= Number(user.role?.level))
      .filter((level) =>
        values.type === "GROUP" ? Number(level.id) < 6 : Number(level.id) > 5
      );
    setRoleLevel(tmpRoleLevel);
  }, [values.type, enumRoleLevel, user]);

  useEffect(() => {
    if (isEdit) {
      if (record.type === values.type) {
        return form.change("level", record.level);
      }
      return form.change("level", values.type === "GROUP" ? 1 : 6);
    }

    return form.change("level", values.type === "GROUP" ? 1 : 6);
  }, [values.type, isEdit]);

  return (
    <SelectInput
      fullWidth
      source="level"
      choices={roleLevel}
      defaultValue={roleLevel[0]?.id}
      validate={required()}
    />
  );
};

export const RoleTypeInput = ({ resource, source, isEdit }) => {
  const user = useAuthUser();
  const enumRoleType = useEnumOptions(resource, source);
  const [roleType, setRoleType] = useState([]);

  useEffect(() => {
    const tmpRoleType = (enumRoleType || []).filter((type) =>
      user.role?.type === "SYSTEM" ? type.id : type.id !== "SYSTEM"
    );

    setRoleType(tmpRoleType);
  }, [user.role?.type, enumRoleType]);

  return (
    <SelectInput
      fullWidth
      source="type"
      choices={roleType}
      defaultValue={roleType?.[0]?.id}
      validate={required()}
    />
  );
};
