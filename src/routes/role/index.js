import RoleEdit from './role.edit';
import RoleCreate from './role.create';

export default {
  edit: RoleEdit,
  create: RoleCreate,
};
