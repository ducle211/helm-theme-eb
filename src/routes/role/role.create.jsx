/* eslint-disable  */
import { required } from "ra-core";
import { SimpleForm } from "react-admin";
import React from "react";
import {
  BooleanInput,
  SelectInput,
  TextInput,
} from "../../base/components/ra/inputs";
import { Create } from "../../base/components/ra/views";
import { RoleTypeInput, RoleLevelInput } from "./components";
import { useEnumOptions } from "../../base/hooks";

const RoleCreate = (props) => {
  const transform = (data) => {
    const { level, name, enabled, type } = data;

    return {
      level: Number(level),
      name,
      enabled,
      type,
    };
  };

  return (
    <Create {...props} transform={transform}>
      <SimpleForm>
        <TextInput validate={required()} fullWidth source="name" />
        <RoleTypeInput source="type" />
        <RoleLevelInput source="level" />
        <BooleanInput fullWidth source="enabled" defaultValue={false} />
      </SimpleForm>
    </Create>
  );
};

export default RoleCreate;
