/* eslint-disable  */
import { required } from "ra-core";
import { SimpleForm } from "react-admin";
import React from "react";
import {
  BooleanInput,
  SelectInput,
  TextInput,
} from "../../base/components/ra/inputs";
import { Edit } from "../../base/components/ra/views";
import { RoleTypeInput, RoleLevelInput } from "./components";
import { useEnumOptions } from "../../base/hooks";

const RoleEdit = (props) => {
  const { resource, id } = props;

  const transform = (data) => {
    const { level, name, enabled, type } = data;

    return {
      level: Number(level),
      name,
      enabled,
      type,
    };
  };

  return (
    <Edit {...props} transform={transform}>
      <SimpleForm redirect={`/${resource}/${id}/show`}>
        <TextInput validate={required()} fullWidth source="name" />
        <RoleTypeInput source="type" isEdit />
        <RoleLevelInput source="level" isEdit />
        <BooleanInput fullWidth source="enabled" defaultValue={false} />
      </SimpleForm>
    </Edit>
  );
};

export default RoleEdit;
