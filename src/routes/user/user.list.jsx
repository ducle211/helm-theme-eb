/* eslint-disable react/prop-types */
import * as React from 'react';
import PropTypes from 'prop-types';
import WealthListGuesser from '../../base/components/guesser/wealth-list.guesser';
import ResetPasswordButton from './components/resetPassword.button';
import ForceChangePasswordButton from './components/forceChangePassword.button';
import { useAuthUser } from '../../base/hooks';

function ExtraActions(props) {
  const { record } = props;
  const { id: userId = '' } = useAuthUser();

  if (!userId || !record?.id || userId === record.id) {
    return null;
  }

  return (
    <>
      <ResetPasswordButton {...props} />
      <ForceChangePasswordButton {...props} />
    </>
  );
}

ExtraActions.propTypes = {
  record: PropTypes.object.isRequired,
};

function UserList(props) {
  return (
    <>
      <WealthListGuesser
        {...props}
        extraActions={<ExtraActions />}
      />
    </>
  );
}

export default UserList;
