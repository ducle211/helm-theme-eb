import PermCreate from './perm.create';
import PermEdit from './perm.edit';

export default {
  create: PermCreate,
  edit: PermEdit,
};
