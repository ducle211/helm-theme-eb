/* eslint-disable  */
import React, { useEffect, useState } from "react";
import { useTranslate, FormDataConsumer } from "ra-core";
import { useNotify, useUpdate } from "react-admin";
import { BooleanInput } from "../../base/components/ra/inputs";
import {
  makeStyles,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import useError from "../../base/hooks/useError";
import WealthEditGuesser from "../../base/components/guesser/wealth-edit.guesser";
import ConfirmationDialog from "./components/confirm-dialog";

const useStyles = makeStyles((theme) => ({
  noticeDisableAllBrands: {
    color: theme.palette.error.main,
  },
}));

const RESOURCE_NAME = "group";

const GroupEdit = (props) => {
  const translate = useTranslate();
  const classes = useStyles();
  const notify = useNotify();
  const history = useHistory();
  const { notifyError } = useError();

  const [isDisableBrandsDialog, setDisableBrandsDialog] = useState(false);
  const [isAgreeDisableAllBrands, setAgreeDisableAllBrands] = useState(false);
  const [isEnableConfirm, setEnableConfirm] = useState(false);

  const onUpdateSuccess = (response) => {
    const { data } = response;
    const groupId = data?.id;

    const translatedGroupName = translate('resources.group.name');
    notify("ra.notification.updated", "info", {
      smart_name: translatedGroupName,
     });

    if (groupId) {
      history.push(`/${RESOURCE_NAME}/${groupId}/show`);
    }
  };

  const [update, { loading: updateLoading, error }] = useUpdate(
    undefined,
    undefined,
    undefined,
    undefined,
    {
      onSuccess: onUpdateSuccess,
    }
  );

  const saveGroupData = (values) => {
    const { enabled, managerEmail, name, desc, id } = values;
    const payload = {
      id,
      data: {
        enabled,
        managerEmail,
        name,
        desc,
      },
    };

    if (!enabled && isAgreeDisableAllBrands) {
      payload.data = {
        ...payload.data,
        disableBrands: true,
      };
    }

    update({ resource: RESOURCE_NAME, payload });
  };

  useEffect(() => {
    if (!updateLoading && error) {
      notifyError(error);
    }
  }, [updateLoading, error]);

  return (
    <>
      <WealthEditGuesser
        {...props}
        excludeFields={["enabled"]}
        onCustomSave={saveGroupData}
      >
        <FormDataConsumer>
          {({ formData }) => (
            <>
              <BooleanInput
                label="resources.group.fields.enabled"
                source="enabled"
                onChange={(isEnabled) => {
                  if (isEnabled === false) {
                    setDisableBrandsDialog(true);
                  } else {
                    setAgreeDisableAllBrands(false);
                  }
                }}
              />
              {!formData.enabled && isAgreeDisableAllBrands && (
                <p className={classes.noticeDisableAllBrands}>
                  {translate("ra.text.disableGroupNoticeText")}
                </p>
              )}
            </>
          )}
        </FormDataConsumer>
      </WealthEditGuesser>
      {isDisableBrandsDialog ? (
        <ConfirmationDialog
          open={true}
          onDeny={() => {
            setAgreeDisableAllBrands(false);
            setDisableBrandsDialog(false);
          }}
          onConfirm={() => {
            setAgreeDisableAllBrands(true);
            setDisableBrandsDialog(false);
          }}
          disableConfirm={!isEnableConfirm}
          title={
            <FormGroup>
              <FormControlLabel
                label={translate("ra.text.disableGroupCheckboxLabel")}
                control={
                  <Checkbox
                    onChange={(event) => {
                      setEnableConfirm(event.target.checked);
                    }}
                  />
                }
              />
            </FormGroup>
          }
          dialogTitle={translate("ra.text.disableGroupDialogTitle")}
        />
      ) : null}
    </>
  );
};

export default GroupEdit;
