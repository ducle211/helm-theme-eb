/* eslint-disable no-bitwise, no-nested-ternary, react/prop-types */
import React from 'react';
import { Area, AreaChart, ResponsiveContainer } from 'recharts';

const statisticsGraphData = [
  {
    month: 'Jan',
    price: 200,
  },
  {
    month: 'Feb',
    price: 300,
  },
  {
    month: 'Mar',
    price: 550,
  },
  {
    month: 'Apr',
    price: 500,
  },
  {
    month: 'May',
    price: 700,
  },
  {
    month: 'Jun',
    price: 450,
  },
  {
    month: 'Jul',
    price: 770,
  },
  {
    month: 'Aug',
    price: 900,
  },
];

function lightenDarkenColor(color, percent) {
  const num = parseInt(color.replace('#', ''), 16);
  const amt = Math.round(2.55 * percent);
  const R = (num >> 16) + amt;
  const B = ((num >> 8) & 0x00ff) + amt;
  const G = (num & 0x0000ff) + amt;
  return `#${(
    0x1000000
    + (R < 255 ? (R < 1 ? 0 : R) : 255) * 0x10000
    + (B < 255 ? (B < 1 ? 0 : B) : 255) * 0x100
    + (G < 255 ? (G < 1 ? 0 : G) : 255)
  )
    .toString(16)
    .slice(1)}`;
}

const PropertiesGraph = ({ fill }) => (
  <ResponsiveContainer
    className="card-img-bottom overflow-hidden"
    width="100%"
    height={95}
  >
    <AreaChart
      data={statisticsGraphData}
      margin={{
        top: 0,
        right: 0,
        left: 0,
        bottom: 0,
      }}
    >
      <Area
        dataKey="price"
        strokeWidth={2}
        stroke={lightenDarkenColor(fill, 5)}
        fill={lightenDarkenColor(fill, -5)}
        fillOpacity={1}
      />
    </AreaChart>
  </ResponsiveContainer>
);

export default PropertiesGraph;
