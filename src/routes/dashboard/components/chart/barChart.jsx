import React from 'react';
import PropTypes from 'prop-types';
import { Bar, BarChart, ResponsiveContainer, Tooltip, XAxis } from 'recharts';
import { makeStyles, Box } from '@material-ui/core';
import { useTheme } from '@material-ui/styles';
import CmtAdvCard from '../../../../theme/@coremat/CmtAdvCard';
import CmtCardHeader from '../../../../theme/@coremat/CmtCard/CmtCardHeader';
import CmtAdvCardContent from '../../../../theme/@coremat/CmtAdvCard/CmtAdvCardContent';

const useStyles = makeStyles(theme => ({
  tooltip: {
    position: 'relative',
    borderRadius: 6,
    padding: '4px 8px',
    backgroundColor: '#8d46ef',
    color: theme.palette.common.white,
  },
}));

const StatBarChart = ({
  title, data, xDataKey,
}) => {
  const classes = useStyles();
  const { palette: { mixedColors } } = useTheme();

  if (!data?.length) return null;

  // extract bars from props in an object except xDataKey prop.
  const bars = Object.keys(data[0])
    .filter(key => key !== xDataKey)
    .map((key, i) => ({
      dataKey: key,
      fill: mixedColors[i % mixedColors.length],
    }));

  return (
    <CmtAdvCard>
      <CmtCardHeader
        titleProps={{
          variant: 'h4',
          component: 'div',
        }}
        title={title}
      />
      <CmtAdvCardContent className={classes.cardContentRoot}>
        <ResponsiveContainer
          width="100%"
          height={56}
        >
          <BarChart
            data={data}
            margin={{
              top: 0,
              right: 0,
              left: 0,
              bottom: 0,
            }}
          >
            <Tooltip
              labelStyle={{
                color: 'black',
              }}
              cursor={false}
              content={item => (
                <Box className={classes.tooltip}>
                  {item?.payload[0]?.payload
                    && Object.keys(item.payload[0].payload)
                      .reverse()
                      .map(
                        key => key !== xDataKey && (
                          <Box key={key}>
                            {key}
                            {' '}
                            :
                            {item.payload[0].payload[key]}
                          </Box>
                        ),
                      )}
                </Box>
              )}
            />
            <XAxis
              dataKey={xDataKey}
              hide
            />
            {bars.map(bar => (
              <Bar
                key={bar}
                stackId="a"
                barSize={6}
                {...bar}
              />
            ))}
          </BarChart>
        </ResponsiveContainer>
      </CmtAdvCardContent>
    </CmtAdvCard>
  );
};
export default StatBarChart;

StatBarChart.defaultProps = {
  data: [],
  xDataKey: 'date',
};

StatBarChart.propTypes = {
  data: PropTypes.array,
  title: PropTypes.string.isRequired,
  xDataKey: PropTypes.string,
};
