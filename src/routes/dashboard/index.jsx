/* eslint-disable react/prop-types */
import React, { } from 'react';
import { useLocale } from 'react-admin';
import { Grid, Card, CardHeader, CardContent } from '@material-ui/core';
import moment from 'moment';
import GridContainer from '../../base/@crema/core/GridContainer';

function Dashboard() {
  const locale = useLocale();
  return (
    <GridContainer>
      <Grid
        item
        xs={12}
      >
        <Card>
          <CardHeader
            title="Chart"
            subheader={(
              <span
                style={{
                  fontSize: '12px',
                }}
              >
                {moment()
                  .locale(locale)
                  .format('LLLL')
                  .toString()}
              </span>
            )}
          />
          <CardContent>Graphs under construction...</CardContent>
        </Card>
      </Grid>
    </GridContainer>
  );
}

export default Dashboard;
