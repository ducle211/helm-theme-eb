import React from 'react';
import WealthShowGuesser from '../../base/components/guesser/wealth-show.guesser';
import { ActionMenu } from './components/action.menu';

export const CronShow = props => (
  <WealthShowGuesser
    {...props}
    actionMenu={<ActionMenu type="toolbar" />}
  />
);
