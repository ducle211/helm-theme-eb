import AccessTime from '@material-ui/icons/AccessTime';
import Axios from 'axios';
import Type from 'prop-types';
import { useNotify } from 'ra-core';
import { Button } from 'ra-ui-materialui';
import React from 'react';
import ToolbarMenu from '../../../base/components/action-menu/toolbar-menu';

export const ActionMenu = props => {
  const { record } = props;
  const notify = useNotify();

  const triggerEvent = () => {
    Axios.post(`api/cron/${record?.id}/trigger`)
      .then(res => {
        if (res.status === 201) {
          notify('Successfully', 'success');
        }
      })
      .catch(() => notify('Trigger failure', 'error'));
  };

  return (
    <ToolbarMenu {...props}>
      <Button
        variant="contained"
        color="secondary"
        label="ra.action.trigger"
        onClick={triggerEvent}
        startIcon={<AccessTime />}
      />
    </ToolbarMenu>
  );
};

ActionMenu.propTypes = {
  record: Type.object,
};

ActionMenu.defaultProps = {
  record: {},
};
