import { History } from '@material-ui/icons';
import Axios from 'axios';
import Type from 'prop-types';
import {
  sanitizeListRestProps,
  useListContext,
  useNotify,
  useRefresh,
} from 'ra-core';
import { Button, TopToolbar } from 'ra-ui-materialui';
import React, { cloneElement } from 'react';

export const ActionList = props => {
  const {
    filters, ...rest
  } = props;
  const {
    resource,
    filterValues,
    showFilter,
    displayedFilters,
  } = useListContext();
  const notify = useNotify();
  const refresh = useRefresh();

  const revertEvent = () => {
    Axios.post('api/cron/revert')
      .then(res => {
        if (res.status === 201) {
          notify('Successfully', 'success');
          refresh();
        }
      })
      .catch(() => notify('Revert failure', 'error'));
  };

  return (
    <TopToolbar {...sanitizeListRestProps(rest)}>
      {filters
        && cloneElement(filters, {
          resource,
          showFilter,
          displayedFilters,
          filterValues,
          context: 'button',
        })}
      <Button
        variant="contained"
        color="secondary"
        label="ra.action.revert"
        onClick={revertEvent}
        startIcon={<History />}
      />
    </TopToolbar>
  );
};

ActionList.propTypes = {
  filters: Type.node.isRequired,
};
