import CronList from './cron.list';
import { CronShow } from './cron.show';

export default {
  list: CronList,
  show: CronShow,
};
