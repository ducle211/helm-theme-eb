/* eslint-disable  */
import React, { useEffect, useState, useMemo } from "react";
import { useTranslate, FormDataConsumer } from "ra-core";
import {
  SimpleForm,
  useNotify,
  ReferenceInput,
  AutocompleteInput,
  useUpdate,
  useEditController,
  useLocale,
} from "react-admin";
import { makeStyles, Box } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import enhancedDataProvider from "../../services/provider/dataProvider";
import useError from "../../base/hooks/useError";
import ReferenceInputCustom from "../../base/components/ra/inputs/reference.input";
import { Edit } from "../../base/components/ra/views";
import TransferList from "./components/transfer-list";

const useStyles = makeStyles((theme) => ({
  contentWrap: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    justifyContent: "space-between",
  },
  leftContent: {
    flex: 1,
  },
  rightContent: {},
  raUpdateMain: {
    // maxWidth: 800,
    margin: "0 auto",
  },
  leftHeadClassName: {
    backgroundColor: "#fff5dc94",
  },
  leftContentClassName: {
    backgroundColor: "#fffcf394",
  },
  rightHeadClassName: {
    backgroundColor: "#9de89d",
  },
  rightContentClassName: {
    backgroundColor: "#c6f9d4",
  },
}));

const AvailableGameEdit = (props) => {
  const translate = useTranslate();
  const classes = useStyles();
  const notify = useNotify();
  const history = useHistory();
  const { notifyError } = useError();
  const { record } = useEditController(props);
  const locale = useLocale();

  const [allGamesData, setAllGamesData] = useState([]);
  const [allGameOptions, setAllGameOptions] = useState([]);
  const [isTransferListInit, setIsTransferInit] = useState(false);
  const [leftLoading, setLeftLoading] = useState(true);
  const [rightLoading, setRightLoading] = useState(true);

  const onUpdateSuccess = (response) => {
    const { data } = response;
    const availableGameId = data?.id;

    notify(translate("ra.message.created"), "success");

    if (availableGameId) {
      history.push(`/available-game/${availableGameId}/show`);
    }
  };

  const [update, { loading: updateLoading, error }] = useUpdate(
    undefined,
    undefined,
    undefined,
    undefined,
    {
      onSuccess: onUpdateSuccess,
    }
  );

  const saveAvailableGame = (values) => {
    if (!values.group?.id) {
      // TODO: add this string-template to BE-message '%{fieldName} should be not empty',
      notify(
        translate("ra.message.shouldNotEmpty").replace(
          "%{fieldName}",
          translate('resources.available-game.fields.group')
        ),
        "error"
      );
      return;
    }

    const { games } = values;

    update({
      resource: `available-game`,
      payload: {
        id: record?.id,
        data: { games },
      },
    });
  };

  const getListGame = async () => {
    try {
      const { data } = await enhancedDataProvider.getList("game", {
        pagination: {
          page: 1,
        },
      });

      if (Array.isArray(data)) {
        setAllGamesData(data);
      }

      setIsTransferInit(true);
      setLeftLoading(false);
    } catch (error) {
      notify(error.message, "error");
    }
  };

  const getLeftInitValues = (availableGamesInitial) => {
    if (Array.isArray(availableGamesInitial)) {
      const gameIdsExisted = availableGamesInitial.map((item) => item.id);
      return allGameOptions.filter(
        (gameId) => !gameIdsExisted.includes(gameId.value)
      );
    }

    return allGameOptions;
  };

  const getRightInitValues = (availableGamesInitial) => {
    if (Array.isArray(availableGamesInitial)) {
      return availableGamesInitial.map((game) => ({
        value: game.id,
        label: game.name,
        enabled: game.enabled,
      }));
    }

    return [];
  };

  const leftOptions = useMemo(() => ({
    initValues: getLeftInitValues(record?.games),
    title: <strong>{translate('ra.text.allGames')}</strong>,
  }), [isTransferListInit, locale]);

  const rightOptions = useMemo(() => ({
    initValues: getRightInitValues(record?.games),
    title: <strong>{translate('ra.text.availableGames')}</strong>,
  }), [isTransferListInit, locale]);

  useEffect(() => {
    if (!updateLoading) {
      if (error) {
        notifyError(error);
      }
    }
  }, [updateLoading, error]);

  useEffect(() => {
    getListGame();
    setRightLoading(false);
  }, []);

  useEffect(() => {
    if (allGamesData.length > 0) {
      const gameIdsFromAllGamesData = allGamesData.map((game) => ({
        value: game.id,
        label: game.name,
        enabled: game.enabled,
      }));
      setAllGameOptions(gameIdsFromAllGamesData);
    }
  }, [allGamesData]);

  return (
    <Edit
      {...props}
      classes={{ main: classes.raUpdateMain }}
      alwaysEnableSaveButton={true}
      onCustomSave={saveAvailableGame}
    >
      <SimpleForm warnWhenUnsavedChanges redirect="show">
        <FormDataConsumer>
          {({ formData }) => (
            <>
              <Box className={classes.contentWrap}>
                <Box className={classes.leftContent}>
                  <ReferenceInputCustom
                    label="resources.group.name"
                    source="group.id"
                    reference="group"
                    fullWidth
                    perPage={100}
                    afterOnChange={(_, form) => {
                      // NOTE: brand.id is source name of brand ReferenceInput
                      form.change("brand.id", "");
                    }}
                    variant="outlined"
                    disabled
                  >
                    <AutocompleteInput optionText="name" on />
                  </ReferenceInputCustom>
                  <ReferenceInput
                    label="resources.brand.name"
                    source="brand.id"
                    reference="brand"
                    fullWidth
                    perPage={100}
                    filter={{
                      "group.id": formData.group?.id,
                    }}
                    disabled
                    variant="outlined"
                    allowEmpty
                  >
                    <AutocompleteInput optionText="name" on />
                  </ReferenceInput>
                </Box>
                <Box className={classes.rightContent}>
                  <TransferList
                    leftOptions={leftOptions}
                    rightOptions={rightOptions}
                    leftLoading={leftLoading}
                    rightLoading={rightLoading}
                    classes={{
                      leftHead: classes.leftHeadClassName,
                      leftContent: classes.leftContentClassName,
                      rightHead: classes.rightHeadClassName,
                      rightContent: classes.rightContentClassName,
                    }}
                    onChange={(_, rightValues) => {
                      formData.games = rightValues.map((item) => ({
                        id: item.value,
                      }));
                    }}
                  />
                </Box>
              </Box>
            </>
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  );
};

export default AvailableGameEdit;
