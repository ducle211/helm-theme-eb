import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Card from '@material-ui/core/Card';
import Box from '@material-ui/core/Box';
import CardHeader from '@material-ui/core/CardHeader';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Tooltip from '@material-ui/core/Tooltip';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ErrorIcon from '@material-ui/icons/Error';
import clsx from 'clsx';
import { useTranslate } from 'ra-core';
import { OPTIMAL_BREAK_POINT_MOBILE, SIDE_TYPE } from './transfer-list.constant';
import Loading from './transfer-list.loading';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    height: 400,
    margin: 'auto',
    [theme.breakpoints.down(OPTIMAL_BREAK_POINT_MOBILE)]: {
      height: 'auto',
      flexDirection: 'column',
    },
  },
  h100percent: {
    height: '100%',
  },
  cardStyle: {
    height: '100%',
    minWidth: 258,
    display: 'flex',
    flexDirection: 'column',
    [theme.breakpoints.down(OPTIMAL_BREAK_POINT_MOBILE)]: {
      minWidth: 258,
    },
  },
  cardHeader: {
    padding: theme.spacing(1, 2),
  },
  listWrap: {
    flex: 1,
    padding: '0 !important',
    [theme.breakpoints.down(OPTIMAL_BREAK_POINT_MOBILE)]: {
      width: '100%',
    },
  },
  list: {
    flex: 1,
    backgroundColor: theme.palette.background.paper,
    overflow: 'auto',
    [theme.breakpoints.down(OPTIMAL_BREAK_POINT_MOBILE)]: {
      maxHeight: 256,
    },
  },
  listItem: {
    paddingLeft: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
  moveButtons: {
    display: 'flex',
    flexDirection: 'column',
    '& > button': {
      minWidth: 40,
      margin: theme.spacing(2, 1.5),
      padding: theme.spacing(10, 0),
    },
    [theme.breakpoints.down(OPTIMAL_BREAK_POINT_MOBILE)]: {
      flexDirection: 'row',
      '& > button': {
        margin: theme.spacing(2, 1.5),
        padding: theme.spacing(1, 10),
      },
    },
  },
  icon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    '& > svg': {
      fontSize: '1.2rem',
      marginLeft: '5px',
    },
  },
}));

function itemsOfBoth(b) {
  return ({ value }) => {
    const valueOfB = b.map(item => item.value);
    return valueOfB.indexOf(value) !== -1;
  };
}

function itemsExcludeB(b) {
  return ({ value }) => {
    const valueOfB = b.map(item => item.value);
    return valueOfB.indexOf(value) === -1;
  };
}

function not(a, b) {
  return a.filter(itemsExcludeB(b));
}

function intersection(a, b) {
  return a.filter(itemsOfBoth(b));
}

function union(a, b) {
  return [...a, ...not(b, a)];
}

function TransferList(props) {
  const {
    onChange,
    leftLoading,
    rightLoading,
    classes: {
      leftHead: leftHeadClass = '',
      leftContent: leftContentClass = '',
      rightHead: rightHeadClass = '',
      rightContent: rightContentClass = '',
    },
    leftOptions: {
      initValues: leftInitValue = [],
      title: leftTitle = '',
    },
    rightOptions: {
      initValues: rightInitValue = [],
      title: rightTitle = '',
    },
  } = props;

  const classes = useStyles();
  const muiTheme = useTheme();
  const translate = useTranslate();
  const matchMobileBreakPoint = useMediaQuery(
    muiTheme.breakpoints.down(OPTIMAL_BREAK_POINT_MOBILE),
  );

  const [checked, setChecked] = React.useState([]);
  const [left, setLeft] = React.useState([]);
  const [right, setRight] = React.useState([]);

  const leftChecked = intersection(checked, left);
  const rightChecked = intersection(checked, right);

  const valueCheckedList = checked.map(checkItem => checkItem.value);

  const handleToggle = item => () => {
    const currentIndex = valueCheckedList.indexOf(item.value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(item);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const numberOfChecked = items => intersection(checked, items).length;

  const handleToggleAll = items => () => {
    if (numberOfChecked(items) === items.length) {
      setChecked(not(checked, items));
    } else {
      setChecked(union(checked, items));
    }
  };

  const handleCheckedRight = () => {
    setRight(right.concat(leftChecked));
    setLeft(not(left, leftChecked));
    setChecked(not(checked, leftChecked));
  };

  const handleCheckedLeft = () => {
    setLeft(left.concat(rightChecked));
    setRight(not(right, rightChecked));
    setChecked(not(checked, rightChecked));
  };

  const customList = ({
    title, items, sideType, headClassName, contentClass, loading,
  }) => {
    const subHeader = translate('ra.text.selected')
      .replace('%{smart_count}', `${numberOfChecked(items)}/${items.length}`);

    const emptyBox = (
      <Box
        padding={2}
        display="flex"
        alignItems="center"
        justifyContent="center"
        width="100%"
        height="90%"
      >
        <Box color="#8A8A8A">
          {translate('ra.text.listEmpty')}
        </Box>
      </Box>
    );

    let isLoading = false;
    let isListEmpty = false;

    if (
      (sideType === SIDE_TYPE.left && loading.left)
      || (sideType === SIDE_TYPE.right && loading.right)
    ) {
      isLoading = true;
    }

    if (!isLoading && items.length === 0) {
      isListEmpty = true;
    }

    return (
      <Card className={classes.cardStyle}>
        <CardHeader
          className={clsx(classes.cardHeader, headClassName)}
          avatar={(
            <Checkbox
              onClick={handleToggleAll(items)}
              checked={
                numberOfChecked(items) === items.length && items.length !== 0
              }
              indeterminate={
                numberOfChecked(items) !== items.length
                && numberOfChecked(items) !== 0
              }
              disabled={items.length === 0}
              inputProps={{
                'aria-label': 'all items selected',
              }}
            />
          )}
          title={title}
          subheader={subHeader}
        />
        <Divider />
        <List
          className={clsx(classes.list, contentClass)}
          dense
          component="div"
          role="list"
        >
          {isLoading && <Loading sideType={sideType} />}
          {isListEmpty ? emptyBox : null}

          {items.map(item => {
            const labelId = `transfer-list-all-item-${item.value}-label`;
            return (
              <ListItem
                key={item.value}
                classes={{
                  root: classes.listItem,
                }}
                role="listitem"
                button
                onClick={handleToggle(item)}
              >
                <ListItemIcon>
                  <Checkbox
                    checked={valueCheckedList.indexOf(item.value) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{
                      'aria-labelledby': labelId,
                    }}
                  />
                </ListItemIcon>
                {item.enabled ? (
                  <ListItemText
                    id={labelId}
                    primary={item.label}
                  />
                ) : (
                  <Box
                    color={muiTheme.palette.error.main}
                    display="flex"
                    alignItems="center"
                  >
                    <ListItemText
                      id={labelId}
                      primary={item.label}
                    />
                    <Tooltip
                      title={translate('ra.text.disableTooltipMessage', {
                        smart_text: 'game',
                      })}
                      placement="top-start"
                    >
                      <Box
                        className={classes.icon}
                      >
                        <ErrorIcon />
                      </Box>
                    </Tooltip>
                  </Box>
                )}
              </ListItem>
            );
          })}
          <ListItem />
        </List>
      </Card>
    );
  };

  useEffect(() => {
    setLeft(leftInitValue);
  }, [leftInitValue]);

  useEffect(() => {
    setRight(rightInitValue);
  }, [rightInitValue]);

  useEffect(() => {
    onChange(left, right);
  }, [left, right]);

  return (
    <Grid
      container
      alignItems="center"
      className={classes.root}
    >
      <Grid
        item
        className={clsx(classes.h100percent, classes.listWrap)}
      >
        {customList({
          title: leftTitle,
          items: left,
          sideType: SIDE_TYPE.left,
          headClassName: leftHeadClass,
          contentClass: leftContentClass,
          loading: {
            left: leftLoading,
          },
        })}
      </Grid>
      <Grid item>
        <Grid
          container
          className={classes.moveButtons}
          direction="column"
          alignItems="center"
        >
          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleCheckedRight}
            disabled={leftChecked.length === 0}
            aria-label="move selected right"
          >
            {matchMobileBreakPoint ? <ExpandMoreIcon /> : <ChevronRightIcon />}
          </Button>
          <Button
            variant="outlined"
            size="small"
            className={classes.button}
            onClick={handleCheckedLeft}
            disabled={rightChecked.length === 0}
            aria-label="move selected left"
          >
            {matchMobileBreakPoint ? <ExpandLessIcon /> : <ChevronLeftIcon />}
          </Button>
        </Grid>
      </Grid>
      <Grid
        item
        className={clsx(classes.h100percent, classes.listWrap)}
      >
        {customList({
          title: rightTitle,
          items: right,
          sideType: SIDE_TYPE.right,
          headClassName: rightHeadClass,
          contentClass: rightContentClass,
          loading: {
            right: rightLoading,
          },
        })}
      </Grid>
    </Grid>
  );
}

TransferList.defaultProps = {
  leftOptions: {},
  rightOptions: {},
  onChange: f => f,
  leftLoading: false,
  rightLoading: false,
  classes: {},
};

TransferList.propTypes = {
  leftOptions: PropTypes.shape({
    initValues: PropTypes.array,
    title: PropTypes.node,
  }),
  rightOptions: PropTypes.shape({
    initValues: PropTypes.array,
    title: PropTypes.node,
  }),
  classes: PropTypes.shape({
    leftHead: PropTypes.string,
    leftContent: PropTypes.string,
    rightHead: PropTypes.string,
    rightContent: PropTypes.string,
  }),
  onChange: PropTypes.func,
  leftLoading: PropTypes.bool,
  rightLoading: PropTypes.bool,
};

export default memo(TransferList);
