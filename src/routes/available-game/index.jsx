import AvailableGameCreate from './available-game.create';
import AvailableGameShow from './available-game.show';
import AvailableGameEdit from './available-game.edit';

export default {
  create: AvailableGameCreate,
  show: AvailableGameShow,
  edit: AvailableGameEdit,
};
