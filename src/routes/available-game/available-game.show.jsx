/* eslint-disable react/prop-types */
import React from 'react';
import { SingleFieldList, useTranslate, ArrayField } from 'react-admin';
import WealthShowGuesser from '../../base/components/guesser/wealth-show.guesser';
import { ChipField } from '../../base/components/ra/fields';

const AvailableGameShow = props => {
  const translate = useTranslate();

  return (
    <WealthShowGuesser {...props}>
      <ArrayField
        label={translate('resources.available-game.fields.games')}
        source="games"
        fieldKey="id"
      >
        <SingleFieldList linkType={false}>
          <ChipField
            clickable={false}
            source="name"
          />
        </SingleFieldList>
      </ArrayField>
    </WealthShowGuesser>
  );
};

export default AvailableGameShow;
