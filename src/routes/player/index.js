import PlayerList from './player.list';
import PlayerShow from './player.show';

export default {
  show: PlayerShow,
  list: PlayerList,
};
