/* eslint-disable react/prop-types */
import { Button } from '@material-ui/core';
import * as React from 'react';
import { useTranslate } from 'react-admin';
import WealthListGuesser from '../../base/components/guesser/wealth-list.guesser';

const WagerLink = ({ record: { id } }) => {
  const translate = useTranslate();
  return (
    <a
      href={`#/wager?filter=%7B%22player%22%3A%7B%22id%7C%7C%24cont%22%3A%22${id}%22%7D%7D`}
    >
      <Button
        color="secondary"
        variant="contained"
        size="small"
      >
        {translate('ra.action.view_wager')}
      </Button>
    </a>
  );
};

function PlayerList(props) {
  return (
    <>
      <WealthListGuesser
        {...props}
        rowAction={<WagerLink label="ra.field.action" />}
        rowClick=""
      />
    </>
  );
}

export default PlayerList;
