/* eslint-disable react/prop-types */
import React from 'react';
import WealthShowGuesser from '../../base/components/guesser/wealth-show.guesser';

const PlayerShow = props => {
  const WagerLink = ({ recordId }) => (
    <a
      href={`#/wager?displayedFilters=%7B"player.id"%3Atrue%7D&filter=%7B"player"%3A%7B"id"%3A"${recordId}"%7D%7D`}
    >
      View wager
    </a>
  );
  return (
    <WealthShowGuesser
      {...props}
      contentFooter={<WagerLink />}
    />
  );
};

export default PlayerShow;
