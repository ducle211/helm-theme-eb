import * as React from 'react';
import { Route } from 'react-router-dom';
import DevTools from './devtools';
import ResetPassword from '../../theme/@jumbo/components/Common/authComponents/ResetPassword';

export const customRoutes = [
  <Route
    exact
    path="/devtools"
    component={DevTools}
  />,
];

export const publicCustomRoutes = [
  <Route
    exact
    path="/reset-password/:token"
    component={ResetPassword}
    noLayout
  />,
];
