/* eslint-disable*/
import {
    Button, Card,
    CardContent,
    CardHeader,

    Chip, makeStyles,

    MenuItem, TextField,
} from '@material-ui/core';
import Axios from 'axios';
import { isString, isArray } from 'lodash';
import { useNotify, useTranslate } from 'ra-core';
import React, { useEffect, useState } from 'react';
import { TopToolbar } from 'react-admin';
import { useSelector } from 'react-redux';

const useStyle = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root': {
            marginBottom: theme.spacing(6),
        },
    },
    chip: {
        marginBottom: theme.spacing(6),
    },
    main: {
        alignItems: 'flex-start',
        flexDirection: 'column',
    },
    btnSubmit: {
        padding: theme.spacing(2, 6),
    },
}));

const ChangePassword = () => {
    const user = useSelector(state => state.auth.user);
    const classes = useStyle();
    const t = useTranslate();
    const notify = useNotify();
    const translate = useTranslate();

    const [state, setState] = useState({
        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
    });

    const [isValid, setIsValid] = useState(true);

    const handleChange = e => {
        const {
            name, value,
        } = e.target;

        if (name === 'confirmPassword') {
            value === state.newPassword ? setIsValid(true) : setIsValid(false);
        }

        setState({
            ...state,
            [name]: value,
        });
    };

    const handleSubmit = async event => {
        event.preventDefault();
        // if (!isSubmit) return;
        let result;

        try {
            result = await Axios.put('api/auth/password/change', state);

            if (result.status === 200) {
                notify(t('resources.notification.fields.updated'));
                setState({
                    currentPassword: '',
                    newPassword: '',
                    confirmPassword: '',
                });
            }
        } catch (e) {
            const errData = e.response?.data;
            if (errData) {
                if (isArray(errData.message)) {
                    notify(JSON.stringify(errData.message), 'warning');
                } else if (isString(errData.message)) {
                    notify(errData.message, 'warning');
                }
                if (errData.key) {
                    notify(t(errData.key), 'warning');
                }
            } else {
                notify(e.message);
            }
        }
    };

    return (
        <TopToolbar className={classes.main}>
            {/* {renderUserStatus()} */}
            <Card>
                <CardHeader title={t('profile.text.changePassword')} />
                <CardContent>
                    {Object.keys(user).length && (
                        <form
                            className={classes.root}
                            autoComplete="off"
                            onSubmit={handleSubmit}
                        >
                            <TextField
                                id="currentPassword-id"
                                name="currentPassword"
                                onChange={handleChange}
                                value={state.currentPassword}
                                type="password"
                                label={t('profile.input.current-password')}
                            />
                            <TextField
                                id="newPassword-id"
                                name="newPassword"
                                onChange={handleChange}
                                value={state.newPassword}
                                type="password"
                                label={t('profile.input.newPassword')}
                            />
                            <TextField
                                id="confirmPassword-id"
                                name="confirmPassword"
                                onChange={handleChange}
                                value={state.confirmPassword}
                                type="password"
                                error={!isValid}
                                helperText={!isValid ? t('wa.exception.newPasswordAndConfirmPasswordNotMatch') : undefined}
                                label={t('profile.input.reEnterNewPassword')}
                            />

                            <Button
                                type="submit"
                                color="primary"
                                disabled={!isValid}
                                className={classes.btnSubmit}
                            >
                                {t('resources.perm.fields.submit')}
                            </Button>
                        </form>
                    )}
                </CardContent>
            </Card>
        </TopToolbar>
    );
};

export default ChangePassword;
