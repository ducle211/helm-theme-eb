import { Card, CardContent, CardHeader, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import * as md5 from 'md5';
import * as React from 'react';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(4),
      width: '50ch',
    },
  },
}));

const DevTools = () => {
  const classes = useStyles();

  const [encodedContent, encodeContent] = React.useState('');
  const handleChange = event => {
    encodeContent(md5(event.target.value));
  };

  return (
    <Card>
      <CardHeader title="WealthAsia DevTools" />
      <CardContent>Encode MD5</CardContent>
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
      >
        <div>
          <TextField
            id="content_input"
            label="Input content to be encoded here"
            onChange={handleChange}
          />
        </div>
        <div>
          <TextField
            disabled
            value={encodedContent}
          />
        </div>
        <div>
          <TextField
            disabled
            value={encodedContent.toUpperCase()}
          />
        </div>
      </form>
    </Card>
  );
};

export default DevTools;
