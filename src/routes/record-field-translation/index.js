import RecordFieldTranslationCreate from './record-field-translation.create';
import RecordFieldTranslationShow from './record-field-translation.show';
import RecordFieldTranslationEdit from './record-field-translation.edit';

export default {
  create: RecordFieldTranslationCreate,
  show: RecordFieldTranslationShow,
  edit: RecordFieldTranslationEdit,
};
