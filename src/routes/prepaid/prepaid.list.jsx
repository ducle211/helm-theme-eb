import * as React from 'react';
import { TextField } from 'react-admin';
import WealthListGuesser from '../../base/components/guesser/wealth-list.guesser';

function PrepaidList(props) {
  return (
    <>
      <WealthListGuesser
        {...props}
        rowClick={false}
      >
        <TextField source="count" />
        <TextField source="betAmount" />
      </WealthListGuesser>
    </>
  );
}

export default PrepaidList;
