/* eslint-disable no-unused-vars */
import React from 'react';
import moment from 'moment';
import {
  SimpleForm,
  Edit,
  AutocompleteArrayInput,
  ReferenceArrayInput,
  TextInput,
  BooleanInput,
  FormDataConsumer,
  DateInput,
  required,
} from 'react-admin';
import { IntegerInput } from '../../base/components/ra/inputs';

const PrepaidEdit = props => {
  const minDate = new Date().toISOString().split('T')[0];
  return (
    <Edit
      {...props}
      transform={({
        // eslint-disable-next-line no-unused-vars
        groupId,
        brandId,
        game,
        ...rest
      }) => ({
        ...rest,
        startDate: new Date(rest?.startDate),
        endDate: new Date(rest?.endDate),
      })}
      undoable={false}
    >
      <SimpleForm
        warnWhenUnsavedChanges
        redirect="show"
      >
        <TextInput
          fullWidth
          source="name"
        />

        <TextInput
          fullWidth
          source="desc"
        />

        <IntegerInput
          fullWidth
          source="minBet"
          min={0}
          label="resources.prepaid.fields.minBet"
        />

        <IntegerInput
          fullWidth
          label="resources.boost.prepaid.betAmount"
          source="value.betAmount"
        />

        <IntegerInput
          fullWidth
          label="resources.boost.prepaid.count"
          source="value.count"
        />

        <DateInput
          fullWidth
          source="startDate"
          inputProps={{
            min: minDate,
          }}
          validate={required()}
        />
        <FormDataConsumer>
          {({ formData }) => formData.startDate && (
            <DateInput
              fullWidth
              source="endDate"
              inputProps={{
                min: formData.startDate,
              }}
              validate={[
                required(),
                (value, values) => {
                  if (moment(value).isBefore(values?.startDate)) return 'End date must come after Start date';
                  return false;
                },
              ]}
            />
          )}
        </FormDataConsumer>

        <BooleanInput
          fullWidth
          source="isAllPlayer"
        />
        <FormDataConsumer>
          {({ formData }) => !formData.isAllPlayer && (
            <ReferenceArrayInput
              label="resources.player.name"
              source="players"
              reference="player"
              fullWidth
              parse={ids => ids?.map(id => ({
                id,
              }))}
              format={data => data?.map(d => d.id)}
            >
              <AutocompleteArrayInput optionText="nativeId" />
            </ReferenceArrayInput>
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Edit>
  );
};
export default PrepaidEdit;
