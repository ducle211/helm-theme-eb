import PrepaidCreate from './prepaid.create';
import PrepaidEdit from './prepaid.edit';
import PrepaidList from './prepaid.list';
import PrepaidShow from './prepaid.show';

export default {
  create: PrepaidCreate,
  list: PrepaidList,
  show: PrepaidShow,
  edit: PrepaidEdit,
};
