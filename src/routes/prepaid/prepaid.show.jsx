/* eslint-disable react/prop-types */
import React from 'react';
import { TextField } from 'react-admin';
import moment from 'moment';
import WealthShowGuesser from '../../base/components/guesser/wealth-show.guesser';
import SharableActionMenu, {
  SharableActionMenuType,
} from '../../base/components/action-menu';

const ActionMenu = props => {
  const { record } = props;
  const { startDate } = record;
  const canEdit = moment(new Date(startDate)).isAfter(new Date());
  return (
    <SharableActionMenu
      {...props}
      type={SharableActionMenuType.toolbar}
      hasEdit={canEdit}
    />
  );
};

const PrepaidShow = props => (
  <WealthShowGuesser
    {...props}
    actionMenu={<ActionMenu />}
  >
    <TextField
      source="value.count"
      label="Count"
    />
    <TextField
      source="value.betAmount"
      label="Bet Amount"
    />
  </WealthShowGuesser>
);

export default PrepaidShow;
