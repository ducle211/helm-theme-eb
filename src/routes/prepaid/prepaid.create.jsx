import moment from 'moment';
import React from 'react';
import {
  SimpleForm,
  Create,
  AutocompleteArrayInput,
  ReferenceArrayInput,
  TextInput,
  BooleanInput,
  FormDataConsumer,
  ReferenceInput,
  AutocompleteInput,
  DateInput,
  required,
} from 'react-admin';
import { IntegerInput } from '../../base/components/ra/inputs';

const PrepaidCreate = props => {
  const minDate = new Date().toISOString().split('T')[0];
  return (
    <Create
      {...props}
      transform={value => ({
        ...value,
        startDate: new Date(value?.startDate),
        endDate: new Date(value?.endDate),
      })}
    >
      <SimpleForm
        warnWhenUnsavedChanges
        redirect="show"
      >
        <ReferenceInput
          label="resources.group.name"
          reference="group"
          source="groupId"
          fullWidth
          validate={required()}
        >
          <AutocompleteInput optionText="name" />
        </ReferenceInput>
        <ReferenceInput
          label="resources.brand.name"
          reference="brand"
          source="brandId"
          fullWidth
          validate={required()}
        >
          <AutocompleteInput optionText="name" />
        </ReferenceInput>

        <ReferenceInput
          label="resources.game.name"
          reference="game"
          source="game.id"
          fullWidth
          validate={required()}
        >
          <AutocompleteInput optionText="name" />
        </ReferenceInput>
        <TextInput
          fullWidth
          source="name"
          validate={required()}
        />

        <TextInput
          fullWidth
          source="desc"
        />

        <IntegerInput
          fullWidth
          source="minBet"
          min={0}
          label="resources.prepaid.fields.minBet"
          validate={required()}
        />

        <IntegerInput
          fullWidth
          label="resources.boost.prepaid.betAmount"
          source="value.betAmount"
          validate={required()}
        />

        <IntegerInput
          fullWidth
          label="resources.boost.prepaid.count"
          source="value.count"
          validate={required()}
        />

        <DateInput
          fullWidth
          source="startDate"
          inputProps={{
            min: minDate,
          }}
          validate={required()}
        />

        <FormDataConsumer>
          {({ formData }) => formData.startDate && (
            <DateInput
              fullWidth
              source="endDate"
              inputProps={{
                min: formData.startDate,
              }}
              validate={[
                required(),
                (value, values) => {
                  if (moment(value).isBefore(values?.startDate)) return 'End date must come after Start date';
                  return false;
                },
              ]}
            />
          )}
        </FormDataConsumer>

        <BooleanInput
          fullWidth
          source="isAllPlayer"
        />
        <FormDataConsumer>
          {({ formData }) => !formData.isAllPlayer && (
            <ReferenceArrayInput
              label="resources.player.name"
              source="players"
              reference="player"
              fullWidth
              validate={required()}
              parse={ids => ids?.map(id => ({
                id,
              }))}
              format={data => data?.map(d => d.id)}
            >
              <AutocompleteArrayInput optionText="nativeId" />
            </ReferenceArrayInput>
          )}
        </FormDataConsumer>
      </SimpleForm>
    </Create>
  );
};
export default PrepaidCreate;
