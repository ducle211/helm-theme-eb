import axios from 'axios';
import polyglotI18nProvider from 'ra-i18n-polyglot';

const defaultMessages = require('./defaultMessages.json');

const waLocale = localStorage?.getItem('waLocale')?.replace(/["]+/g, '');
let init = 1;

export const i18nProvider = polyglotI18nProvider(locale => {
  if (init) {
    init = false;
    return defaultMessages;
  }

  // load language from api
  return axios.get(`api/i18n/backoffice/${locale}`).then(({ data }) => data);
}, waLocale || 'en', {
  allowMissing: true,
});
