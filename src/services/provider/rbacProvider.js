export const RbacAction = {
  CREATE: 'create',
  READ: 'read',
  LIST: 'list',
  UPDATE: 'update',
  DELETE: 'delete',
};

export const RbacActionWithoutClone = {
  CREATE: 'create',
  READ: 'read',
  LIST: 'list',
  UPDATE: 'update',
  DELETE: 'delete',
};

export const canI = (action, name, myPerms) => {
  let resource = name;
  if (resource?.includes('/')) {
    const splitResource = resource.split('/');
    resource = splitResource[splitResource.length - 1];
  }

  return (
    myPerms?.superman
    || (myPerms?.length > 0
      && myPerms?.some(p => p.resource.name === resource && p[action]))
  );
};
