/* eslint-disable no-param-reassign, prefer-destructuring */
import { CondOperator, RequestQueryBuilder } from '@nestjsx/crud-request';
import axios from 'axios';
import { camelCase, flatten, startCase, upperCase } from 'lodash';
import { stringify } from 'querystring';
import { fetchUtils, HttpError } from 'react-admin';
import { convertFileToBase64 } from '../util/convertFileToBase64';
import { prefixFilterDateRange } from '../../base/components/guesser/date-range-input';
import { stripResourceBuiltinData } from '../util/strip-resource-builtin-data';

const httpClient = async args => new Promise((resolve, reject) => {
  axios.request({
    ...args,
    method: args.method || 'get',
    // eslint-disable-next-line no-unused-vars
    validateStatus: code => true,
  })
    .then(({
      data, ...rest
    }) => {
      if (data.statusCode) {
        return reject(
          new HttpError(
            Array.isArray(data.message)
              ? data.message.join(', ')
              : data.message,
            data.statusCode,
            data,
          ),
        );
      }

      if (rest.status >= 400) {
        return reject(
          new HttpError(
            `${(Array.isArray(data.message)
              ? data.message.join(', ')
              : data.message) || rest.statusText}`,
            rest.status,
            data,
          ),
        );
      }

      return resolve({
        ...rest,
        data,
      });
    });
});
export const composeFilter = paramsFilter => {
  const flatFilter = fetchUtils.flattenObject(
    Object.keys(paramsFilter)
      .filter(key => key !== '$or')
      .reduce((obj, key) => ({
        ...obj,
        [key]: paramsFilter[key],
      }), {}),
  );

  return Object.keys(flatFilter)
    .filter(key => typeof flatFilter[key] !== 'object') // skip the empty-object filterValue when add new filter
    .map(key => {
      let filterValue = flatFilter[key];

      const splitKey = key.split('||');
      let field = splitKey[0];
      let ops = splitKey[1];

      if (!ops) {
        if (['boolean'].includes(typeof filterValue)) {
          ops = CondOperator.EQUALS;
        } else if (filterValue.startsWith(prefixFilterDateRange)) {
          ops = CondOperator.BETWEEN;
          filterValue = filterValue.split(prefixFilterDateRange)[1];
        } else {
          ops = CondOperator.CONTAINS;
        }
      }

      if (field.startsWith('_') && field.includes('.')) {
        field = field.split(/\.(.+)/)[1];
      }
      return {
        field,
        operator: ops,
        value: filterValue,
      };
    });
};

export const composeOr = paramsFilter => {
  const orParams = flatten(Object.keys(paramsFilter).filter(key => key === '$or').map(key => paramsFilter[key]));
  return orParams.map(item => {
    const splitKey = item?.split('||');
    const field = splitKey[0];
    const operator = splitKey[1];
    const value = splitKey?.[2];
    return {
      field,
      operator,
      value,
    };
  });
};

export const composeQueryParams = (queryParams = {}) => stringify(fetchUtils.flattenObject(queryParams));
export const mergeEncodedQueries = (...encodedQueries) => encodedQueries.map(query => query).join('&');

const crudProvider = apiUrl => ({
  getAll: (resource, params) => {
    const {
      q: queryParams, ...filter
    } = params.filter || {};
    const encodedQueryParams = composeQueryParams(queryParams);
    const encodedQueryFilter = RequestQueryBuilder.create({
      filter: composeFilter(filter),
    }).query();
    const query = mergeEncodedQueries(encodedQueryParams, encodedQueryFilter);
    const url = `${apiUrl}/${resource}?${query}`;
    return httpClient({
      url,
    }).then(({ data }) => ({
      data: data || [],
      total: data?.length,
    }));
  },

  getList: (resource, params) => {
    const {
      page, perPage = 25,
    } = params.pagination;
    const {
      q: queryParams, ...filter
    } = params.filter || {};

    // Get rid of null/undefined/empty string search value
    const enhancedFilters = Object.entries(filter || {}).reduce((obj, [key, value]) => {
      if (!Number.isInteger(value) && typeof value !== 'boolean') {
        if (!value) {
          return obj;
        }
      }
      obj[key] = value;
      return obj;
    }, {});

    const encodedQueryParams = composeQueryParams(queryParams);
    const encodedQueryFilter = RequestQueryBuilder.create({
      filter: composeFilter(enhancedFilters),
      or: composeOr(enhancedFilters),
    })
      .setLimit(perPage)
      .setPage(page)
      .sortBy(params.sort)
      .setOffset((page - 1) * perPage)
      .query();
    const query = mergeEncodedQueries(encodedQueryParams, encodedQueryFilter);
    const url = `${apiUrl}/${resource}?${query}`;
    return httpClient({
      url,
    }).then(({
      data: {
        data, total, summary,
      },
    }) => ({
      data,
      total,
      summary,
    }));
  },

  getOne: (resource, params) => httpClient({
    url: `${apiUrl}/${resource}/${params.id}`,
  }).then(({ data }) => ({
    data,
  })),

  getMany: (resource, params) => {
    const query = RequestQueryBuilder.create()
      .setFilter({
        field: 'id',
        operator: CondOperator.IN,
        value: `${params.ids}`,
      })
      .query();
    const url = `${apiUrl}/${resource}?${query}`;
    return httpClient({
      url,
    }).then(({ data }) => ({
      data,
    }));
  },

  getManyReference: (resource, params) => {
    const {
      page, perPage,
    } = params.pagination;
    const {
      q: queryParams, ...otherFilters
    } = params.filter || {};
    const filter = composeFilter(otherFilters);
    filter.push({
      field: params.target,
      operator: CondOperator.EQUALS,
      value: params.id,
    });

    const encodedQueryParams = composeQueryParams(queryParams);
    const encodedQueryFilter = RequestQueryBuilder.create({
      filter,
    })
      .sortBy(params.sort)
      .setLimit(perPage)
      .setOffset((page - 1) * perPage)
      .query();

    const query = mergeEncodedQueries(encodedQueryParams, encodedQueryFilter);
    const url = `${apiUrl}/${resource}?${query}`;

    return httpClient({
      url,
    }).then(({
      data: {
        data, total,
      },
    }) => ({
      data,
      total,
    }));
  },

  update: (resource, params) => httpClient({
    url: `${apiUrl}/${resource}/${params.id}`,
    method: 'patch',
    data: stripResourceBuiltinData(params?.data),
  }).then(({ data }) => ({
    data,
  })),

  submit: (resource, params) => httpClient({
    url: `${apiUrl}/${resource}/${params.id}/submit`,
    method: 'post',
  }).then(({ data }) => ({
    data,
  })),

  updateMany: (resource, params) => Promise.all(
    params.ids.map(id => httpClient({
      url: `${apiUrl}/${resource}/${id}`,
      method: 'patch',
      data: params.data,
    }).then(({ data }) => data)),
  ).then(data => ({
    data,
  })),

  create: (resource, params) => httpClient({
    url: `${apiUrl}/${resource}`,
    method: 'post',
    data: params.data,
  }).then(({ data }) => ({
    data: {
      ...params.data,
      id: data.id,
    },
  })),

  delete: (resource, params) => httpClient({
    url: `${apiUrl}/${resource}/${params.id}`,
    method: 'delete',
  }).then(({ data }) => ({
    data: {
      ...data,
      id: params.id,
    },
  })),

  deleteMany: (resource, params) => Promise.all(
    params.ids.map(id => httpClient({
      url: `${apiUrl}/${resource}/${id}`,
      method: 'delete',
    })),
  ).then(() => ({
    data: params.ids,
  })),

  put: async (resource, params) => httpClient({
    url: `${apiUrl}/${resource}${params.id ? `/${params.id}` : ''}`,
    method: 'put',
    data: params.data,
  }).then(({ data }) => ({
    data,
  })),
});

const pascalCase = str => startCase(camelCase(str)).replace(/ /g, '');

const parseBase64Data = async (resource, params) => {
  // Check has image field
  let realResource;
  realResource = pascalCase(resource.split('/')[0]);

  if (resource.includes('/')) {
    realResource = pascalCase(resource.split('/')[1]);
  }

  const { data } = await axios.get('api/api-json');
  const dataProperties = data.components.schemas[realResource]?.properties || data.components.schemas[upperCase(realResource)]?.properties;

  const imageFields = [];

  // eslint-disable-next-line no-undef
  if (realResource.toLocaleLowerCase() === 'setting' && params.data.value?.rawFile instanceof Blob) {
    return {
      value: {
        id: await convertFileToBase64(params.data.value.rawFile),
      },
    };
  }

  Object.keys(dataProperties || {}).forEach(key => {
    const refField = dataProperties[key].$ref || dataProperties[key].allOf?.filter(i => i?.$ref)?.[0]?.$ref;
    if (refField?.endsWith('Blob')) {
      imageFields.push(key);
    }
  });

  if (imageFields?.length === 0) {
    return {};
  }

  const base64Pictures = await Promise.all(imageFields
    .filter(fieldName => params.data[fieldName]?.rawFile instanceof window.File)
    .map(async fieldName => ({
      key: fieldName,
      value: await convertFileToBase64(params.data[fieldName]?.rawFile),
    })));

  return base64Pictures.reduce((obj, picture) => {
    obj[picture.key] = {
      id: picture.value,
    };
    return obj;
  }, {});
};

const enhancedDataProvider = {
  ...crudProvider('/api'),
  create: async (resource, params) => {
    const blobData = await parseBase64Data(resource, params);

    return crudProvider('/api').create(resource, {
      ...params,
      data: {
        ...params.data,
        ...blobData, // overide blob fields with blob data
      },
    });
  },
  update: async (resource, params) => {
    const blobData = await parseBase64Data(resource, params);
    return crudProvider('/api').update(resource, {
      ...params,
      data: {
        ...params.data,
        ...blobData,
      },
    });
  },
  // Write custom handlers here
  // https://marmelab.com/react-admin/DataProviders.html#extending-a-data-provider-example-of-file-upload
};

export default enhancedDataProvider;
