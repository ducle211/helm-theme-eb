import axios from 'axios';
import { forceLogout } from '../util/handleByHttpCode';
import { getHook } from '../util/react-hooks-outside';

const handleNotifyErrorOutside = error => {
  const notifyErrorInstant = getHook('useError');
  if (
    notifyErrorInstant
    && typeof notifyErrorInstant?.notifyError === 'function'
  ) {
    notifyErrorInstant.notifyError(error);
  }
};

// Add a request interceptor
axios.interceptors.request.use(config => config, error => Promise.reject(error));

// Add a response interceptor
axios.interceptors.response.use(response => response, error => {
  if (error?.response?.status === 401) {
    forceLogout();
  } else {
    handleNotifyErrorOutside(error);
  }
  return Promise.reject(error);
});
