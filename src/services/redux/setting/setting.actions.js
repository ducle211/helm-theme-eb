import { TOGGLE_NAV_COLLAPSED } from '../root.actions';

export const toggleNavCollapsed = isCollapsed => ({
  type: TOGGLE_NAV_COLLAPSED,
  payload: isCollapsed,
});
