import { TOGGLE_LOADING, GET_ALLOWED_RESOURCES_SUCCESS } from '../root.actions';

const initialApps = {
  isLoading: false,
  allowedResources: {},
};

export const appReducer = (state = initialApps, action) => {
  switch (action.type) {
    case TOGGLE_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case GET_ALLOWED_RESOURCES_SUCCESS:
      return {
        ...state,
        allowedResources: action.allowedResources,
      };
    default:
      return state;
  }
};
