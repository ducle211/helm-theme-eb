import { TOGGLE_LOADING, GET_ALLOWED_RESOURCES_SUCCESS } from '../root.actions';

export const toggleLoading = isLoading => ({
  type: TOGGLE_LOADING,
  payload: isLoading,
});

export const getAllowedResources = allowedResources => ({
  type: GET_ALLOWED_RESOURCES_SUCCESS,
  allowedResources,
});
