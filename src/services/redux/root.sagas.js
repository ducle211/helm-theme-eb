import { announcementSaga } from './announcement/announcement.saga';
import { authSaga } from './auth/auth.saga';
import { schemaSaga } from './schema/schema.saga';

export const rootSagas = [
  schemaSaga,
  authSaga,
  announcementSaga,
];
