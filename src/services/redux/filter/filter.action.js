const prefix = 'filter';

export const SET_FIELD_FILTER = `${prefix}/SET_FIELD_FILTER`;

export const setFieldFilter = (resource, properties) => ({
  type: SET_FIELD_FILTER,
  resource,
  properties,
});
