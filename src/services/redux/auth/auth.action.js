import { GET_AUTH_INFO, GET_AUTH_INFO_SUCCESS } from '../root.actions';

export const getAuthInfo = () => ({
  type: GET_AUTH_INFO,
});

export const setAuthInfo = user => ({
  type: GET_AUTH_INFO_SUCCESS,
  payload: {
    user,
  },
});
