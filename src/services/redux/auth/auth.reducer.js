import { GET_AUTH_INFO, GET_AUTH_INFO_SUCCESS, GET_AUTH_INFO_FAILED } from '../root.actions';

const initialSettings = {
  user: {},
};

export const authReducer = (state = initialSettings, action) => {
  switch (action.type) {
    case GET_AUTH_INFO:
      return {
        ...state,
        loading: true,
      };

    case GET_AUTH_INFO_SUCCESS:
      return {
        ...state,
        user: action.payload.user,
        loading: false,
      };

    case GET_AUTH_INFO_FAILED:
      return {
        ...state,
        user: {},
        loading: false,
      };

    default:
      return state;
  }
};
