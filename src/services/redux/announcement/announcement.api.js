import axios from 'axios';

export const getAnnouncements = async () => axios.get('/api/announcement/available');
