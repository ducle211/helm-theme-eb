import { GET_ANNOUNCEMENTS } from '../root.actions';

export const getAnnouncements = () => ({
  type: GET_ANNOUNCEMENTS,
});
