import { useNotify } from 'ra-core';
import { useError } from '../../../base/hooks';
import { setHook } from './index';

function registerHooks() {
  // Register any hook which you need to use outside app
  setHook('useNotify', useNotify)
    .setHook('useError', useError);
}

export default registerHooks;
