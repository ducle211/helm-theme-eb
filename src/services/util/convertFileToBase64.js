export const convertFileToBase64 = rawFile => new Promise((resolve, reject) => {
  const reader = new window.FileReader();
  reader.onload = () => resolve(reader.result);
  reader.onerror = reject;
  reader.readAsDataURL(rawFile);
});
