export const formatResources = (permittedResource = []) => {
  const data = permittedResource.map(resource => ({
    ...resource,
    children: resource.children.map(child => {
      if (child.name !== resource.name) {
        return {
          ...child,
          name: child.prefix ? `${child.prefix}/${child.name}` : child.name,
        };
      }
      return child;
    }),
  }));

  // flatten resource
  const resources = data.reduce((result, d) => {
    const parent = d;
    if (d.children.length > 0) {
      result.push(
        ...d.children.map(child => ({
          ...child,
          prefix: child.prefix,
          isChild: true,
        })),
      );
      // the children permission will be overridden if it has the same 'name' with parent
      const sameChild = d.children.find(x => x.name === d.name);
      if (sameChild) {
        parent.id = sameChild.id;
        parent.list = sameChild.list;
        parent.update = sameChild.update;
        parent.create = sameChild.create;
        parent.read = sameChild.read;
        parent.delete = sameChild.delete;
      }
    }
    result.push(parent);
    return result;
  }, []);

  return resources;
};
