import axios from 'axios';
import Cookie from './handleCookie';
import LocalStorage, { userStorageKey } from '../local-storage';
import { getHook } from './react-hooks-outside';

const informUnAuthorized = options => {
  const notify = getHook('useNotify');
  if (notify) {
    if (options?.hideMessage) {
      return;
    }

    if (options?.message) {
      notify(options.message, options.messageType);
      return;
    }

    notify('ra.message.yourAccountUnAuthorized', 'error');
  }
};

const requestSignOut = async () => {
  try {
    await axios.post('/api/Auth/signout');
  } catch {
    // Do nothing!
  } finally {
    Cookie.removeItem('accessToken');
    Cookie.removeItem('refreshToken');
    LocalStorage.removeItem(userStorageKey);
  }
};

export const forceLogout = options => {
  if (!window.location.hash.endsWith('/login')) {
    requestSignOut();
    informUnAuthorized(options);
    window.location.replace('/#/login');
  }
};
