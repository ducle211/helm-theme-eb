const getItem = key => {
  try {
    return JSON.parse(window.localStorage.getItem(key));
  } catch (e) {
    return undefined;
  }
};

const setItem = (key, value) => {
  try {
    window.localStorage.setItem(key, JSON.stringify(value));
    return true;
  } catch (e) {
    return false;
  }
};

const removeItem = key => {
  try {
    window.localStorage.removeItem(key);
    return true;
  } catch (e) {
    return false;
  }
};

const LocalStorage = {
  getItem,
  setItem,
  removeItem,
};

export const userStorageKey = 'user.me';

export default LocalStorage;
