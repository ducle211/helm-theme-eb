import { useEffect, useMemo } from 'react';
import useAccessToken from './useAccessToken';
import useAuthUser from './useAuthUser';
import useNavigation from './useNavigation';
import useSetting from './useSetting';

const FAVICON_DEFAULT = './favicon.ico';
const USER_TYPE = {
  system: 'system',
  group: 'group',
  brand: 'brand',
};

const useSite = () => {
  const user = useAuthUser();
  const accessTokenData = useAccessToken();
  const { currentPage } = useNavigation();
  const settings = useSetting();

  const userType = useMemo(() => {
    if (accessTokenData.group) {
      return USER_TYPE.group;
    } if (accessTokenData.affiliate) {
      return USER_TYPE.affiliate;
    }
    return USER_TYPE.system;
  }, [accessTokenData]);

  const USER_TYPE_OPTIONS = {
    [USER_TYPE.system]: {
      favicon: settings.site_favicon,
      title: settings.site_name,
    },
    [USER_TYPE.group]: {
      favicon: settings.site_group_favicon,
      title: `Group ${accessTokenData.group?.code || ''}`,
    },
  };

  useEffect(() => {
    const faviconEl = document.getElementById('site-favicon');
    if (!faviconEl) return;

    const faviconUrl = USER_TYPE_OPTIONS[userType].favicon;
    if (faviconUrl) {
      faviconEl.href = faviconUrl;
      faviconEl.onerror = `this.onerror=null;this.href='${FAVICON_DEFAULT}'`;
    }
  }, [user, currentPage?.label]);

  useEffect(() => {
    document.title = USER_TYPE_OPTIONS[userType].title;
  }, [user, currentPage?.label]);

  return {};
};

export default useSite;
