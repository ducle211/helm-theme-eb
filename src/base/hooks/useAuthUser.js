import { useMemo, useEffect } from 'react';
import { isEmpty } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUser } from '../../services/provider/authProvider';
import { setAuthInfo } from '../../services/redux/auth/auth.action';

const useAuthUser = () => {
  const dispatch = useDispatch();

  const user = useSelector(({ auth }) => auth?.user);

  useEffect(() => {
    (async () => {
      const currentUser = await fetchUser();
      if (currentUser) {
        dispatch(setAuthInfo(currentUser));
      }
    })();
  }, []);

  return useMemo(() => {
    if (!isEmpty(user)) {
      return {
        ...user,
        displayName: `${user.firstName} ${user.lastName}`,
      };
    }

    return {
      displayName: '',
    };
  }, [user]);
};

export default useAuthUser;
