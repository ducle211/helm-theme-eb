import { useNotify, useTranslate } from 'react-admin';
import { get } from 'lodash';

export const standardizeMessageArgs = (args, translate) => {
  const realArgs = {};
  Object.entries(args).forEach(([key, value]) => {
    const msgTranslated = translate(value);
    if (msgTranslated !== value) {
      realArgs[key] = msgTranslated;
      return;
    }
    realArgs[key] = value;
  });

  return realArgs;
};

const useError = () => {
  const notify = useNotify();
  const translate = useTranslate();
  const showError = msg => notify(msg, 'error');

  const additionHandlingErrorMessage = ({
    message, args,
  }) => {
    if (Array.isArray((message))) {
      (message).forEach(msg => {
        let finalMessage = msg;
        const messageTranslated = translate(msg);

        if (messageTranslated !== msg) {
          finalMessage = messageTranslated;
        }

        showError(finalMessage);
      });
      return;
    }

    const argsValid = args && Object.keys(args).length > 0;
    if (argsValid) {
      const realArgs = standardizeMessageArgs(args, translate);
      const messageTranslated = translate(message, realArgs);
      showError(messageTranslated);
      return;
    }

    showError(`Error! ${message}`);
  };

  const notifyError = error => {
    if (get(error, 'response.data.message')) {
      additionHandlingErrorMessage(error.response.data);
      return;
    }

    if (get(error, 'body.message')) {
      additionHandlingErrorMessage(error.body);
      return;
    }

    if (Array.isArray(error?.message)) {
      error.message.forEach(msg => showError(msg));
      return;
    }

    console.error(error.message);
    showError(translate('ra.message.tryAgain'));
  };

  return {
    notifyError,
  };
};

export default useError;
