import Axios from 'axios';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SETTINGS } from '../../services/redux/root.actions';

let isSettingLoading = false;

const useSetting = () => {
  const dispatch = useDispatch();
  const settings = useSelector(({ setting }) => setting.commonSettings);

  // Get settings common
  useEffect(() => {
    async function fetchSetting() {
      if (settings || isSettingLoading) return;

      isSettingLoading = true;
      const { data } = await Axios.get('api/shared/setting');
      isSettingLoading = false;

      dispatch({
        type: SETTINGS,
        settings: {
          ...data,
        },
      });
    }

    fetchSetting();
  }, [settings]);

  return settings;
};

export default useSetting;
