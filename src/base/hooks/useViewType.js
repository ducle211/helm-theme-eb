import { useEffect, useState } from 'react';

export const EnumViewType = {
  CREATE: '@VIEW_TYPE/CREATE',
  LIST: '@VIEW_TYPE/LIST',
  EDIT: '@VIEW_TYPE/EDIT',
  SHOW: '@VIEW_TYPE/SHOW',
};

let viewTypeGlobal = '';

const useViewType = (viewTypeProp = '') => {
  const [viewType, setViewType] = useState(viewTypeGlobal);

  useEffect(() => {
    if (Object.values(EnumViewType).includes(viewTypeProp)) {
      setViewType(viewTypeProp);
    }
  }, [viewTypeProp]);

  useEffect(() => {
    viewTypeGlobal = viewType;
  }, [viewType]);

  return {
    isCreateView: viewTypeGlobal === EnumViewType.CREATE,
    isListView: viewTypeGlobal === EnumViewType.LIST,
    isEditView: viewTypeGlobal === EnumViewType.EDIT,
    isShowView: viewTypeGlobal === EnumViewType.SHOW,
  };
};

export default useViewType;
