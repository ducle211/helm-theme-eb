/* eslint-disable react/jsx-filename-extension */
import { useSelector } from 'react-redux';
import React, { useMemo } from 'react';
import { Icon } from '@material-ui/core';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ViewListIcon from '@material-ui/icons/ViewList';
import { without, kebabCase } from 'lodash';
import { getResources, useTranslate } from 'react-admin';
import { useLocation } from 'react-router-dom';
import useAuthUser from './useAuthUser';
import { canI, RbacAction } from '../../services/provider/rbacProvider';
import { customRoutes } from '../../routes/customRoutes';

const useNavigation = () => {
  const resources = useSelector(getResources);
  const translate = useTranslate();
  const { pathname } = useLocation();
  const user = useAuthUser();
  const specialRoutes = customRoutes.map(x => x.path);

  const groupResources = useMemo(
    () => resources?.filter(r => {
      const valid = r.options?.isGroup === true && r.options.children?.length > 0;
      return valid;
    }) || [],
    [resources],
  );

  const nonGroupResources = useMemo(
    () => resources?.filter(r => {
      const valid = r.options?.isGroup !== true && !r.options?.isChild && r.hasList;
      return valid;
    }) || [],
    [resources],
  );

  const mainNavItem = {
    name: translate('ra.page.home'),
    type: 'collapse',
    icon: <DashboardIcon />,
    children: [
      {
        name: translate('ra.page.dashboard'),
        type: 'item',
        link: '/#',
        icon: <DashboardIcon />,
      },
    ],
  };

  const getIcon = icon => {
    if (!icon) return <ViewListIcon />;
    if (typeof icon !== 'string') {
      return <Icon>{React.createElement(icon)}</Icon>;
    }
    return <Icon>{icon}</Icon>;
  };

  const mapChildNavItem = c => {
    const realResource = c.name.includes('/')
      ? `${c.name.split('/')[1]}`
      : kebabCase(c.name);

    return {
      name: translate(`resources.${realResource}.name`),
      type: 'item',
      link: `/${c.name}`,
      icon: getIcon(c.icon),
      url: realResource,
      index: c.index,
    };
  };

  const perms = user?.superman
    ? {
      superman: user.superman,
    }
    : user?.perms?.filter(
      p => p.create || p.list || p.read || p.update || p.delete,
    );

  const mapParentNavItem = g => {
    const children = [...(g.options?.children || [])].filter(r => {
      const realResource = r.name.includes('/')
        ? `${r.name.split('/')[1]}`
        : kebabCase(r.name);
      return r.list && canI(RbacAction.LIST, realResource, perms);
    });

    return {
      name: translate(`resources.group.${kebabCase(g.name)}.name`),
      type: 'collapse',
      icon: getIcon(g.icon),
      children: children
        ?.map(c => mapChildNavItem(c))
        ?.sort((a, b) => a.index - b.index),
      url: g.name,
    };
  };

  const navigationMenus = useMemo(
    () => [mainNavItem].concat(
      groupResources
        .map(g => mapParentNavItem(g))
        .concat(
          nonGroupResources
            .map(c => mapChildNavItem(c))
            ?.sort((a, b) => a.index - b.index),
        ),
    ),
    [mainNavItem, groupResources, nonGroupResources],
  );

  const pageInfo = useMemo(() => {
    let currentPage;
    const results = [];
    const paths = without(pathname?.split('/') || [], '');

    // home
    currentPage = {
      label: translate('ra.page.dashboard'),
      link: '/',
    };
    results.push({
      ...currentPage,
      label: translate('ra.page.home'),
    });
    // resource
    if (paths?.length) {
      if (paths.length === 1 && specialRoutes.includes(paths[0])) {
        currentPage = {
          label: translate(`ra.page.${paths[0]}`),
          link: `/${paths[0]}`,
        };
        results.push(currentPage);
      } else {
        const menu = navigationMenus?.find(
          x => x.url
            && (x.url === paths?.[0]
              || x.children.find(c => c.url === paths?.[0])),
        );

        if (menu) {
          const childMenu = menu.children.find(c => c.url === paths?.[0]);

          currentPage = {
            label: menu.name,
            link: menu.link,
          };
          results.push(currentPage);

          if (childMenu) {
            currentPage = {
              label: childMenu.name,
              link: childMenu.link,
            };
            results.push(currentPage);
          }
        }

        if (paths?.length >= 2) {
          const uuidRegex = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/;
          if (paths[1].match(uuidRegex)) {
            results.push({
              label: `#${paths[1].slice(paths[1].length - 4)}`,
            }); // show / edit
          } else {
            results.push({
              label: translate(`ra.page.${paths[1]}`),
            }); // create
          }
        }
      }
    }

    // set active for latest item
    if (results?.length) {
      results[results.length - 1].isActive = true;
    }

    return {
      breadcrumbs: results,
      currentPage,
    };
  }, [resources, navigationMenus, pathname]);

  return {
    navigationMenus,
    ...pageInfo,
  };
};

export default useNavigation;
