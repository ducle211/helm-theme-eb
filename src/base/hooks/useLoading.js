import { useSelector, useDispatch } from 'react-redux';
import { toggleLoading as toggleLoadingAction } from '../../services/redux/app/app.actions';

const useLoading = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector(({ app }) => app?.isLoading);

  const toggleLoading = visible => {
    dispatch(toggleLoadingAction(visible));
  };

  return {
    isLoading,
    toggleLoading,
  };
};

export default useLoading;
