import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { PULL_WEALTH_SCHEMA } from '../../services/redux/root.actions';

const useSchema = () => {
  const {
    api, ref,
  } = useSelector(state => state?.schema);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!api) {
      dispatch({
        type: PULL_WEALTH_SCHEMA,
      });
    }
  }, [api]);

  return {
    api,
    ref,
  } || {};
};

export default useSchema;
