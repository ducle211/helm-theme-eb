import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles(() => ({
  root: {},
}));

export const withStandardButtonProps = WrappedComponent => props => {
  const classes = useStyles();

  return (
    <WrappedComponent
      className={classes.root}
      size="medium"
      variant="contained"
      disableElevation
      {...props}
    />
  );
};
