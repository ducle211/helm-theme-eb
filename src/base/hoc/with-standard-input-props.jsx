import React from 'react';

export const withStandardInputProps = WrappedComponent => props => (
  <WrappedComponent
    {...props}
    variant="outlined"
  />
);
