import React, { useContext } from 'react';
import PropTypes from 'prop-types';

const LanguageContext = React.createContext(null);

export const useLanguageContext = () => useContext(LanguageContext);

export const LanguageContextProvider = ({ children }) => {
  const [language, setLanguage] = React.useState(null);
  return (
    <LanguageContext.Provider
      value={{
        language,
        setLanguage,
      }}
    >
      {children({
        language,
      })}
    </LanguageContext.Provider>
  );
};

LanguageContextProvider.propTypes = {
  children: PropTypes.func.isRequired,
};

LanguageContextProvider.defaultProps = {};
