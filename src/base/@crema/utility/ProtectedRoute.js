import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = ({
  component: Component,
  isAuthenticated,
  isVerifying,
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      isVerifying ? (
        <Box />
      ) : isAuthenticated ? (
        <Component {...props} />
      ) : (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: props.location },
              }}
            />
          )
    }
  />
);

ProtectedRoute.propTypes = {
  component: PropTypes.node.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  isVerifying: PropTypes.bool.isRequired,
};

export default ProtectedRoute;