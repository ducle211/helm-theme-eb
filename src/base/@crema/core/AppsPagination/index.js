import TablePagination from '@material-ui/core/TablePagination';
import PropTypes from 'prop-types';
import React from 'react';

const AppsPagination = ({
  count,
  page,
  onPageChange,
  rowsPerPage = 15,
  className,
}) => {
  return (
    <TablePagination
      component="div"
      count={count || 0}
      rowsPerPage={rowsPerPage}
      className={className}
      page={page}
      backIconButtonProps={{ 'aria-label': 'Previous Page' }}
      nextIconButtonProps={{ 'aria-label': 'Next Page' }}
      onChangePage={onPageChange}
      rowsPerPageOptions={[]}
    />
  );
};

AppsPagination.defaultProps = {
  className: '',
};

AppsPagination.propTypes = {
  count: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  onPageChange: PropTypes.func,
  className: PropTypes.string,
};

export default AppsPagination;