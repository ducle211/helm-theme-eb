import React from 'react';
import PropTypes from 'prop-types';
import RowMenu from './row-menu';
import ToolbarMenu from './toolbar-menu';

export const SharableActionMenuType = Object.freeze({
  row: 'row',
  toolbar: 'toolbar',
});

const SharableActionMenu = props => {
  const {
    type, record, makeExtraOptions,
  } = props;

  const extraOptions = makeExtraOptions && makeExtraOptions(record);

  if (type === SharableActionMenuType.toolbar) {
    return (
      <ToolbarMenu
        extraOptions={extraOptions}
        {...props}
      />
    );
  }

  return (
    <RowMenu
      extraOptions={extraOptions}
      {...props}
    />
  );
};

SharableActionMenu.propTypes = {
  basePath: PropTypes.string.isRequired,
  resource: PropTypes.string.isRequired,
  record: PropTypes.object,
  anchorEl: PropTypes.object,
  handleClose: PropTypes.func,
  hasShow: PropTypes.bool,
  hasList: PropTypes.bool,
  hasEdit: PropTypes.bool,
  makeExtraOptions: PropTypes.func,
  type: PropTypes.string,
};

SharableActionMenu.defaultProps = {
  record: {},
  hasList: true,
  hasEdit: true,
  hasShow: true,
  anchorEl: null,
  handleClose: () => null,
  makeExtraOptions: () => {},
  type: SharableActionMenuType.row,
};

export default SharableActionMenu;
