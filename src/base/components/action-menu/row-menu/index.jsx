import { ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

const RowMenu = props => {
  const {
    anchorEl,
    handleClose,
    record,
    resource,
    extraOptions,
    hasShow,
    hasEdit,
  } = props;

  function handleClick(action) {
    handleClose();
    if (action?.onClick instanceof Function) {
      action.onClick();
    }
  }

  return (
    <Menu
      anchorEl={anchorEl}
      keepMounted
      open={Boolean(anchorEl)}
      onClose={handleClose}
    >
      {hasShow && (
        <MenuItem
          component={Link}
          to={`${resource}/${record?.id}/show`}
        >
          <ListItemIcon>
            <VisibilityIcon fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Show</Typography>
        </MenuItem>
      )}

      {hasEdit && (
        <MenuItem
          component={Link}
          to={`${resource}/${record?.id}`}
        >
          <ListItemIcon>
            <EditIcon fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Edit</Typography>
        </MenuItem>
      )}

      {extraOptions?.map(
        action => action.condition && (
          <MenuItem
            key={action.name}
            component={action?.to ? Link : 'span'}
            to={action?.to}
            onClick={() => handleClick(action)}
          >
            <ListItemIcon>
              {React.cloneElement(action.icon, {
                fontSize: 'small',
              })}
            </ListItemIcon>
            <Typography variant="inherit">{action.name}</Typography>
          </MenuItem>
        ),
      )}
    </Menu>
  );
};

RowMenu.propTypes = {
  hasShow: PropTypes.bool,
  hasEdit: PropTypes.bool,
  anchorEl: PropTypes.object,
  handleClose: PropTypes.func.isRequired,
  record: PropTypes.object.isRequired,
  resource: PropTypes.string.isRequired,
  extraOptions: PropTypes.arrayOf(
    PropTypes.shape({
      condition: PropTypes.bool,
      to: PropTypes.string,
      name: PropTypes.string,
      icon: PropTypes.object, // MaterialUI Icon
    }),
  ),
};

RowMenu.defaultProps = {
  anchorEl: null,
  extraOptions: [],
  hasShow: true,
  hasEdit: true,
};

export default RowMenu;
