/* eslint-disable no-nested-ternary */
import {
  Box,
  Chip,
  ListItemIcon,
  makeStyles,
  Menu,
  MenuItem,
  Typography,
  useMediaQuery,
} from '@material-ui/core';
import ContentCreate from '@material-ui/icons/Create';
import ActionList from '@material-ui/icons/List';
import { useTheme } from '@material-ui/styles';
import PropTypes from 'prop-types';
import { linkToRecord } from 'ra-core';
import React, { useMemo, useState } from 'react';
import {
  Button,
  EditButton,
  ListButton,
  TopToolbar,
  useTranslate,
} from 'react-admin';
import { Link } from 'react-router-dom';
import CustomTooltip from '../../custom-tooltip';
import { MenuField } from '../../ra/fields';
import { useViewType } from '../../../hooks';
import { Workflow } from '../workflow';

const ToolbarMenuDisplayType = Object.freeze({
  context: 'context',
  button: 'button',
});

const useStyles = makeStyles(theme => ({
  groupChip: {
    display: 'flex',
    gap: '0.5rem',
    flexWrap: 'wrap',
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '80%',
    },
  },
}));

const ToolbarMenu = props => {
  const {
    basePath,
    resource,
    record,
    hasList,
    hasEdit,
    children,
    extraOptions,
    displayType,
    isCustom, // eslint-disable-line react/prop-types
  } = props;

  const { isEditView } = useViewType();
  const classes = useStyles();
  const translate = useTranslate();
  const [anchorEl, setAnchorEl] = useState();
  const theme = useTheme();
  const screenLg = useMediaQuery(theme.breakpoints.up('lg'));

  const openExtraMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeExtraMenu = () => {
    setAnchorEl(false);
  };

  const handleClick = action => {
    closeExtraMenu();
    if (action?.onClick instanceof Function) {
      action.onClick();
    }
  };

  const defaultOptions = useMemo(() => {
    const results = [];

    if (!screenLg) {
      if (hasList) {
        results.push({
          name: translate('ra.action.list'),
          condition: true,
          icon: <ActionList />,
          to: basePath,
        });
      }
      if (hasEdit) {
        results.push({
          name: translate('ra.action.edit'),
          condition: true,
          icon: <ContentCreate />,
          to: linkToRecord(basePath, record && record.id),
        });
      }
    }

    return results;
  }, [hasList, hasEdit, basePath, record, translate, screenLg]);

  const defaultButtons = useMemo(() => {
    if (!screenLg) return null;

    const editable = hasEdit && !isEditView;

    return (
      <>
        {hasList && (
          <CustomTooltip
            title={translate('ra.action.list')}
            placement="top"
          >
            <ListButton
              basePath={basePath}
              record={record}
              label=""
            />
          </CustomTooltip>
        )}
        {editable && (
          <CustomTooltip
            title={translate('ra.action.edit')}
            placement="top"
          >
            <EditButton
              basePath={basePath}
              record={record}
              label=""
            />
          </CustomTooltip>
        )}
      </>
    );
  }, [hasList, hasEdit, basePath, record, translate, screenLg, isEditView]);

  const contextMenuItems = useMemo(() => [...defaultOptions, ...extraOptions], [
    defaultOptions,
    extraOptions,
  ]);

  const renderChip = () => {
    let color;
    if (resource === 'player' && basePath === '/player') {
      switch (record?.status?.name) {
        case 'Active':
          color = 'primary';
          break;
        case 'Locked':
          color = 'secondary';
          break;
        default:
          color = 'default';
          break;
      }

      return (
        <div className={classes.groupChip}>
          <Chip
            label={`${translate('resources.player.fields.totalBet')}: ${record?.totalBet
            }`}
            color="primary"
          />
          <Chip
            label={`${translate('resources.player.fields.totalWinLoss')}: ${record?.totalWinLoss
            }`}
            color="primary"
          />
        </div>
      );
    }

    // if record have enable
    if (record?.enabled) {
      switch (record?.enabled) {
        case true:
          color = 'primary';
          break;
        default:
          color = 'secondary';
          break;
      }

      return (
        <Chip
          // label={`${translate(`resources.${resource}.fields.enabled`)}: ${record?.enabled ? 1 : 0
          // }`}
          label={
            record?.enabled
              ? translate(`resources.${resource}.fields.enabled`)
              : translate(`resources.${resource}.fields.disable`)
          }
          color={color}
        />
      );
    }

    return <div />;
  };

  const renderContextMenu = () => (
    <>
      <MenuField
        record={record}
        onClick={openExtraMenu}
      />
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={!!anchorEl}
        onClose={closeExtraMenu}
      >
        {contextMenuItems.map(
          action => action.condition && (
            <MenuItem
              key={action.name}
              component={action?.to ? Link : 'span'}
              to={action?.to}
              onClick={() => handleClick(action)}
            >
              {action.icon && (
                <ListItemIcon>
                  {React.cloneElement(action.icon, {
                    fontSize: 'small',
                  })}
                </ListItemIcon>
              )}
              <Typography variant="inherit">{action.name}</Typography>
            </MenuItem>
          ),
        )}
      </Menu>
    </>
  );

  const renderButtonMenu = () => extraOptions?.map(
    option => option.condition && (
      <Button
        key={option.name}
        startIcon={option.icon}
        label={option.name}
        to={option.to || ''}
        {...(option.onClick instanceof Function
          ? {
            onClick: () => option.onClick(),
          }
          : {
            component: Link,
          })}
      />
    ),
  );

  return (
    <TopToolbar>
      {renderChip(record)}
      <Box
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        {children}
        {defaultButtons}
        {!isCustom && (
          <Workflow
            record={record}
            resource={resource}
          />
        )}
        {displayType === ToolbarMenuDisplayType.context
          && contextMenuItems.length ? (
            contextMenuItems.every(option => option.condition === false) ? (
              <></>
            ) : (
              renderContextMenu()
            )
          ) : (
            renderButtonMenu()
          )}
      </Box>
    </TopToolbar>
  );
};

ToolbarMenu.propTypes = {
  basePath: PropTypes.string.isRequired,
  resource: PropTypes.string.isRequired,
  record: PropTypes.object,
  children: PropTypes.element,
  hasList: PropTypes.bool,
  hasEdit: PropTypes.bool,
  extraOptions: PropTypes.arrayOf(
    PropTypes.shape({
      condition: PropTypes.bool,
      to: PropTypes.string,
      name: PropTypes.string,
      icon: PropTypes.object,
      onClick: PropTypes.func,
    }),
  ),
  displayType: PropTypes.oneOf([
    ToolbarMenuDisplayType.context,
    ToolbarMenuDisplayType.button,
  ]),
};

ToolbarMenu.defaultProps = {
  extraOptions: [],
  hasList: false,
  children: null,
  hasEdit: false,
  displayType: ToolbarMenuDisplayType.context,
  record: undefined,
};

export default ToolbarMenu;
