import React from 'react';
import PropTypes from 'prop-types';
import { Button, Link } from 'react-admin';

/**
 * @FantasticButton is a filterable button that will redirect to @resource and filter with @filter
 * Example of usage:
 * <FantasticButton
 *  label="Pending withdrawal"
 *  targetResource="withdrawal"
 *  filter={{
 *    user: {
 *      id: "fa6b6d28-643f-250d-7eeb-08e2893876a0",
 *    },
 *    status: {
 *      id: "4e70305b-e79a-dade-0667-1a75add746b4",
 *    }
 *  }}
 * />
 */
const FantasticButton = ({
  startIcon,
  label,
  filter,
  recordId,
  action,
  resource,
  children,
  target,
  ...props
}) => {
  let route = '';
  if (filter) {
    route = `/${resource}?filter=${JSON.stringify(filter)}`;
  } else {
    route = `/${resource}/${recordId}${action ? `/${action}` : ''}`;
  }

  if (label) {
    return (
      <Button
        {...props}
        startIcon={startIcon}
        label={label}
        to={route}
        component={Link}
      >
        {children}
      </Button>
    );
  }

  return (
    <a
      href={`/#${route}`}
      target={target}
    >
      {children}
    </a>
  );
};

FantasticButton.propTypes = {
  label: PropTypes.string,
  startIcon: PropTypes.node,
  filter: PropTypes.object.isRequired,
  resource: PropTypes.string.isRequired,
  target: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  action: PropTypes.oneOf(['show', '']),
  recordId: PropTypes.string,
};

FantasticButton.defaultProps = {
  label: undefined,
  startIcon: undefined,
  target: '_blank',
  children: undefined,
  action: 'show',
  recordId: undefined,
};

export default FantasticButton;
