/* eslint-disable react/no-array-index-key, react/require-default-props, no-shadow */
import React, { memo, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
} from '@material-ui/core';
import isEmpty from 'lodash/isEmpty';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  rootTableContainer: props => ({
    maxHeight: props.notLimitContent ? 'unset' : 600,
    overflow: 'auto',
    position: 'relative',
  }),
  rootTable: {
    [theme.breakpoints.down('xs')]: {
      overflow: 'auto',
    },
  },
  cellHead: {
    padding: theme.spacing(1.5),
    textTransform: 'capitalize',
    fontSize: '14px',
    cursor: 'pointer',
  },
  row: {
    border: 'none',
  },
  cellRow: {
    borderBottom: 'none',
  },
  rowOdd: {},
  header: {
    background: 'transparent',
    '& > *': {
      borderBottom: 0,
      fontWeight: 700,
    },
  },
  width100: {
    display: 'block',
    width: 'max-content',
    '&.MuiChip-root': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  win: {
    background: theme.palette.success.main,
    color: '#FFF',
  },
  lose: {
    background: theme.palette.error.main,
    color: '#FFF',
  },
}));

const TableComponent = ({
  listHeader, listRow, className, notLimitContent, pagination,
}) => {
  const classes = useStyles({
    notLimitContent,
  });

  const refTable = useRef({});

  useEffect(() => {
    if (!isEmpty(refTable.current)) {
      refTable.current.scrollTo(0, 0);
    }
  }, [listRow]);

  return (
    <TableContainer
      ref={refTable}
      classes={{
        root: clsx(classes.rootTableContainer, className),
      }}
    >
      <Table
        classes={{
          root: classes.rootTable,
        }}
      >
        {listHeader && (
          <TableHead>
            <TableRow className={classes.nonBorder}>
              {listHeader?.map((item, index) => (
                <TableCell
                  key={index}
                  className={classes.cellHead}
                  align={item.position || 'left'}
                >
                  <span className={classes.width100}>{item.nameHead}</span>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
        )}
        <TableBody>
          {listRow?.map((row, index) => (
            <TableRow
              key={index}
              className={clsx(
                classes.nonBorder,
                classes.row,
                row.isHeader && classes.header,
                index % 2 === 1 && classes.rowOdd,
              )}
            >
              {row.contents?.map(({
                row, position, component: Component, componentProps,
              }, i) => (
                <TableCell
                  key={i}
                  align={position}
                  scope="row"
                  component="th"
                  className={classes.cellRow}
                >
                  {Component ? (
                    <Component
                      className={clsx(classes.width100, classes[row?.toLowerCase()])}
                      label={row ?? 'N/A'}
                      {...componentProps}
                    >
                      {row ?? 'N/A'}
                    </Component>
                  ) : (
                    <span className={classes.width100}>{row ?? 'N/A'}</span>
                  )}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <TablePagination {...pagination} />
    </TableContainer>
  );
};

TableComponent.propTypes = {
  listHeader: PropTypes.array,
  listRow: PropTypes.array,
  className: PropTypes.string,
  notLimitContent: PropTypes.bool,
  pagination: PropTypes.object,
};

export default memo(TableComponent);
