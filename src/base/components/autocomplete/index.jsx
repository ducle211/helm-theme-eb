/* eslint-disable */
import * as React from "react";
import { Autocomplete } from "@material-ui/lab";
import { TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
  formControl: {
    minWidth: 200,
    maxWidth: 300,
    "& div": {
      "& label": {
        transform: "translate(14px, 12px) scale(1)",
      },
      "& div": {
        padding: "0px !important",
      },
    },
    "& > div > div": {
      minHeight: "40px",
    },
    "& input": {
      paddingLeft: "14px !important",
      paddingRight: "14px !important",
    },
  },
  field: {
    margin: 0,
  },
}));

const AutocompleteInput = ({
  label,
  data,
  value,
  onChange,
  getOptionLabel,
  ...props
}) => {
  const classes = useStyles();

  return (
    <Autocomplete
      className={classes.formControl}
      options={data || []}
      value={value}
      freeSolo
      onChange={onChange}
      getOptionLabel={getOptionLabel}
      renderInput={(params) => (
        <TextField
          {...params}
          classes={{
            root: classes.field,
          }}
          label={label}
          margin="normal"
          variant="outlined"
        />
      )}
      {...props}
    />
  );
};

export default AutocompleteInput;
