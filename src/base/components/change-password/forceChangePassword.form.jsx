import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useTranslate } from 'ra-core';
import ChangePasswordForm from './changePassword.form';

function ForceChangePasswordForm(props) {
  const {
    isOpen, onClose, user,
  } = props;
  const translate = useTranslate();

  const handleClose = () => {
    onClose();
  };

  const formTitle = useMemo(() => (
    <span>
      {translate('ra.text.changePasswordTitle')
        .replace(
          '%{smart_text}',
          `#${user.username}`,
        )}
    </span>
  ), [user.username, translate]);

  return (
    <ChangePasswordForm
      title={formTitle}
      record={user}
      open={isOpen}
      close={handleClose}
      hasCloseButton
      hasLogoutButton={false}
      hasRequireChangePassword={false}
    />
  );
}

ForceChangePasswordForm.propTypes = {
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  user: PropTypes.object.isRequired,
};

ForceChangePasswordForm.defaultProps = {
  isOpen: false,
  onClose: f => f,
};

export default ForceChangePasswordForm;
