/* eslint-disable react/require-default-props */
/* eslint-disable no-param-reassign */
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { getResources, Loading } from 'react-admin';
import { useSchema, useViewType } from '../../hooks';
import { EnumViewType } from '../../hooks/useViewType';
import WealthCreate from './wealth-create';
import { guessProperty } from './wealth-guesser';

const WealthCreateGuesser = props => {
  useViewType(EnumViewType.CREATE);

  const resources = useSelector(getResources);
  const [guessedInfo, setGuessedInfo] = useState({});

  const {
    api, ref,
  } = useSchema();
  const {
    excludeFields, ...rest
  } = props;
  const {
    hasCreate, basePath, resource,
  } = rest;

  const transform = writableFields => data => (writableFields.length
    ? writableFields.reduce((result, field) => {
      result[field] = data?.[field];
      return result;
    }, {})
    : data);

  useEffect(() => {
    if (api) {
      const { paths } = api;
      let requestRef = paths?.[basePath]?.post?.requestBody?.content?.['application/json']
        ?.schema;

      requestRef = requestRef?.$ref
        || requestRef?.allOf?.filter(i => i?.$ref)?.[0]?.$ref;
      const requestComponent = ref.get(requestRef);
      const {
        properties, required,
      } = requestComponent;
      const writableFields = Object.keys(properties)
        ?.filter(key => !excludeFields?.includes(key))
        ?.filter(key => {
          const simpleFieldRO = properties?.[key]?.readOnly;
          const complexFieldRO = properties?.[key]?.allOf?.some(
            p => p.readOnly,
          );

          return !simpleFieldRO && !complexFieldRO;
        });
      const components = writableFields?.map(i => guessProperty({
        source: i,
        properties,
        apiRef: ref,
        view: 'create',
        resource,
        resources,
        required,
      }));

      setGuessedInfo({
        required,
        components,
        writableFields,
      });
    }
  }, [api, basePath]);

  if (!hasCreate) {
    throw new Error('This resource does not support create');
  }

  return api ? (
    <WealthCreate
      required={guessedInfo?.required}
      resource={resource}
      transform={transform(guessedInfo?.writableFields)}
      writableFields={guessedInfo?.writableFields}
      {...rest}
    >
      {guessedInfo?.components}
    </WealthCreate>
  ) : (
    <Loading />
  );
};

WealthCreateGuesser.propTypes = {
  hasCreate: PropTypes.bool,
  basePath: PropTypes.string,
  excludeFields: PropTypes.arrayOf(PropTypes.string),
};

WealthCreateGuesser.defaultProps = {
  hasCreate: false,
  basePath: null,
  excludeFields: [],
};

export default WealthCreateGuesser;
