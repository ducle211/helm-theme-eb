const RefManyBulkActionTypes = {
  ADD: 'add',
  REMOVE: 'remove',
};
export default RefManyBulkActionTypes;
