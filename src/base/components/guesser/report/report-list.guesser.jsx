/* eslint-disable react/prop-types */
import { isEmpty, startCase } from 'lodash';
import React from 'react';
import {
  Datagrid,
  ReferenceField,
  TextField,
  useListContext,
  useTranslate,
} from 'react-admin';
import List from '../../ra/list/List';
import { IdField } from '../../ra/fields';
import SearchPanel from './search-panel';
import EmptyTable from './emptyTable';

function DatagridContainer(dProps) {
  const {
    data, ids,
  } = useListContext();
  const translate = useTranslate();

  return (
    <>
      {ids?.length > 0 ? (
        <Datagrid {...dProps}>
          {Object.keys(data[ids[0]] || {})?.map(key => {
            const label = startCase(
              translate(`resources.report.fields.${key}`),
            );
            if (['group', 'brand'].includes(key)) {
              return (
                <ReferenceField
                  key={key}
                  reference={key}
                  source={key}
                  link={false}
                  label={label}
                >
                  <TextField source="name" />
                </ReferenceField>
              );
            }
            if (key === 'playerId') {
              return (
                <IdField
                  source="playerId"
                  key={key}
                  label={label}
                />
              );
            }
            return (
              key !== 'id' && (
                <TextField
                  label={
                    key === 'rtp' || key?.indexOf('Trend') > 0
                      ? `${label} (%)`
                      : label
                  }
                  key={key}
                  source={key}
                />
              )
            );
          })}
        </Datagrid>
      ) : (
        <EmptyTable />
      )}
    </>
  );
}

function ReportListGuesser(props) {
  const {
    reportInterface, resource, ...rest
  } = props;

  return (
    <>
      <List
        {...rest}
        resource={resource}
        searchPanel={
          !isEmpty(reportInterface.filter) && (
            <SearchPanel
              filters={reportInterface.filter}
              resource={resource}
            />
          )
        }
        bulkActionButtons={false}
      >
        <DatagridContainer />
      </List>
    </>
  );
}

export default ReportListGuesser;
