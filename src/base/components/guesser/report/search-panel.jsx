/* eslint-disable react/prop-types */
import { Box, Grid, CircularProgress } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import React from 'react';
import {
  Button,
  NumberInput,
  ReferenceInput,
  SelectInput,
  TextInput,
  useListContext,
  RadioButtonGroupInput,
  DateTimeInput,
  useTranslate,
} from 'react-admin';
import { startCase, camelCase } from 'lodash';
import { Field, Form } from 'react-final-form';
import { useEnumOptions } from '../../../hooks';
import { CheckBoxGroup, AutocompleteInput } from '../../ra/inputs';
import DateRangeInput from '../date-range-input';
import MonthYearInput from './monthYear.input';
import useLoading from '../../../hooks/useLoading';

function SearchPanel(props) {
  const {
    filters, resource,
  } = props;
  const {
    setFilters, filterValues,
  } = useListContext();
  const translate = useTranslate();
  const { isLoading } = useLoading();

  const handleSearch = values => {
    const searchCriteria = {
      ...values,
      groupBy: values?.groupBy?.join(','),
    };
    setFilters(searchCriteria);
  };

  const guessFilterInput = filter => {
    const label = startCase(
      translate(`resources.report.fields.${camelCase(filter.label)}`),
    );
    const enumOptions = useEnumOptions(filter.reference, filter.source);
    const optionText = filter?.optionText?.[0] || 'name';
    switch (filter.inputType) {
      case 'reference':
        return (
          <Field
            key={filter?.source}
            name={filter.source}
          >
            {() => (
              <ReferenceInput
                fullWidth
                label={label}
                source={filter?.source}
                reference={filter?.reference}
                filter={filter?.filter}
                filterToQuery={value => ({
                  [filter?.optionText?.[0]]: value || '',
                })}
                variant="outlined"
                allowEmpty
              >
                <AutocompleteInput
                  emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
                  optionText={optionText}
                />
              </ReferenceInput>
            )}
          </Field>
        );
      case 'enum':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <SelectInput
                source={filter?.source}
                optionText="name"
                choices={enumOptions}
                allowEmpty
                variant="outlined"
                emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
              />
            )}
          </Field>
        );
      case 'date-range':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <DateRangeInput
                source={filter?.source}
                resource={resource}
                label={label}
                variant="outlined"
              />
            )}
          </Field>
        );
      case 'datetime':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <DateTimeInput
                source={filter?.source}
                resource={resource}
                label={label}
                variant="outlined"
              />
            )}
          </Field>
        );
      case 'number':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <NumberInput
                source={filter?.source}
                label={label}
                variant="outlined"
              />
            )}
          </Field>
        );
      case 'checkboxGroup':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <CheckBoxGroup
                source={filter?.source}
                label={label}
                choices={filter?.choices}
                resource={resource}
              />
            )}
          </Field>
        );
      case 'select':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <SelectInput
                allowEmpty
                source={filter?.source}
                label={label}
                choices={filter?.choices}
                variant="outlined"
                translateChoice
                emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
              />
            )}
          </Field>
        );
      case 'monthYear':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <MonthYearInput
                source={filter?.source}
                label={label}
              />
            )}
          </Field>
        );
      case 'radioGroup':
        return (
          <Field
            name={filter?.source}
            key={filter?.source}
          >
            {() => (
              <RadioButtonGroupInput
                source={filter?.source}
                label={label}
                choices={filter?.choices}
              />
            )}
          </Field>
        );
      default:
        return (
          <Field
            name={filter?.reference}
            key={filter?.source}
          >
            {() => (
              <TextInput
                source={filter?.source}
                label={label}
                variant="outlined"
              />
            )}
          </Field>
        );
    }
  };

  return (
    <Form
      initialValues={filterValues}
      onSubmit={(values, form) => handleSearch(values, form)}
      validate={() => {}}
      render={({
        handleSubmit, form,
      }) => (
        <form
          style={{
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Grid
            container
            spacing={1}
            display="flex"
            style={{
              gap: '1rem',
              paddingLeft: '8px',
              paddingRight: '8px',
            }}
          >
            {filters?.map(filter => (
              <Grid
                item
                key={filter.source}
              >
                {guessFilterInput(filter, form)}
              </Grid>
            ))}
          </Grid>
          <Box
            display="flex"
            justifyContent="flex-end"
            paddingLeft="8px"
            paddingRight="8px"
          >
            <Button
              variant="contained"
              color="primary"
              disabled={isLoading}
              onClick={() => {
                handleSubmit();
              }}
              label={translate('ra.action.search')}
              disableElevation
            >
              {isLoading ? <CircularProgress size="1rem" /> : <SearchIcon />}
            </Button>
          </Box>
        </form>
      )}
    />
  );
}

export default SearchPanel;
