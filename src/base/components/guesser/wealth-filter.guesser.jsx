/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/require-default-props */
import { kebabCase, sortBy } from 'lodash';
import PropTypes from 'prop-types';
import React, { memo } from 'react';
import {
  Filter,
  ReferenceInput,
  useListContext,
  useTranslate,
} from 'react-admin';
import { AutocompleteInput } from '../ra/inputs';
import { guessInputComponent } from './wealth-component.guesser';

const FilterGuesser = props => {
  const {
    properties, resource,
  } = props;
  const realResource = resource.includes('/')
    ? resource.split('/')[1]
    : resource;
  const context = useListContext();
  const usedProps = JSON.parse(JSON.stringify(props));
  const usedFilters = {};
  const translate = useTranslate();

  Object.keys(properties)
    .filter(key => !properties[key]?.properties?.hide && !key.startsWith('_'))
    .forEach(i => {
      usedFilters[i] = {
        ...properties[i],
        name: i,
      };
    });

  // Sort filter by "type" and "form", and move the boolean field to end
  const sorted = [
    ...sortBy(
      Object.values(usedFilters).filter(item => item.type !== 'boolean'),
      ['type', 'format'],
    ).reverse(),
    ...Object.values(usedFilters).filter(item => item.type === 'boolean'),
  ];

  const blackListFormat = ['code', 'richtext', 'multiline'];
  const notInBlacklistFormat = format => blackListFormat.includes(format) === false;

  return (
    <Filter {...usedProps}>
      {sorted
        .filter(
          source => notInBlacklistFormat(source.format)
            && !source.properties?.hideFilter?.default
            // eslint-disable-next-line no-underscore-dangle
            && (!usedFilters._metaData?.properties?.hideSystemFields
              // eslint-disable-next-line no-underscore-dangle
              || !usedFilters._docMeta?.properties?.systemFields?.default?.includes(
                source.name,
              )),
        )
        .map(source => {
          const sourceSchema = usedFilters?.[source.name];
          const {
            InputComponent, guessedProps,
          } = guessInputComponent(sourceSchema, {
            isFilter: true,
            translate,
          });

          const extendProps = source.name === 'docStatus'
            ? {
              onChange: e => {
                if (e.target.value) {
                  context.setFilters({
                    docStatus: `"${e.target.value}"`,
                  });
                }
              },
              parse: ids => (ids ? `"${ids}"` : ''),
              format: data => data?.replaceAll(/"/gi, '') || '',
            }
            : {};

          let sourceName = source.name;
          let refName;
          let optionText = 'name';
          let refFilter;
          let componentProps = {};
          let isReferenceInput;
          if (
            sourceSchema?.$ref
            || sourceSchema?.allOf?.filter(i => i?.$ref)?.[0]?.$ref
          ) {
            const name = source.name.split('.').pop();
            refName = (
              usedFilters[name].$ref
              || usedFilters[name]?.properties?.reference?.$ref
              || usedFilters[name]?.allOf?.filter(i => i?.$ref)?.[0]?.$ref
            )
              .split('/schemas/')
              .pop();
            if (refName.endsWith('Status')) {
              refFilter = {
                type: refName,
              };
              refName = 'Status';
            }
            if (refName === 'User') optionText = 'username';
            if (refName === 'Player') optionText = 'nativeId';
            sourceName = `${source.name}.id`;
            isReferenceInput = true;
          }

          if (sourceSchema?.type === 'number') {
            sourceName = `${sourceName}||$eq`;
          }

          componentProps = {
            label: `resources.${realResource}.fields.${source.name}`,
            key: sourceName,
            source: sourceName,
            alwaysOn: true,
          };

          if (isReferenceInput) {
            return (
              <ReferenceInput
                {...componentProps}
                reference={kebabCase(refName)}
                filter={refFilter}
                perPage={25}
                filterToQuery={value => ({
                  [optionText]: value || '',
                })}
              >
                <AutocompleteInput
                  emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
                  optionText={optionText}
                  suggestionLimit={10}
                  resettable
                />
              </ReferenceInput>
            );
          }

          if (sourceSchema?.type === 'array') {
            const refField = sourceSchema?.items?.$ref
              || sourceSchema?.items?.allOf?.filter(i => i?.$ref)?.[0]?.$ref;
            refName = kebabCase(refField?.split('/schemas/')?.pop());

            if (refName === 'user') optionText = 'username';

            componentProps = {
              label: `resources.${realResource}.fields.${source.name}`,
              key: sourceName,
              source: `${sourceName}.id||$cont`,
              alwaysOn: true,
            };

            return (
              <ReferenceInput
                {...componentProps}
                reference={kebabCase(refName)}
                filter={refFilter}
                perPage={25}
                filterToQuery={value => ({
                  [optionText]: value || '',
                })}
                allowEmpty
              >
                <AutocompleteInput
                  optionText={optionText}
                  suggestionLimit={10}
                  emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
                  resettable
                />
              </ReferenceInput>
            );
          }

          // Group
          if (sourceSchema?.properties?.reference?.$ref) {
            const ref = sourceSchema?.properties?.reference?.$ref;
            refName = kebabCase(ref?.split('/schemas/')?.pop());

            componentProps = {
              label: `resources.${refName}.name`,
              key: sourceName,
              source: `${sourceName}`,
              alwaysOn: true,
            };

            return (
              <ReferenceInput
                {...componentProps}
                reference={kebabCase(refName)}
                filter={refFilter}
                perPage={25}
                filterToQuery={value => ({
                  [optionText]: value || '',
                })}
              >
                <AutocompleteInput
                  optionText={optionText}
                  suggestionLimit={10}
                  emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
                  resettable
                />
              </ReferenceInput>
            );
          }

          return (
            <InputComponent
              {...componentProps}
              {...guessedProps}
              {...extendProps}
              allowEmpty
              resettable
            />
          );
        })}
    </Filter>
  );
};

FilterGuesser.propTypes = {
  properties: PropTypes.object,
  resource: PropTypes.string,
};

FilterGuesser.defaultProps = {
  properties: {},
};

export default memo(FilterGuesser);
