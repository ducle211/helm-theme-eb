import React, { useState } from 'react';
import { Button } from 'react-admin';
import EditIcon from '@material-ui/icons/Edit';
import BulkEditDialog from '../../bulk-edit';

const EditMultipleDataButton = props => {
  const [open, setOpen] = useState();

  const handleOpenBulkEdit = () => {
    setOpen(true);
  };

  return (
    <>
      <Button
        label="ra.action.bulkedit"
        onClick={handleOpenBulkEdit}
        color="primary"
      >
        <EditIcon />
      </Button>
      <BulkEditDialog
        open={open}
        setOpen={setOpen}
        {...props}
      />
    </>
  );
};

EditMultipleDataButton.propTypes = {};

EditMultipleDataButton.defaultProps = {};

export default EditMultipleDataButton;
