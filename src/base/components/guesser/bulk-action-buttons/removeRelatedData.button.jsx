import React from 'react';
import {
  Button,
  useRefresh,
  useNotify,
  useUnselectAll,
  useDataProvider,
} from 'react-admin';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { camelCase } from 'lodash';
import PropTypes from 'prop-types';

const RemoveRelatedDataButton = props => {
  const {
    selectedIds, refManyEditProps,
  } = props;
  const {
    referenceResource,
    editingResource,
    editingResourceId,
    isManyToMany,
    referenceResourceNameAsProperty,
  } = refManyEditProps || {};

  const refresh = useRefresh();
  const notify = useNotify();
  const unselectAll = useUnselectAll();
  const dataProvider = useDataProvider();

  // update one-to-many
  const updateOneToMany = async () => {
    try {
      await dataProvider.updateMany(
        referenceResource,
        {
          ids: selectedIds,
          data: {
            [camelCase(editingResource)]: {
              id: null,
            },
          },
        },
        {
          onSuccess: () => {
            refresh();
            notify('Updated!', 'success');
            unselectAll(referenceResource);
          },
          onFailure: () => {
            notify('Error!', 'error');
          },
        },
      );
    } catch {
      notify('Error!', 'error');
    }
  };

  // update many-to-many
  const updateManyToMany = async () => {
    try {
      const { data: editingResourceData } = await dataProvider.getOne(
        editingResource,
        {
          id: editingResourceId,
        },
      );
      const existedIds = editingResourceData?.[
        referenceResourceNameAsProperty
      ]?.map(d => d.id);
      const remainIds = existedIds
        ?.filter(id => !selectedIds?.includes(id))
        ?.map(id => ({
          id,
        }));

      await dataProvider.update(
        editingResource,
        {
          id: editingResourceId,
          data: {
            [referenceResourceNameAsProperty]: remainIds,
          },
          previousData: editingResourceData,
        },
        {
          onSuccess: () => {
            refresh();
            notify('Updated!', 'success');
            unselectAll(referenceResource);
          },
          onFailure: () => {
            notify('Error!', 'error');
          },
        },
      );
    } catch (error) {
      notify('Error!', 'error');
    }
  };

  const handleClick = () => {
    if (isManyToMany) {
      updateManyToMany();
    } else {
      updateOneToMany();
    }
  };

  return (
    <Button
      label="ra.action.remove"
      onClick={handleClick}
      color="secondary"
    >
      <RemoveCircleOutlineIcon />
    </Button>
  );
};

RemoveRelatedDataButton.propTypes = {
  selectedIds: PropTypes.arrayOf(PropTypes.string),
  // eslint-disable-next-line react/require-default-props
  refManyEditProps: PropTypes.shape({
    editingResource: PropTypes.string,
    editingResourceId: PropTypes.string,
    editingResourceNameAsProperty: PropTypes.string,
    referenceResource: PropTypes.string,
    referenceResourceNameAsProperty: PropTypes.string,
    isManyToMany: PropTypes.bool,
    refManyBulkAction: PropTypes.string,
    onToggleDrawer: PropTypes.func,
  }),
};

RemoveRelatedDataButton.defaultProps = {
  selectedIds: [],
};

export default RemoveRelatedDataButton;
