/* eslint-disable no-param-reassign */
import React from "react";
import Type from "prop-types";
import { Edit, SimpleForm } from "react-admin";

const WealthEdit = (props) => {
  const { children, required, redirect, ...rest } = props;

  const transform = (requiredFields) => (data) =>
    requiredFields.length
      ? requiredFields.reduce((result, field) => {
          result[field] = data?.[field];
          return result;
        }, {})
      : data;

  return (
    <Edit {...rest} undoable={false} transform={transform(required)}>
      <SimpleForm redirect={redirect}>{children}</SimpleForm>
    </Edit>
  );
};

WealthEdit.propTypes = {
  required: Type.arrayOf(Type.string),
  children: Type.oneOfType([Type.element, Type.arrayOf(Type.element)])
    .isRequired,
  redirect: Type.string, // "edit"|"show"|"list"|<custom route>
};

WealthEdit.defaultProps = {
  required: [],
  redirect: "show",
};

export default WealthEdit;
