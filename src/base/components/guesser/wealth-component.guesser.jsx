import {
  BooleanField,
  DateField,
  EmailField,
  ImageField,
  TextField,
  required,
  email,
} from 'react-admin';
import {
  ChipField,
  DocStatusField,
  IdField,
  NameField,
  MaskField,
  TuiViewerField,
  AmountField,
} from '../ra/fields';
import CodeField from '../ra/fields/code.field';
import LinkField from '../ra/fields/link.field';
import {
  CodeEditorInput,
  DateInput,
  DateTimeInput,
  NumberInput,
  SelectArrayInput,
  SelectInput,
  TextInput,
  TuiEditorInput,
  NullableBooleanInput,
  BooleanInput,
} from '../ra/inputs';
import { getRegexValidate } from '../ra/validate';
import DateRangeInput from './date-range-input';

export const guessFieldComponent = schema => {
  let PropComponent = TextField;
  const guessedPropsComponent = {};

  switch (schema?.type) {
    case 'boolean':
      PropComponent = BooleanField;
      break;
    case 'mask':
      PropComponent = MaskField;
      break;
    case 'string':
      switch (schema?.format) {
        case 'date':
        case 'datetime-local':
        case 'date-time':
          PropComponent = DateField;
          guessedPropsComponent.showTime = true;
          break;
        case 'email':
          PropComponent = EmailField;
          break;
        case 'ip':
        case 'chip':
          PropComponent = ChipField;
          break;
        case 'image':
          PropComponent = ImageField;
          break;
        case 'richtext':
          PropComponent = TuiViewerField;
          break;
        case 'id':
          PropComponent = IdField;
          break;
        case 'name':
          PropComponent = NameField;
          break;
        case 'currency':
          PropComponent = AmountField;
          break;
        default:
          PropComponent = TextField;
          break;
      }
      break;
    default:
      PropComponent = TextField;
      break;
  }

  if (schema?.format === 'link') {
    PropComponent = LinkField;
  }

  if (schema?.sourceName === 'docStatus') {
    PropComponent = DocStatusField;
  }

  if (schema?.format === 'code') {
    PropComponent = CodeField;
  }

  return {
    PropComponent,
    guessedPropsComponent,
  };
};

export const guessInputComponent = (schema, options = {}) => {
  const {
    isFilter,
    translate,
    isRequired,
  } = options;

  // Default input component
  let InputComponent = TextInput;

  const guessedProps = {
    valueformat: schema?.type,
    variant: 'outlined',
    validate: [],
  };

  if (!isFilter) {
    // Check and add regex validation
    const regexValidation = getRegexValidate(schema?.properties);
    if (!isFilter && regexValidation) {
      guessedProps.validate.push(regexValidation);
    }

    // Check and add required validation
    if (isRequired) {
      guessedProps.validate.push(required());
    }
  }

  if (typeof schema?.default !== 'undefined' && !isFilter) {
    guessedProps.defaultValue = schema.default;
  }

  switch (schema?.type) {
    case 'boolean':
      if (isFilter) {
        InputComponent = NullableBooleanInput;
        guessedProps.trueLabel = translate('ra.boolean.true');
        guessedProps.falseLabel = translate('ra.boolean.false');
        guessedProps.nullLabel = `--${translate('ra.text.any').toUpperCase()}--`;
      } else {
        InputComponent = BooleanInput;
      }
      break;

    case 'string':
      if (schema?.format) guessedProps.valueformat = schema?.format;

      // Don't validate email when using filter feature
      if (!isFilter && schema?.format === 'email') {
        guessedProps.valueformat = 'string';
        guessedProps.type = 'email';
        guessedProps.validate.push(email());
      }

      if (schema?.format === 'date') {
        InputComponent = DateInput;
      }

      if (
        schema?.format === 'datetime-local'
        || schema?.format === 'date-time'
      ) {
        InputComponent = DateTimeInput;
        guessedProps.valueformat = 'date';
        guessedProps.isFilter = isFilter;
      }

      if (
        ['date', 'datetime-local', 'date-time'].includes(schema?.format)
        && isFilter
      ) {
        InputComponent = DateRangeInput;
      }

      if (schema?.format === 'multiline') {
        guessedProps.multiline = true;
        guessedProps.rows = 5;
      }
      if (schema?.format === 'richtext') {
        InputComponent = TuiEditorInput;
        guessedProps.multiline = true;
        guessedProps.rows = 5;
      }
      if (schema?.enum) {
        guessedProps.valueformat = 'selection';
        InputComponent = SelectInput;
        if (schema?.format === 'multiple-choices') {
          InputComponent = SelectArrayInput;
        }
        guessedProps.choices = schema.enum.map(i => ({
          id: i,
          name: i,
        }));
        if (isFilter) {
          guessedProps.emptyText = `--${translate(
            'ra.text.any',
          ).toUpperCase()}--`;
        }
      }
      if (schema?.format === 'code') {
        InputComponent = CodeEditorInput;
      }
      break;

    case 'number':
      InputComponent = NumberInput;
      break;

    default:
      break;
  }

  return {
    InputComponent,
    guessedProps,
  };
};
