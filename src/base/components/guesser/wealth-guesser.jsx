import guessFieldComponentWithProp from './wealth-field.guesser';
import guessInputComponentWithProp from './wealth-input.guesser';

export const guessProperty = ({
  source, properties, apiRef, view, resource, resources, required, hasShow,
}) => {
  if (!properties?.[source]) {
    throw new Error('Unknown property', source, properties);
  }

  switch (view) {
    case 'list':
      return guessFieldComponentWithProp(
        {
          source,
          properties,
          apiRef,
          view,
          resource,
          resources,
          hasShow,
        },
      );
    case 'edit':
    case 'create':
      return guessInputComponentWithProp({
        source,
        properties,
        apiRef,
        view,
        resource,
        resources,
        required,
      });
    default:
      return guessFieldComponentWithProp({
        source,
        properties,
        apiRef,
        view,
        resource,
        resources,
        hasShow,
      });
  }
};
