/* eslint-disable */
/* eslint-disable react/require-default-props */
/* eslint-disable no-param-reassign */
import Axios from "axios";
import PropTypes from "prop-types";
import React from "react";
import {
  getResources,
  Loading,
  Tab,
  TabbedShowLayout,
  TabbedShowLayoutTabs,
  useShowController,
  useTranslate,
} from "react-admin";
import { useSelector } from "react-redux";
import { useSchema, useViewType } from '../../hooks';
import { EnumViewType } from '../../hooks/useViewType';
import Actions from "../action-menu/toolbar-menu";
import { Show } from "../ra/views";

import { guessProperty } from "./wealth-guesser";

const WealthShowGuesser = (props) => {
  useViewType(EnumViewType.SHOW);

  const resources = useSelector(getResources);
  const translate = useTranslate();

  const { record } = useShowController(props);
  const {
    basePath,
    hasShow,
    resource,
    actionMenu,
    extendsActionMenu,
    children,
    ...rest
  } = props;

  const {
    options: {
      resourceId,
      approvable,
      submittable,
    },
  } = resources.find((r) => r.name === resource);

  if (!hasShow) {
    throw new Error("This resource does not support show");
  }

  const { api, ref } = useSchema();

  if (api) {
    const { paths } = api;
    const responseSchema =
      paths?.[basePath]?.get?.responses?.["200"]?.content?.["application/json"]
        ?.schema;
    let responseComponent;
    let responseRef;
    if (responseSchema?.oneOf) {
      responseRef = responseSchema.oneOf.filter((r) => r.type === "array")?.[0]
        ?.items;

      responseRef =
        responseRef?.$ref ||
        responseRef?.allOf?.filter((i) => i?.$ref)?.[0]?.$ref;

      responseComponent = ref.get(responseRef);
    }
    const { properties } = responseComponent;

    Object.keys(properties)
      .filter((key) => properties[key]?.properties?.hide || key.startsWith("_"))
      .forEach((i) => {
        delete properties[i];
      });

    const components = Object.keys(properties)
      ?.filter(
        (prop) =>
          (properties?.[prop]?.type !== "array" ||
            (properties?.[prop]?.type === "array" && prop === "blobs")) &&
          prop !== "attachment"
      )
      ?.map((prop) =>
        guessProperty({
          source: prop,
          properties,
          apiRef: ref,
          resource,
          resources,
          hasShow,
        })
      );

    const tabComponents = Object.keys(properties)
      ?.filter(
        (prop) =>
          !properties?.[prop]?.deprecated &&
          properties?.[prop]?.type === "array" &&
          prop !== "blobs"
      )
      ?.map((prop) =>
        guessProperty({
          source: prop,
          properties,
          apiRef: ref,
          resource,
          resources,
          hasShow,
        })
      );

    const actions = actionMenu
      ? {
          actions: React.cloneElement(actionMenu, {
            basePath: basePath,
            record: record,
            resource: resource,
            submittable: submittable,
            approvable: approvable,
          }),
        }
      : {
          actions: (
            <Actions
              basePath={basePath}
              record={record}
              resource={resource}
              submittable={submittable}
              approvable={approvable}
            />
          ),
        };
    const realResource = resource.includes("/")
      ? resource.split("/")[1]
      : resource;

    return (
      <>
        <Show
          {...rest}
          resource={resource}
          basePath={basePath}
          title={translate("ra.page.show", {
            name: translate(`resources.${realResource}.name`),
            id: record?.id,
          })}
          actions={
            <CustomAction
              {...actions}
              extendsActionMenu={extendsActionMenu}
              basePath={basePath}
              record={record}
              resource={resource}
              submittable={submittable}
              approvable={approvable}
            />
          }
        >
          <TabbedShowLayout
            tabs={
              <TabbedShowLayoutTabs variant="scrollable" scrollButtons="auto" />
            }
          >
            <Tab label={translate("wa.common.general")} path="">
              {components}
              {children}
            </Tab>
            {tabComponents?.map((tab) => (
              <Tab
                key={tab.key}
                label={`resources.${tab.key}.name`}
                path={tab.key}
              >
                {tab}
              </Tab>
            ))}
          </TabbedShowLayout>
        </Show>
      </>
    );
  }
  return <Loading />;
};

const CustomAction = ({ actions, extendsActionMenu, ...props }) => (
  <div
    style={{
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    }}
  >
    {extendsActionMenu || <div />}
    <div
      style={{
        width: "100%",
      }}
    >
      {React.cloneElement(actions, {
        ...props,
      })}
    </div>
  </div>
);

WealthShowGuesser.propTypes = {
  basePath: PropTypes.string,
  hasShow: PropTypes.bool,
  // eslint-disable-next-line react/require-default-props
  resource: PropTypes.string,
  record: PropTypes.object,
  actionMenu: PropTypes.object,
  id: PropTypes.string.isRequired,
};

WealthShowGuesser.defaultProps = {
  basePath: null,
  hasShow: false,
  record: null,
  actionMenu: null,
};

export default WealthShowGuesser;
