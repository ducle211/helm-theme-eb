/* eslint-disable react/prop-types */
import TranslateIcon from '@material-ui/icons/Translate';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import MuiButton from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core';
import axios from 'axios';
import * as React from 'react';
import {
  Button,
  CreateButton,
  ExportButton,
  sanitizeListRestProps,
  TopToolbar,
  useListContext,
  useNotify,
  useTranslate,
} from 'react-admin';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import CustomTooltip from '../../custom-tooltip';

const useStyle = makeStyles(theme => ({
  showFilterBtn: {
    [theme.breakpoints.down('md')]: {
      backgroundColor: 'transparent',
    },
  },
}));

const ListActions = props => {
  const {
    className, translatable, resourceId, toggleFilterPanel, isDisplayFilterPanel, ...rest
  } = props;
  const {
    currentSort,
    resource,
    filterValues,
    hasCreate,
    basePath,
    total,
    exporter,
  } = useListContext();

  const notify = useNotify();
  const translate = useTranslate();
  const classes = useStyle();

  const handleTranslate = async () => {
    try {
      await axios.post('/api/translation/translate-resource', {
        resourceId,
      });
      notify('Success!', 'success');
    } catch (error) {
      if (!error?.response) {
        notify('Please try again later!', 'error');
      }
      notify(`Error! ${error?.response?.data?.message}`, 'error');
    }
  };

  return (
    <TopToolbar
      className={clsx(className, 'hidden-label-filter')}
      {...sanitizeListRestProps(rest)}
    >
      <MuiButton
        color="primary"
        variant="text"
        size="small"
        className={classes.showFilterBtn}
        startIcon={isDisplayFilterPanel ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
        onClick={toggleFilterPanel}
      >
        {isDisplayFilterPanel ? translate('ra.action.hideFilter') : translate('ra.action.showFilter')}
      </MuiButton>
      {hasCreate && (
        <CustomTooltip
          title={translate('ra.action.create')}
          placement="top"
        >
          <CreateButton
            basePath={basePath}
            label=""
          />
        </CustomTooltip>
      )}
      {exporter && (
        <CustomTooltip
          title={translate('ra.action.export')}
          placement="top"
        >
          <ExportButton
            disabled={total === 0}
            resource={resource}
            sort={currentSort}
            filterValues={filterValues}
            label=""
            maxResults={100000}
          />
        </CustomTooltip>
      )}
      {translatable && (
        <CustomTooltip
          title={translate('ra.action.translate')}
          placement="top"
        >
          <Button onClick={handleTranslate}>
            <TranslateIcon />
          </Button>
        </CustomTooltip>
      )}
    </TopToolbar>
  );
};

export default ListActions;

ListActions.propTypes = {
  toggleFilterPanel: PropTypes.func,
  isDisplayFilterPanel: PropTypes.bool,
};

ListActions.defaultProps = {
  toggleFilterPanel: f => f,
  isDisplayFilterPanel: false,
};
