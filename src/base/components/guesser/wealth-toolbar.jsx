/* eslint-disable react/prop-types */
/* eslint-disable react/require-default-props */
import { makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Toolbar } from 'react-admin';
import { useForm } from 'react-final-form';
import useLoading from '../../hooks/useLoading';
import DeleteButton from '../ra/buttons/DeleteButton';
import { SaveButton as SaveButtonRA } from '../ra/buttons';

const useStyles = makeStyles(
  theme => ({
    defaultToolbar: {
      flex: 1,
      display: 'flex',
      justifyContent: 'space-between',
      paddingRight: theme.spacing(1),
    },
    btnDelete: {
      backgroundColor: theme.palette.error.main,
      color: 'white',
      '&:hover': {
        backgroundColor: theme.palette.error.dark,
      },
    },
  }),
  {
    name: 'RaToolbar',
  },
);

const valueOrDefault = (value, defaultValue) => (typeof value === 'undefined' ? defaultValue : value);

const SaveButton = props => {
  const {
    alwaysEnableSaveButton, pristine, disabled,
  } = props;
  const disabledBtn = disabled || !valueOrDefault(alwaysEnableSaveButton, !pristine);

  return (
    <SaveButtonRA
      {...props}
      invalid={false}
      disabled={disabledBtn}
    />
  );
};

SaveButton.propTypes = {
  alwaysEnableSaveButton: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired,
};

export const WealthToolbar = props => {
  const classes = useStyles();
  const { toggleLoading } = useLoading();
  const form = useForm();

  const {
    disabledBtnSave,
    hasDelete,
    alwaysEnableSaveButton,
    onCustomSave,
    ...rest
  } = props;

  const { record } = rest;

  useEffect(() => {
    toggleLoading(rest.saving);
    return () => toggleLoading(false);
  }, [rest.saving]);

  return (
    <>
      <Toolbar {...rest}>
        <div className={classes.defaultToolbar}>
          {record && typeof record.id !== 'undefined' && hasDelete ? (
            <DeleteButton
              {...rest}
              className={classes.btnDelete}
              redirect="list"
            />
          ) : (
            <div />
          )}
          <SaveButton
            {...rest}
            disabled={disabledBtnSave}
            alwaysEnableSaveButton={alwaysEnableSaveButton}
            {...(onCustomSave && {
              onSave: (values, redirect) => onCustomSave(values, redirect, form),
            })}
          />
        </div>
      </Toolbar>
    </>
  );
};

WealthToolbar.propTypes = {
  record: PropTypes.object,
  hasDelete: PropTypes.bool,
};

WealthToolbar.defaultProps = {
  record: {},
  hasDelete: null,
};
