import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { DateRangePicker, LocalizationProvider } from '@material-ui/pickers';
import { InputAdornment } from '@material-ui/core';
import DateRangeIcon from '@material-ui/icons/DateRange';
import { useListContext, useInput, useLocale } from 'react-admin';
import * as moment from 'moment';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import queryString from 'querystring';
import MomentAdapter from '@material-ui/pickers/adapter/moment';
import ClearIcon from '@material-ui/icons/Clear';
import { get } from 'lodash';
import { TextInput } from '../../ra/inputs';

moment.locale('en');
const localLocale = moment();

export const prefixFilterDateRange = '**||between||**';

const formatDate = (date, dateFormat) => {
  if (!date) return '';

  // Format this date to `L`, it's meaning format date with current locale
  return (moment(date, dateFormat).format('L'));
};

export const convertDate = (value, startDay) => {
  if (startDay) {
    return moment(value)
      .startOf('day')
      .toISOString();
  }
  return moment(value)
    .endOf('day')
    .toISOString();
};

const DateRangeInput = props => {
  const {
    source, resource, label, isFilter,
  } = props;
  const [dateRange, setDateRange] = useState([null, null]);
  const [openDialog, setOpenDialog] = useState(false);
  const [dateFormat, setDateFormat] = useState();
  const context = useListContext();
  const history = useHistory();
  const { input: { onChange } } = useInput(props);
  let locale = useLocale();

  if (locale === 'cn') locale = 'zh-cn';

  const paramsFilter = useSelector(
    state => state.admin.resources[resource]?.list.params,
  );

  const handleClearDateRange = e => {
    e.stopPropagation();
    setDateRange([null, null]);
    onChange('');
  };

  const ClearRangeComponent = () => ((dateRange[0] || dateRange[1]) ? (
    <ClearIcon
      style={{
        cursor: 'pointer',
        color: 'rgba(0, 0, 0, 0.26)',
        width: '16px',
        height: '16px',
        margin: '0 6px',
      }}
      onClick={e => handleClearDateRange(e)}
    />
  ) : null);

  useEffect(() => {
    if (dateRange[0] && dateRange[1]) {
      setOpenDialog(false);

      if (onChange) {
        onChange(
          `${prefixFilterDateRange}${convertDate(
            dateRange[0],
            true,
          )},${convertDate(dateRange[1])}`,
        );
      }

      if (isFilter) {
        context.setFilters({
          ...context.filterValues,
          [source]: `${prefixFilterDateRange}${convertDate(
            dateRange[0],
            true,
          )},${convertDate(dateRange[1])}`,
        });
      }
    }
  }, [dateRange]);

  useEffect(() => {
    const { filter } = queryString.parse(history.location.search.substring(1)) || {};
    const objFilter = filter ? JSON.parse(filter) : {};
    const dateRangeUrl = objFilter?.[source] || paramsFilter?.filter?.[source];
    const rangedate = dateRangeUrl?.split(prefixFilterDateRange)?.[1] || '';
    const [startDate, endDate] = rangedate?.split(',') || [];

    if (startDate && endDate) {
      setDateRange([moment(startDate), moment(endDate)]);
    }
  }, []);

  useEffect(() => {
    // Get date format depend on locale
    // Example
    // + locale of `th`: DD/MM/YYYY
    // + locale of `en` or `cn`: MM/DD/YYYY
    const currentFormat = get(
      localLocale.locale(locale),
      '_locale._config.longDateFormat.L',
      'MM/DD/YYYY',
    );

    setDateFormat(currentFormat);
  }, [locale]);

  return (
    <LocalizationProvider
      dateLibInstance={moment}
      dateAdapter={MomentAdapter}
      locale={locale}
    >
      <DateRangePicker
        startText=""
        endText=""
        value={dateRange}
        open={openDialog}
        disableMaskedInput
        onChange={newRange => setDateRange(newRange)}
        onClose={() => setOpenDialog(false)}
        renderInput={(startProps, endProps) => {
          const start = formatDate(startProps?.inputProps?.value, dateFormat);
          const end = formatDate(endProps?.inputProps?.value, dateFormat);
          return (
            <TextInput
              className="date-range-input"
              source={source}
              label={label}
              onClick={() => setOpenDialog(true)}
              fullWidth
              autoComplete="off"
              helperText={false}
              onChange={() => null}
              parse={() => null}
              InputProps={{
                endAdornment: (
                  <InputAdornment>
                    <ClearRangeComponent />
                    <DateRangeIcon />
                  </InputAdornment>
                ),
              }}
              placeholder={`${dateFormat} ~ ${dateFormat}`}
              format={() => (start || end ? `${start} ~ ${end}` : '')}
            />
          );
        }}
      />
    </LocalizationProvider>
  );
};

DateRangeInput.propTypes = {
  source: PropTypes.string.isRequired,
  resource: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  isFilter: PropTypes.bool,
};

DateRangeInput.defaultProps = {
  isFilter: false,
};

export default memo(DateRangeInput);
