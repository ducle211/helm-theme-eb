/* eslint-disable no-shadow, no-underscore-dangle, react/prop-types */
import PropTypes from 'prop-types';
import React, { memo, useMemo } from 'react';
import { EditButton, getResources, Loading, useTranslate } from 'react-admin';
import { useSelector } from 'react-redux';
import { useSchema, useViewType } from '../../hooks';
import { EnumViewType } from '../../hooks/useViewType';
import Datagrid from '../ra/data-grid';
import Pagination from '../customizable-colums/Pagination';
import { IdField } from '../ra/fields';
import List from '../ra/list/List';
import ListActions from './actions/list.action';
import ReportListGuesser from './report/report-list.guesser';
import WealthCustomQueryList from './wealth-custom-query-list';
import WealthEmptyPage from './wealth-empty-page';
import FilterGuesser from './wealth-filter.guesser';
import { guessProperty } from './wealth-guesser';
import RowActionField from '../ra/fields/row-action-field/row-action.field';
import exporters from '../ra/list/exporters';

const WealthListGuesserType = {
  default: 'default',
  custom: 'custom',
};

const WealthListGuesserComponent = memo(props => {
  useViewType(EnumViewType.LIST);

  const translate = useTranslate();
  const {
    api, ref,
  } = useSchema();

  const {
    basePath,
    hasList,
    resource,
    children,
    rowClick,
    hasEdit,
    hasShow,
    refManyEditGuesser,
    quickEdit, // edit button in edit view
    refManyEditAddButton,
    listType,
    optimized,
    permissions,
    resources,
    rowAction,
    hasCreate,
    extraActions,
    ...rest
  } = props;

  const {
    options: {
      hasDelete, reportInterface, translatable, resourceId,
    },
  } = resources?.filter(r => r.name === resource)?.[0];

  const {
    components, properties, required,
  } = useMemo(() => {
    if (!api) return {};

    const { paths } = api;
    const responseSchema = paths?.[basePath]?.get?.responses?.['200']?.content?.['application/json']
      ?.schema;
    let responseComponent;
    let responseRef;
    if (responseSchema?.oneOf) {
      responseRef = responseSchema.oneOf.filter(r => r.type === 'array')?.[0]
        ?.items;

      responseRef = responseRef?.$ref
        || responseRef?.allOf?.filter(i => i?.$ref)?.[0]?.$ref;

      responseComponent = ref.get(responseRef);
    }

    const {
      properties, required,
    } = responseComponent;

    Object.keys(properties)
      .filter(
        key => properties[key]?.properties?.hide
          || properties._metaData?.properties?.hiddenFields?.default?.includes(
            key,
          )
          || properties._metaMerchantData?.properties?.hiddenFields?.default?.includes(
            key,
          ),
      )
      .forEach(i => {
        delete properties[i];
      });

    // skip custom properties passed to children
    let components = Object.keys(properties)
      ?.filter(
        property => !properties[property]?.properties?.hideList
          && !property.startsWith('_')
          && (children?.length > 0
            ? !children.map(child => child.props.source).includes(property)
            : children?.props?.source !== property),
      )
      ?.map(property => guessProperty({
        source: property,
        properties,
        required,
        apiRef: ref,
        view: 'list',
        resource,
        resources,
        hasShow,
      }));

    if (properties._metaData) {
      const metaProps = properties._metaData?.properties;
      if (
        metaProps.hideSystemFields
        && properties._docMeta?.properties?.systemFields?.default?.length > 0
      ) {
        components = components.filter(
          i => !properties._docMeta?.properties?.systemFields?.default.includes(
            i.props.source,
          ),
        );
      }
    }

    return {
      components,
      properties,
      required,
    };
  }, [api, ref, children, resource, resources?.length]);

  if (!hasList) {
    throw new Error('This resource does not support list');
  }

  if (!api) {
    return <Loading />;
  }

  const defaultSort = {
    field: 'created',
    order: 'DESC',
  };
  // delete metaData
  const ListComponent = listType === WealthListGuesserType.custom ? WealthCustomQueryList : List;
  return (
    <>
      {reportInterface ? (
        <ReportListGuesser
          hasList={hasList}
          hasShow={hasShow}
          hasEdit={hasEdit}
          hasCreate={hasCreate}
          basePath={basePath}
          resource={resource}
          perPage={25}
          reportInterface={reportInterface}
          pagination={<Pagination />}
          permissions={permissions}
          {...rest}
        />
      ) : (
        <>
          <ListComponent
            hasList={hasList}
            hasShow={hasShow}
            hasEdit={hasEdit}
            hasCreate={hasCreate}
            basePath={basePath}
            resource={resource}
            empty={<WealthEmptyPage />}
            sort={defaultSort}
            isDisplayFilterPanel={false}
            title={
              resource.includes('/')
                ? translate(`resources.${resource.replace('/', '.')}.name`)
                : translate(`resources.${resource}.name`)
            }
            filters={<FilterGuesser properties={properties} />}
            perPage={25}
            bulkActionButtons={false}
            actions={(
              <ListActions
                properties={properties}
                resourceId={resourceId}
                translatable={translatable}
                permissions={permissions}
              />
            )}
            pagination={<Pagination />}
            permissions={permissions}
            exporter={
              Object.keys(exporters).includes(resource)
                ? exporters[resource]
                : false
            }
            {...rest}
          >
            <Datagrid
              defaultColumns={required
                ?.filter(i => properties?.[i]?.format !== 'richtext')
                .filter(key => key !== 'buildIn')}
              rowClick={refManyEditGuesser ? '' : rowClick}
              refManyEditAddButton={refManyEditAddButton}
              optimized={optimized}
            >
              <IdField
                source="id"
                hasShow={hasShow}
              />
              {components}
              {children}
              {rowAction
                && React.cloneElement(rowAction, {
                  ...props,
                })}
              {(hasDelete || extraActions) && (
                <RowActionField
                  label={translate('ra.field.action')}
                  hasDelete={hasDelete}
                  extraActions={extraActions}
                />
              )}
              {quickEdit && <EditButton />}
            </Datagrid>
          </ListComponent>
        </>
      )}
    </>
  );
});

const WealthListGuesser = props => {
  const originalResources = useSelector(getResources);

  const resources = useMemo(() => originalResources, [
    originalResources?.length,
  ]);

  return (
    <WealthListGuesserComponent
      {...props}
      resources={resources}
    />
  );
};

WealthListGuesser.propTypes = {
  hasList: PropTypes.bool,
  basePath: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]),
  resource: PropTypes.string.isRequired,
  rowClick: PropTypes.string,
  hasEdit: PropTypes.bool,
  hasShow: PropTypes.bool,
  refManyEditGuesser: PropTypes.bool,
  quickEdit: PropTypes.bool,
  // eslint-disable-next-line react/require-default-props
  refManyEditAddButton: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.bool,
  ]),
  listType: PropTypes.oneOf([
    WealthListGuesserType.default,
    WealthListGuesserType.custom,
  ]),
  optimized: PropTypes.bool,
  permissions: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};

WealthListGuesser.defaultProps = {
  hasList: false,
  basePath: null,
  children: null,
  rowClick: '',
  hasEdit: false,
  hasShow: false,
  refManyEditGuesser: false,
  quickEdit: false,
  listType: WealthListGuesserType.default,
  optimized: false,
  permissions: [],
};

export default React.memo(WealthListGuesser);
