/* eslint-disable */
import { Box, makeStyles } from "@material-ui/core";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import { kebabCase, get } from "lodash";
import PropTypes from "prop-types";
import * as React from "react";
import {
  ArrayField,
  BooleanField,
  Datagrid,
  ImageField,
  NumberField,
  ReferenceField,
  SingleFieldList,
  TextField,
  useNotify,
  useTranslate,
} from "react-admin";
import { JsonField } from "react-admin-json-view";
import { ChipField, IdField, RefField } from "../ra/fields";
import CustomLabeled from "../ra/labeled";
import { guessFieldComponent } from "./wealth-component.guesser";

const getSourceRefTitle = (source, properties) => {
  // Use defined `optionText` as ref title
  const definedTitle = Object.keys(properties)?.filter(
    (key) => properties?.[key]?.properties?.optionText?.default
  )?.[0];
  if (definedTitle) return definedTitle;

  // if source ref contains property 'name' use 'name' for reference field
  if (Object.keys(properties).includes("name")) {
    return "name";
  }

  if (Object.keys(properties).includes("username")) {
    return "username";
  }

  if (Object.keys(properties).includes("presignedUrl")) {
    return "presignedUrl";
  }

  // return id for default reference field
  return "id";
};

const useStyles = makeStyles(() => ({
  btn: {
    cursor: "pointer",
  },
}));

const CustomLabel = React.memo((props) => {
  const classes = useStyles();
  const notify = useNotify();
  const translate = useTranslate();
  const { realResource, record, source, isCopy } = props;

  return (
    <Box display="inline-flex" alignItems="center">
      <span>{translate(`resources.${realResource}.fields.${source}`)}</span>

      {isCopy && (
        <Box
          display="inline"
          ml={3}
          className={classes.btn}
          onClick={(e) => {
            e.preventDefault();
            navigator.clipboard.writeText(record[source]);
            notify("Copied", "success");
          }}
        >
          <FileCopyIcon />
        </Box>
      )}
    </Box>
  );
});

CustomLabel.propTypes = {
  realResource: PropTypes.string.isRequired,
  record: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
  isCopy: PropTypes.object,
};

CustomLabel.defaultProps = {
  isCopy: undefined,
};

const parseSourceRef = (source, properties, apiRef) => {
  const name = source.split(".").pop();
  const sourceSchema = properties?.[name];
  const fieldRef =
    sourceSchema.$ref ||
    sourceSchema?.allOf?.filter((i) => i?.$ref)?.[0]?.$ref ||
    sourceSchema?.properties?.reference?.$ref;
  const sourceRef = apiRef.get(fieldRef);

  let sourceName = source;
  let sourceRefTitle = getSourceRefTitle(source, sourceRef.properties);
  let sourceRefTitleSchema = sourceRef.properties?.[sourceRefTitle];
  let refName = fieldRef.split("/schemas/").pop();

  if (!sourceSchema?.properties?.reference?.$ref) {
    sourceName = `${source}.id`;
  }

  if (sourceRefTitle === "id") {
    sourceRefTitleSchema = {
      type: "string",
      format: "id",
    };
  }

  if (refName.toLowerCase().endsWith("status")) {
    refName = 'status',
    sourceRefTitleSchema = {
      type: "string",
      format: "chip",
    };
  }

  const {
    PropComponent: FieldComponent,
    guessedPropsComponent,
  } = guessFieldComponent(sourceRefTitleSchema);

  if (refName.toLowerCase().endsWith("user")) {
    sourceRefTitle = "username";
    sourceName = "user.id";
  }

  return {
    source: sourceRefTitle,
    SourceReferenceField: {
      label: source,
      source: sourceName,
      reference: refName,
    },
    FieldComponent,
    guessedPropsComponent,
  };
};

const guessFieldComponentWithPropUseStyle = makeStyles(theme => ({
  jsonHeaderStyle: {
    minWidth: 180,
    [theme.breakpoints.down('sm')]: {
      minWidth: 250,
    },
  },
  jsonStyle: {
    textAlign: 'left',
    width: 'fit-content',
    paddingLeft: 20,
    [theme.breakpoints.down('sm')]: {
      paddingLeft: 10,
    },
  }
}));

// @TODO: REFACTOR use constant for hardcode (view, type, format)
const guessFieldComponentWithProp = ({
  source,
  properties,
  apiRef,
  view = "show",
  resource,
  resources, // eslint-disable-line no-unused-vars
  hasShow,
}) => {
  const sourceSchema = properties?.[source];
  if (sourceSchema?.properties?.hide?.default) return;
  const realResource = resource.includes("/")
    ? resource.split("/")[1]
    : resource;

  if (source === "name" && hasShow) {
    sourceSchema.format = "name";
  }

  let {
    PropComponent: FieldComponent,
    guessedPropsComponent,
  } = guessFieldComponent(sourceSchema);

  let sourceName = source;
  let SourceReferenceField;
  let className = "";
  const type = sourceSchema?.format;
  const isJSON = type === "json";
  const collapseJSON = !!sourceSchema?.properties?.collapse;

  // ref to a defined object type
  const fieldRef =
    sourceSchema?.$ref ||
    sourceSchema?.allOf?.filter((i) => i?.$ref)?.[0]?.$ref ||
    sourceSchema?.properties?.reference?.$ref;

  const customRef = sourceSchema?.properties?.reference?.$ref;

  if (fieldRef) {
    const sourceRef = parseSourceRef(source, properties, apiRef);
    sourceName = sourceRef.source;
    FieldComponent = sourceRef.FieldComponent;
    SourceReferenceField = sourceRef.SourceReferenceField;
    guessedPropsComponent = sourceRef.guessedPropsComponent;
    if (view === "list" && sourceName === "presignedUrl") {
      className = "custom-image-field";
    }
  }

  // Guess ReferenceField
  if (customRef) {
    const realRef = resources?.find((r) => {
      if (r.name.includes("/")) {
        return r.name === kebabCase(SourceReferenceField.reference);
      }

      return false;
    });

    return (
      <ReferenceField
        key={sourceName}
        source={SourceReferenceField.source}
        reference={
          sourceSchema?.properties?.alias?.default || // custom reference resource
          realRef?.name ||
          kebabCase(SourceReferenceField.reference)
        }
        label={`resources.${realResource}.fields.${source}`}
        link="show"
      >
        <FieldComponent
          source={sourceName}
          className={className}
          {...guessedPropsComponent}
        />
      </ReferenceField>
    );
  }

  // Guess ReferenceField
  if (SourceReferenceField) {
    return (
      <ReferenceField
        key={sourceName}
        source={SourceReferenceField.source}
        label={`resources.${realResource}.fields.${source}`}
        reference={kebabCase(SourceReferenceField.reference)}
        link="show"
      >
        <FieldComponent
          source={sourceName}
          className={className}
          {...guessedPropsComponent}
        />
      </ReferenceField>
    );
  }

  // Guess ArrayField
  if (sourceSchema?.type === "array") {
    const refField =
      sourceSchema?.items?.$ref ||
      sourceSchema?.items?.allOf?.filter((i) => i?.$ref)?.[0]?.$ref;
    const refName = kebabCase(refField?.split("/schemas/")?.pop());

    let chipSource = "name";
    if (refName === "user") chipSource = "username";
    if (refName === "affiliate") chipSource = "email";

    const DatagridContainer = (props) => {
      const {
        data, // eslint-disable-line react/prop-types
        ids, // eslint-disable-line react/prop-types
        reference, // eslint-disable-line react/prop-types
      } = props;

      const classes = guessFieldComponentWithPropUseStyle();

      const fields = Object.keys(data?.[ids?.[0]] || {});
      let requiredFields = [...fields];
      let tmpResource = resource;
      if (refField) {
        const sourceRef = apiRef.get(refField);
        requiredFields = fields.filter((field) =>
          sourceRef.required.includes(field)
        );
        if (reference) {
          tmpResource = reference;
        }
      }

      const fieldsComp = requiredFields.map((field) => {
        const fieldValue = data?.[ids?.[0]]?.[field];
        let fieldType = fieldValue;

        if (isJSON) {
          fieldType = 'json';
        }

        switch (fieldType) {
          case "json":
            const isJsonString = (typeof jsonData === 'string');
            return (
              <JsonField
                key={field}
                label={`resources.${tmpResource}.fields.${field}`}
                source={field}
                addLabel={true}
                jsonString={isJsonString}
                reactJsonOptions={{
                  name: null,
                  collapsed: collapseJSON,
                  collapsed: true,
                  enableClipboard: true,
                  displayDataTypes: false,
                }}
              />
            );
          case "number":
            return (
              <NumberField
                source={field}
                key={field}
                label={`resources.${tmpResource}.fields.${field}`}
              />
            );
          case "boolean":
            return (
              <BooleanField
                source={field}
                key={field}
                label={`resources.${tmpResource}.fields.${field}`}
              />
            );
          case "object":
            if (!data?.[ids?.[0]]?.[field]) {
              return (
                <TextField
                  source={field}
                  key={field}
                  label={`resources.${tmpResource}.fields.${field}`}
                />
              );
            }
            // eslint-disable-next-line react/prop-types
            if (data?.[ids?.[0]]?.[field]?.presignedUrl) {
              return (
                <RefField
                  key={field}
                  source={`${field}`}
                  label={`resources.${tmpResource}.fields.${field}`}
                >
                  <ImageField source="presignedUrl" className={className} />
                </RefField>
              );
            }
            return (
              <RefField
                key={field}
                source={`${field}.id`}
                reference={field}
                label={`resources.${tmpResource}.fields.${field}`}
                link="show"
              >
                <FieldComponent source="name" {...guessedPropsComponent} />
              </RefField>
            );
          default:
            return (
              <TextField
                source={field}
                key={field}
                label={`resources.${tmpResource}.fields.${field}`}
              />
            );
        }
      });

      return (
        <div
          style={{
            overflowX: "auto",
            width: "calc(100% - 48px)",
          }}
        >
          <Datagrid {...props} optimized>
            <RefField
              label={`resources.${tmpResource}.fields.id`}
              source={`${tmpResource}.id`}
              reference={tmpResource}
            >
              <IdField source="id" hasShow={hasShow} resource={tmpResource} />
            </RefField>
            {fieldsComp}
          </Datagrid>
        </div>
      );
    };

    return (
      <ArrayField
        key={refName}
        source={source}
        reference={refName}
        label={`resources.${realResource}.fields.${source}`}
      >
        {view === "list" ? (
          <SingleFieldList linkType={false} classes={{ root: 'align-center' }}>
            <ChipField clickable={false} source={chipSource} />
          </SingleFieldList>
        ) : (
          <DatagridContainer />
        )}
      </ArrayField>
    );
  }

  // Guest Normal Field
  const FieldContainer = ({ children, ...props }) => {
    let newProps = props || {};
    const { record } = props;
    let schema = {
      ...sourceSchema,
      sourceName,
    };

    // Guess Normal Field by "value" for "setting" resource
    if (resource === "setting" && sourceName === "value") {
      schema = {
        type: "string",
        format: record?.format,
      };

      if (record?.maskMe) {
        schema = {
          type: "mask",
        };
      } else if (record?.format === "boolean") {
        schema = {
          type: record?.format,
        };
      }

      if (record?.format === "blob") {
        schema = {
          type: "string",
          format: "image",
        };
      }
    }

    let {
      PropComponent: Component,
      guessedPropsComponent,
    } = guessFieldComponent(schema);

    if (isJSON) {
      const jsonData = get(record, source);
      const isJsonString = (typeof jsonData === 'string');
      Component = JsonField;
      
      newProps = {
        ...props,
        addLabel: true,
        jsonString: isJsonString,
        reactJsonOptions: {
          // Props passed to react-json-view
          name: null,
          collapsed: collapseJSON,
          collapsed: true,
          enableClipboard: true,
          displayDataTypes: false,
        },
      };
    }

    return children({
      props: newProps,
      isCopy: schema?.properties?.copiable?.default,
      record,
      Component,
      guessedPropsComponent,
    });
  };

  return (
    <FieldContainer
      key={sourceName}
      source={sourceName}
      label={`resources.${realResource}.fields.${source}`}
      tooltip={sourceSchema?.description}
    >
      {({ props, isCopy, record, Component, guessedPropsComponent }) => (
        <>
          {view === "list" ? (
            <Component {...props} record={record} {...guessedPropsComponent} />
          ) : (
            <CustomLabeled
              label={
                <CustomLabel
                  realResource={realResource}
                  source={source}
                  isCopy={isCopy}
                  record={record}
                />
              }
            >
              <Component
                {...props}
                record={record}
                {...guessedPropsComponent}
              />
            </CustomLabeled>
          )}
        </>
      )}
    </FieldContainer>
  );
};

export default guessFieldComponentWithProp;
