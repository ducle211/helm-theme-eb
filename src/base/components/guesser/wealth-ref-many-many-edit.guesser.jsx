import React, { useEffect, useState } from 'react';
import { Loading } from 'react-admin';
import PropTypes from 'prop-types';
import { camelCase, isEmpty } from 'lodash';
import WealthListGuesser from './wealth-list.guesser';

export function RefManyManyEditGuesserList({
  refManyEditProps, ...listProps
}) {
  const {
    referenceResource,
    editingResource,
    editingResourceId,
    isManyToMany,
    editingResourceNameAsProperty,
  } = refManyEditProps;

  // mock props to use List component
  const [mockProps, setMockProps] = useState({
    basePath: `/${referenceResource}`,
    hasCreate: false,
    hasEdit: false,
    hasList: true,
    hasShow: false,
    history: {},
    location: {
      pathname: '/',
      search: '',
      hash: '',
      state: undefined,
    },
    match: {
      path: '/',
      url: '/',
      isExact: true,
      params: {},
    },
    options: {},
    permissions: null,
    resource: referenceResource,
    bulkActionButtons: false,
    actions: false,
    exporter: false,
    filters: <></>,
  });

  // handle set permanent filter
  useEffect(() => {
    if (isManyToMany !== undefined) {
      let permanentFilter;
      const name = [camelCase(editingResource)];
      if (isManyToMany) {
        permanentFilter = {
          [editingResourceNameAsProperty]: {
            'id||$eq': editingResourceId,
          },
        };
      } else {
        permanentFilter = {
          [name]: {
            id: editingResourceId,
          },
        };
      }

      setMockProps({
        ...mockProps,
        basePath: `/${referenceResource}`,
        resource: referenceResource,
        filter: permanentFilter,
      });
    }
  }, [isManyToMany, refManyEditProps.referenceResource]);

  return !isEmpty(mockProps?.filter) ? (
    <WealthListGuesser
      {...listProps}
      {...mockProps}
      listType="custom"
      optimized
      refManyEditGuesser
      quickEdit
    />
  ) : (
    <Loading />
  );
}

RefManyManyEditGuesserList.propTypes = {
  // eslint-disable-next-line react/require-default-props
  refManyEditProps: PropTypes.shape({
    editingResource: PropTypes.string,
    editingResourceId: PropTypes.string,
    editingResourceNameAsProperty: PropTypes.string,
    referenceResource: PropTypes.string,
    isManyToMany: PropTypes.bool,
    onToggleDrawer: PropTypes.func,
  }),
};
