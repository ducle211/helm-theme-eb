/* eslint-disable no-shadow */
import { kebabCase, get } from 'lodash';
import * as React from 'react';
import {
  email,
  ImageField,
  ImageInput,
  number,
  ReferenceArrayInput,
  ReferenceInput,
  regex,
  required,
  useTranslate,
  minLength,
  maxLength,
} from 'react-admin';
import { useFormState, useForm } from 'react-final-form';
import { useSelector } from 'react-redux';
import {
  AutocompleteArrayInput,
  AutocompleteInput,
  BooleanInput,
  SelectInput,
  JsonInput,
} from '../ra/inputs';
import { guessInputComponent } from './wealth-component.guesser';

const checkTheOtherValidate = ({
  listExtraValidate,
  currValidate,
  sourceSchema,
}) => listExtraValidate.reduce((result, currItem) => {
  const scheDataByType = get(sourceSchema, currItem.type);
  if (scheDataByType) {
    const {
      validation: {
        hasParams, action,
      },
    } = currItem;
    const validateAction = hasParams ? action(scheDataByType) : action();
    result.push(validateAction);
  }
  return result;
}, currValidate || []);

const listOtherValidate = [
  {
    type: 'minLength',
    validation: {
      action: minLength,
      hasParams: true,
    },
  },
  {
    type: 'maxLength',
    validation: {
      action: maxLength,
      hasParams: true,
    },
  },
];

const getRefFilter = (refName, refFilter, sourceSchema, values, user, form) => {
  const tmpRefFilter = {
    ...refFilter,
  };

  const filterOn = sourceSchema?.properties?.filterOn?.properties;
  const filterOnProps = Object.keys(filterOn || {});
  if (filterOn) {
    filterOnProps.forEach(f => {
      const value = values?.[filterOn[f].default];
      const paramFilter = filterOn[f].format;

      if (user?.role?.type === 'GROUP') {
        if (['groupId', 'group'].includes(f)) {
          form.change('groupId', user?.groupId);
          tmpRefFilter[paramFilter || 'groupId'] = user?.groupId;
        }
      }

      if (value) {
        if (Array.isArray(value)) {
          const arrId = value.map(i => i.id).join(',');
          if (paramFilter) {
            tmpRefFilter[paramFilter] = arrId;
          } else {
            tmpRefFilter[`${f}||$in`] = arrId;
          }
        } else if (['groupId', 'group'].includes(f)) {
          if (refName === 'user') {
            tmpRefFilter.group = {
              id: value?.id || value,
            };
          } else {
            tmpRefFilter[paramFilter || 'groupId'] = value?.id || value || user?.groupId;
          }
        } else if (value?.id) {
          tmpRefFilter[f] = {
            id: value.id,
          };
        } else {
          tmpRefFilter[f] = value;
        }
      }
    });
  }

  const filterOnRef = sourceSchema?.properties?.filterOnRef?.properties;
  const filterOnRefProps = Object.keys(filterOnRef || {});
  if (filterOnRef) {
    filterOnRefProps.forEach(f => {
      const v = values?.[filterOnRef[f].default];
      if (v?.id) {
        tmpRefFilter[f] = v.id;
      } else if (v) {
        tmpRefFilter[f] = v;
      }
    });
  }

  const filter = sourceSchema?.properties?.filter?.properties;
  const filterProps = Object.keys(filter || {});
  if (filter) {
    filterProps.forEach(f => {
      tmpRefFilter[f] = filter[f].default;
    });
  }

  return tmpRefFilter;
};

const guessInputComponentWithProp = ({
  source,
  properties,
  apiRef,
  resource,
  resources,
  required: requiredField = [],
}) => {
  let sourceName = source;
  let refName;
  let isReferenceInput;
  let isImage;
  let refFilter = {};
  let optionText = 'name';

  const realResource = resource.split('/').pop();
  const sourceSchema = properties?.[source];
  const type = sourceSchema?.format;
  const isJSON = type === 'json';
  const isRequired = requiredField.includes(sourceName);

  // find $ref
  const fieldRef = sourceSchema?.$ref
    || sourceSchema?.allOf?.filter(i => i?.$ref)?.[0]?.$ref
    || sourceSchema?.properties?.reference?.$ref;

  // Guess ReferenceInput (one/many to one)
  // ref to a defined object type
  if (fieldRef) {
    isReferenceInput = true;
    refName = kebabCase(fieldRef.split('/schemas/').pop());
    if (refName.endsWith('status')) {
      refFilter = {
        type: fieldRef.split('/schemas/').pop(),
      };
      refName = 'status';
    }

    if (refName === 'user') {
      optionText = 'username';
      sourceName = `${source}.id`;
    }

    // find optionText
    Object.keys(apiRef.get(fieldRef).properties).forEach(prp => {
      if (
        apiRef.get(fieldRef).properties[prp]?.properties?.optionText?.default
      ) {
        optionText = prp;
      }
    });

    const enabledFilter = apiRef.get(fieldRef).properties.enabled !== undefined;
    if (enabledFilter) {
      refFilter = {
        enabled: '1',
      };
    }

    if (!sourceSchema?.properties?.reference?.$ref) {
      sourceName = `${source}.id`;
    }

    if (refName === 'blob') {
      isImage = true;
    }
  }

  const {
    InputComponent, guessedProps,
  } = guessInputComponent(sourceSchema, {
    isRequired,
  });

  // Guess validation for "value" of "setting" resource
  const InputContainer = ({
    children,
    defaultValue,
    requiredField,
    ...props
  }) => {
    const {
      record, source: inputSource, resource: inputResource,
    } = props;

    const form = useForm();
    const translate = useTranslate();
    const { values } = useFormState();
    const user = useSelector(state => state?.auth?.user);

    // check if this input depend on any other value
    const trueSourceName = inputSource.split('.')?.[0];
    let validate = requiredField.includes(trueSourceName) ? [required()] : [];
    const dependOnProperties = properties?.[trueSourceName]?.properties?.dependOn?.properties || {};
    const allow = properties?.[trueSourceName]?.properties?.allow?.default; // allow user of a role to create/update the record's properties
    const exclude = properties?.[trueSourceName]?.properties?.exclude?.default; // exclude user of a role to create/update the record's properties
    const dependOnPropertyList = Object.keys(dependOnProperties);

    const hiddenOnProperties = properties?.[trueSourceName]?.properties?.hiddenOn?.properties || {};
    const hiddenOnPropertyList = Object.keys(hiddenOnProperties);

    const shoduleBeHiddenOn = hiddenOnPropertyList.some(i => {
      const propertyValue = get(values, i);
      const dependOnValue = get(hiddenOnProperties, i);
      if (dependOnValue?.default && Array.isArray(dependOnValue?.default)) {
        return dependOnValue?.default?.includes(propertyValue);
      }

      if (dependOnValue?.default) {
        return dependOnValue.default === propertyValue;
      }

      return propertyValue || propertyValue?.length !== 0;
    });

    if (shoduleBeHiddenOn) {
      form.change(props.source, undefined);
      return null;
    }

    const shouldBeHidden = dependOnPropertyList.some(i => {
      const propertyValue = get(values, i);
      const dependOnValue = get(dependOnProperties, i);

      if (dependOnValue?.default && Array.isArray(dependOnValue?.default)) {
        return !dependOnValue?.default?.includes(propertyValue);
      }

      if (dependOnValue?.default) {
        return dependOnValue?.default !== propertyValue;
      }

      return !propertyValue || propertyValue?.length === 0;
    });

    if (allow && !allow.includes(user?.role?.type)) return null;

    if (exclude && exclude.includes(user?.role?.type)) return null;

    if (shouldBeHidden) {
      form.change(props.source, undefined);
      return null;
    }

    // Detect Blob object
    if (isImage) {
      return (
        <ImageInput
          source={source}
          accept="image/*"
          key={source}
          helperText={translate(sourceSchema?.description || '')}
        >
          <ImageField source="presignedUrl" />
        </ImageInput>
      );
    }

    if (isReferenceInput) {
      const realRef = resources?.find(r => {
        if (r.name.includes('/')) {
          return r.name === kebabCase(refName);
        }
        return false;
      });

      refFilter = getRefFilter(
        refName,
        refFilter,
        sourceSchema,
        values,
        user,
        form,
      );

      return (
        <ReferenceInput
          key={sourceName}
          source={sourceName}
          reference={realRef?.name || refName}
          filter={refFilter}
          allowEmpty
          fullWidth
          label={`resources.${realResource}.fields.${source}`}
          helperText={sourceSchema.description}
          filterToQuery={value => ({
            [optionText]: value || '',
          })}
          defaultValue={defaultValue}
          validate={validate}
        >
          {realRef?.name === 'status' || refName === 'status' ? (
            <SelectInput
              optionText={optionText}
              emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
            />
          ) : (
            <AutocompleteInput
              optionText={optionText}
              suggestionLimit={10}
              variant={guessedProps.variant}
              emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
            />
          )}
        </ReferenceInput>
      );
    }

    // Guess ReferenceInput (one/many to many)
    if (sourceSchema?.type === 'array') {
      const name = source.split('.').pop();
      const refField = properties[name].items?.$ref
        || properties[name].items?.allOf?.filter(i => i?.$ref)?.[0]?.$ref;
      // find optionText
      Object.keys(apiRef.get(refField).properties).forEach(prp => {
        if (
          apiRef.get(refField).properties[prp]?.properties?.optionText?.default
        ) {
          optionText = prp;
        }
      });

      refName = kebabCase(refField.split('/schemas/').pop());
      sourceName = `${source}.id`;

      if (refName === 'user') optionText = 'username';
      if (refName === 'affiliate') optionText = 'email';

      const realRef = resources?.find(r => {
        if (r.name.includes('/')) {
          return r.name.includes(kebabCase(refName));
        }

        return false;
      });

      refFilter = getRefFilter(refName, refFilter, sourceSchema, values);

      return (
        <ReferenceArrayInput
          key={refName}
          source={source}
          filter={refFilter}
          reference={realRef?.name || refName}
          label={`resources.${realResource}.fields.${source}`}
          parse={ids => ids?.map(id => ({
            id,
          }))}
          format={data => data?.map(d => d.id)}
          fullWidth
          helperText={sourceSchema.description}
          resource={realRef?.name || refName}
          filterToQuery={value => ({
            [optionText]: value || '',
          })}
          validate={validate}
          defaultValue={defaultValue}
        >
          <AutocompleteArrayInput
            optionText={optionText}
            variant={guessedProps.variant}
            emptyText={`--${translate('ra.text.any').toUpperCase()}--`}
          />
        </ReferenceArrayInput>
      );
    }

    let Component = InputComponent;
    let newProps = {
      ...props,
      defaultValue,
      validate,
    };

    // Get input for `value field` in `Setting resource`
    if (inputResource === 'setting' && sourceName === 'value') {
      validate = [required()];
      switch (record?.format) {
        case 'email':
          validate.push(email());
          break;
        case 'number':
          validate.push(number());
          break;
        case 'ip':
          validate.push(
            regex(
              /^(?=\d+\.\d+\.\d+\.\d+$)(?:(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.?){4}$/,
              'Must be a valid IP address',
            ),
          );
          break;
        case 'boolean':
          Component = BooleanInput;
          break;
        case 'blob':
          return (
            <ImageInput
              source="value"
              accept="image/*"
              key="value"
              helperText={translate(sourceSchema?.description || '')}
            >
              <ImageField source="presignedUrl" />
            </ImageInput>
          );
        case 'multiline':
          newProps.multiline = true;
          newProps.rows = 20;
          break;
        default:
          break;
      }

      newProps = {
        ...newProps,
        validate,
      };
    }

    if (isJSON) {
      Component = JsonInput;
      newProps = {
        ...newProps,
        jsonString: false,
        reactJsonOptions: {
          // Props passed to react-json-view
          name: null,
          collapsed: false,
          enableClipboard: false,
          displayDataTypes: false,
        },
      };
    }

    const newValidate = checkTheOtherValidate({
      listExtraValidate: listOtherValidate,
      currValidate: newProps.validate,
      sourceSchema,
    });

    newProps = {
      ...newProps,
      validate: newValidate,
    };

    return children({
      Component,
      props: newProps,
    });
  };

  // Normal Input
  const componentProps = {
    key: sourceName,
    source: sourceName,
  };
  // omit type prop if value is undefined to prevent wrong behavior of Input Components
  if (type) componentProps.type = type;

  return (
    <InputContainer
      {...componentProps}
      fullWidth
      variant={guessedProps?.variant}
      requiredField={requiredField}
      defaultValue={guessedProps?.defaultValue}
    >
      {({
        Component, props,
      }) => (Component && (
        <Component
          {...props}
          {...guessedProps}
          validate={[...props.validate, ...guessedProps.validate]}
          placeholder={sourceSchema?.example}
          label={`resources.${realResource}.fields.${source}`}
          helperText={sourceSchema?.description}
          variant={guessedProps?.variant}
        />
      )) || <></>}
    </InputContainer>
  );
};

export default guessInputComponentWithProp;
