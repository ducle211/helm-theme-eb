/* eslint-disable no-param-reassign */
import Type from 'prop-types';
import React from 'react';
import { SimpleForm, useTranslate } from 'react-admin';
import { Create } from '../ra/views';

const WealthCreate = props => {
  const translate = useTranslate();
  const {
    children,
    required,
    resource,
    redirect,
    writableFields,
    ...rest
  } = props;

  const transform = requiredFields => data => (requiredFields.length
    ? requiredFields.reduce((result, field) => {
      result[field] = data?.[field];
      return result;
    }, {})
    : data);

  const realResource = resource.includes('/')
    ? `resources.${resource.split('/')[1]}.name`
    : `resources.${resource}.name`;

  return (
    <Create
      {...rest}
      resource={resource}
      title={translate('ra.page.create', {
        name: `${translate(realResource)}`,
      })}
      transform={transform(required)}
      writableFields={writableFields}
    >
      <SimpleForm redirect={redirect}>{children}</SimpleForm>
    </Create>
  );
};

WealthCreate.propTypes = {
  required: Type.arrayOf(Type.string),
  children: Type.oneOfType([Type.element, Type.arrayOf(Type.element)]),
  resource: Type.string,
  redirect: Type.string, // "edit"|"show"|"list"|<custom route>
  writableFields: Type.arrayOf(Type.object),
};

WealthCreate.defaultProps = {
  required: [],
  children: null,
  resource: {},
  redirect: 'show',
  writableFields: [],
};

export default WealthCreate;
