export const docStatus = {
  DRAFT: '0',
  SUBMITTED: '5',
  APPROVED: '6',
  CANCELED: '9',
};
