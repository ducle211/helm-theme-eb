/* eslint-disable */
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  makeStyles
} from "@material-ui/core";
import PropTypes from "prop-types";
import { useDataProvider, useTranslate } from "ra-core";
import * as React from "react";
import { getResources, useNotify } from "react-admin";
import { Form } from "react-final-form";
import { useSelector } from "react-redux";
import { useSchema } from "../../hooks";
import AutocompleteInput from "../autocomplete";
import { guessProperty } from "../guesser/wealth-guesser";
import { Button } from "../ra/buttons";

const useStyles = makeStyles(() => ({
  dialogContent: {
    "& form > div, & form > div > div": {
      margin: 0,
    },
    "& form > div p": {
      display: "none",
    },
  },
  fieldSelectionWrapper: {
    "& div > div, & div > div > div": {
      margin: 0,
    },
  },
}));

const BulkEditDialog = (props) => {
  const {
    selectedIds,
    basePath,
    resource,
    excludeFields,
    open,
    setOpen,
    ...rest
  } = props;

  const classes = useStyles();
  const translate = useTranslate();
  const notify = useNotify();
  const dataProvider = useDataProvider();
  const resources = useSelector(getResources);
  const [guessedInfo, setGuessedInfo] = React.useState({});
  const { api, ref } = useSchema();

  const formRef = React.useRef({});
  const [changedField, setChangedField] = React.useState();
  const [changedComponent, setChangedComponent] = React.useState();

  const realResource = resource.includes("/")
    ? resource.split("/")[1]
    : resource;

  React.useEffect(() => {
    if (api && basePath && open) {
      const { paths } = api;
      let requestRef =
        paths?.[basePath]?.post?.requestBody?.content?.["application/json"]
          ?.schema;

      console.log({
        requestRef,
      });

      requestRef =
        requestRef?.$ref ||
        requestRef?.allOf?.filter((i) => i?.$ref)?.[0]?.$ref;
      const requestComponent = ref.get(requestRef);
      const { properties, required } = requestComponent;
      const writableFields = Object.keys(properties)
        ?.filter((key) => !excludeFields?.includes(key))
        ?.filter((key) => {
          const simpleFieldRO = properties?.[key]?.readOnly;
          const complexFieldRO = properties?.[key]?.allOf?.some(
            (p) => p.readOnly
          );

          return !simpleFieldRO && !complexFieldRO;
        });

      setGuessedInfo({
        required,
        properties,
        writableFields,
      });
    }
  }, [api, basePath, open]);

  const onFieldChange = (_, value) => {
    setChangedField(value);
    if (value) {
      const component = guessProperty({
        source: value,
        properties: guessedInfo?.properties,
        apiRef: ref,
        view: "create",
        resource,
        resources,
      });
      setChangedComponent(component);
    } else {
      setChangedComponent(null);
    }
  };

  const onClose = () => {
    setOpen(false);
  };

  const onSubmit = () => {
    if (formRef.current?.dispatchEvent) {
      formRef.current.dispatchEvent(
        new Event("submit", {
          cancelable: true,
          bubbles: true,
        })
      );
    }
  };

  const onFormSubmit = async (values) => {
    try {
      const resp = await dataProvider.updateMany(resource, {
        ids: selectedIds,
        data: {
          [changedField]: values?.hasOwnProperty(changedField)
            ? values?.[changedField]
            : "",
        },
      });
      setOpen(false);
      notify("ra.notification.updated", "success", resp?.data?.length || 0);
    } catch (ex) {
      notify(ex.message, "error");
    }
  };

  return (
    <Dialog fullWidth open={!!open} aria-labelledby="bulk-edit-dialog-title">
      <DialogTitle id="bulk-edit-dialog-title">
        {translate("ra.action.bulkedit")}
      </DialogTitle>
      <DialogContent className={classes.dialogContent}>
        <Grid container spacing={2} className={classes.fieldSelectionWrapper}>
          <Grid item md={6} xs={12}>
            <AutocompleteInput
              label={translate("ra.field.field")}
              data={guessedInfo?.writableFields}
              getOptionLabel={(option) =>
                translate(`resources.${realResource}.fields.${option}`)
              }
              onChange={onFieldChange}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            {changedComponent && (
              <Form
                onSubmit={onFormSubmit}
                render={({ handleSubmit }) => (
                  <form
                    ref={formRef}
                    onSubmit={(e) => {
                      handleSubmit();
                      e.preventDefault();
                    }}
                  >
                    {React.cloneElement(changedComponent)}
                  </form>
                )}
              />
            )}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          label={translate("ra.action.close")}
          variant="text"
          color="primary"
          size="small"
          onClick={onClose}
        />
        <Button
          label={translate("ra.action.save")}
          color="primary"
          variant="text"
          onClick={onSubmit}
          disabled={!changedComponent}
        />
      </DialogActions>
    </Dialog>
  );
};

BulkEditDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func,
};

BulkEditDialog.defaultProps = {
  open: false,
  onSubmit: () => {},
  onClose: () => {},
};

export default BulkEditDialog;
