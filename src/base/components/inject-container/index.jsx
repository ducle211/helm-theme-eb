/* eslint-disable */
import * as React from "react";
import PropTypes from "prop-types";
import { isValidElement } from "react";
import { useShowContext } from "ra-core";
import classnames from "classnames";
import Labeled from "ra-ui-materialui/lib/input/Labeled";
import { Box, Grid } from "@material-ui/core";

const InjectContainer = ({
  record: rootRecord,
  resource: rootResource,
  ...props
}) => {
  const { children, className, xs, sm, md, lg } = props;
  const {
    basePath,
    record: defaultRecord,
    resource: defaultResource,
    version,
  } = useShowContext(props);
  const record = rootRecord || defaultRecord;
  const resource = rootResource || defaultResource;

  return (
    <Grid className={className} container>
      {React.Children.map(children, (field) =>
        field && isValidElement(field) ? (
          <Grid
            item
            xs={xs}
            sm={sm}
            md={md}
            lg={lg}
            key={field.props.source}
            className={classnames(
              "ra-field",
              `ra-field-${field.props.source}`,
              field.props.className
            )}
          >
            {field.props.addLabel ? (
              <Labeled
                label={field.props.label}
                source={field.props.source}
                basePath={basePath}
                record={record}
                resource={resource}
              >
                {field}
              </Labeled>
            ) : typeof field.type === "string" ? (
              field
            ) : (
              React.cloneElement(field, {
                basePath,
                record,
                resource,
                version,
              })
            )}
          </Grid>
        ) : null
      )}
    </Grid>
  );
};

InjectContainer.propTypes = {
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
};

InjectContainer.defaultProps = {
  xs: 12,
  sm: 6,
  md: 4,
  lg: 4,
};

export default InjectContainer;
