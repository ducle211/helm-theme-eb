import React, { memo, useEffect, useState, useCallback } from 'react';
import { CondOperator, RequestQueryBuilder } from '@nestjsx/crud-request';
import { useNotify } from 'react-admin';
import axios from 'axios';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import loDebounce from 'lodash/debounce';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import TextField from '@material-ui/core/TextField';
import Loader from '../../@crema/core/Loader';

const useStyles = makeStyles({
  container: {
    minHeight: '50vh',
  },
  tableCell: {
    backgroundColor: 'black',
    color: 'white',
    cursor: 'pointer',
    position: 'relative',
  },
  icon: {
    position: 'absolute',
    right: '5px',
    fontSize: '16px',
    top: '10px',
  },
});

const FraudTool = ({ ipChecker }) => {
  const notify = useNotify();
  const classes = useStyles();
  const [objDataChecker, setObjDataChecker] = useState({
    data: [],
    total: 0,
  });
  const [loading, setLoading] = useState(false);

  const [sort, setSort] = useState();
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState();
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage + 1);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(1);
  };

  const getUrlRedirect = text => `#/user?displayedFilters=%7B"id"%3Atrue%7D&filter=%7B"id||in||"%3A"${text}"%7D&order=ASC&page=1&perPage=25&sort=id`;

  const mapObjChecker = () => ({
    heading1: ipChecker
      ? {
        name: 'IP Count',
        value: 'total',
        width: '45%',
      }
      : {
        name: 'Bank Account Count',
        value: 'bankAccountCount',
        width: '45%',
      },
    heading2: ipChecker
      ? {
        name: 'IP',
        value: 'ip',
        width: '50%',
      }
      : {
        name: 'Bank Account Name',
        value: 'bankAccountName',
        width: '50%',
      },
  });

  const getDataChecker = async () => {
    setLoading(true);
    try {
      const type = ipChecker ? 'ip' : 'bank';
      const fieldSearch = ipChecker ? 'ip' : 'bankAccountName';

      const encodedQueryFilter = RequestQueryBuilder.create()
        .setPage(page)
        .setLimit(rowsPerPage);

      if (sort) {
        sort.order = sort.order || 'DESC';
        encodedQueryFilter.sortBy(sort);
      }

      if (search) {
        encodedQueryFilter.setFilter({
          field: fieldSearch,
          value: search,
          operator: CondOperator.CONTAINS,
        });
      }

      const {
        data: {
          data = [], total = 0,
        },
      } = await axios.get(`api/${type}-checker?${encodedQueryFilter.query()}`);

      setObjDataChecker({
        data,
        total,
      });
    } catch (error) {
      if (!error?.response) {
        notify('Please try again later!', 'error');
      }
      notify(`Error! ${error?.response?.data?.message}`, 'error');
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getDataChecker();
  }, [page, rowsPerPage, sort, search]);

  const onChangeInput = useCallback(
    loDebounce(newValue => setSearch(newValue), 400),
    [],
  );

  const objChecker = mapObjChecker();

  return (
    <Card>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <CardHeader
          title={ipChecker ? 'Duplicate Ip' : 'Duplicate Bank Account Name'}
        />
        <div
          style={{
            display: 'flex',

            alignItems: 'center',
          }}
        >
          Search:
          <TextField
            type="search"
            size="small"
            style={{
              marginLeft: '5px',
            }}
            placeholder={ipChecker ? 'ip' : 'Bank account name'}
            variant="outlined"
            onChange={e => onChangeInput(e.target.value)}
          />
        </div>
      </div>
      <TableContainer
        component={Paper}
        elevation={0}
        style={{
          width: '100%',
          marginTop: '20px',
        }}
        className={classes.container}
      >
        <Table
          aria-label="bank-checker"
          size="small"
          style={{
            position: 'relative',
          }}
        >
          <TableHead>
            <TableRow>
              <TableCell
                className={classes.tableCell}
                align="center"
              >
                No.
              </TableCell>
              {Object.keys(objChecker).map(key => {
                const {
                  name, value, width,
                } = objChecker[key];

                return (
                  <TableCell
                    className={classes.tableCell}
                    style={{
                      width,
                    }}
                    align="center"
                    onClick={() => {
                      if (value) {
                        setSort({
                          field: value,
                          order:
                            sort?.field !== value
                              ? 'DESC'
                              : sort?.order === 'DESC' && 'ASC',
                        });
                      }
                    }}
                  >
                    {name}
                    {sort?.field === value && (
                      <>
                        {sort?.order === 'DESC' ? (
                          <ArrowDownwardIcon className={classes.icon} />
                        ) : (
                          <ArrowUpwardIcon className={classes.icon} />
                        )}
                      </>
                    )}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
          {!loading ? (
            <TableBody>
              {objDataChecker?.data.map((row, index) => {
                const name = ipChecker ? row.ip : row.bankAccountName;
                const total = ipChecker ? row.total : row.bankAccountCount;
                const urlRedirect = getUrlRedirect(row.userIds.join('%2C'));
                return (
                  <TableRow
                    key={name}
                    style={{
                      cursor: 'pointer',
                    }}
                    onClick={() => {
                      window.open(urlRedirect);
                    }}
                  >
                    <TableCell
                      component="th"
                      scope="row"
                      align="center"
                    >
                      {(page - 1 < 0 ? 0 : page - 1) * rowsPerPage + index + 1}
                    </TableCell>
                    <TableCell align="center">{total}</TableCell>
                    <TableCell align="center">{name}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          ) : (
            <Loader
              top={50}
              left={50}
            />
          )}
        </Table>
      </TableContainer>
      {objDataChecker?.data?.length > 0 && (
        <TablePagination
          component="div"
          count={objDataChecker.total || 0}
          page={page - 1}
          onChangePage={handleChangePage}
          rowsPerPage={rowsPerPage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      )}
    </Card>
  );
};

FraudTool.propTypes = {
  ipChecker: PropTypes.bool,
};

FraudTool.defaultProps = {
  ipChecker: false,
};

export default memo(FraudTool);
