/* eslint-disable */
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";
import { makeStyles } from "@material-ui/core/styles";
import IconClose from "@material-ui/icons/Close";
import T from "prop-types";
import React, { useState, useEffect } from "react";
import { FieldTitle, useTranslate } from "react-admin";
import Paper from "@material-ui/core/Paper";
import Draggable from "react-draggable";

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
}

const useStyles = makeStyles((theme) => ({
  dialogStyle: {
    "& [class*=\"MuiDialog-paper\"]": {
      [theme.breakpoints.up('sm')]: {
        minWidth: 500,
      },
      [theme.breakpoints.up('md')]: {
        minWidth: 600,
      },
    },
  },
  selectAllBtn: {
    marginRight: 'auto',
  },
}));

const SelectionDialog = ({
  columns,
  selection,
  onClose,
  resource,
  onColumnClicked,
}) => {
  const [selectColumns, setSelectColumns] = useState(selection);
  const classes = useStyles();

  const onColumnClick = ({ target: { value: columnName } }) => {
    setSelectColumns({
      ...selectColumns,
      [columnName]: !selectColumns[columnName],
    });
  };

  const translate = useTranslate();

  const handleOk = () => {
    onColumnClicked(selectColumns);
    onClose();
  };

  const handleClose = () => {
    setSelectColumns(selection);
    onClose();
  };

  const handleSelectAll = () => {
    const selection = columns.reduce((selectObj, currentCol) => ({
      ...selectObj,
      [currentCol.source]: true,
    }), {});
    onColumnClicked(selection);
    onClose();
  };

  useEffect(() => {
    setSelectColumns(selectColumns);
  }, [selection]);

  return (
    <Dialog
      className={classes.dialogStyle}
      maxWidth="sm"
      aria-labelledby="ra-columns-dialog-title"
      onClose={handleClose}
      PaperComponent={PaperComponent}
      open
    >
      <DialogTitle id="ra-columns-dialog-title">Configuration</DialogTitle>
      <DialogContent dividers style={{ maxHeight: "400px" }}>
        <FormGroup>
          {columns.map(({ source, label }) => (
            <FormControlLabel
              key={source}
              control={
                <Checkbox
                  checked={!!selectColumns[source]}
                  onChange={onColumnClick}
                  value={source}
                />
              }
              label={
                <FieldTitle label={label} source={source} resource={resource} />
              }
            />
          ))}
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button className={classes.selectAllBtn} autoFocus onClick={handleSelectAll} color="primary">
          {translate("ra.action.selectAll")}
        </Button>
        <Button autoFocus onClick={handleClose} color="primary">
          {translate("ra.action.cancel")}
        </Button>
        <Button onClick={handleOk} color="primary">
          {translate("ra.action.ok")}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

SelectionDialog.propTypes = {
  columns: T.arrayOf(
    T.shape({
      label: T.string,
      source: T.string.isRequired,
    })
  ).isRequired,
  selection: T.object,
};

SelectionDialog.defaultProps = {
  columns: [],
  columns: {},
};

export default SelectionDialog;
