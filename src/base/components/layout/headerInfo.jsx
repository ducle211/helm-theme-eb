import { Typography, Box } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useAuthUser } from '../../hooks';

function getTime() {
  const [time, setTime] = useState(new Date().toISOString());
  function tick() {
    setTime(new Date().toISOString());
  }
  useEffect(() => {
    const timerID = setInterval(() => tick(), 1000);
    return () => {
      clearInterval(timerID);
    };
  }, []);
  return `${time}[UTC]`;
}

function HeaderInfo() {
  const user = useAuthUser();
  return (
    <Box
      display="flex"
      minWidth="250px"
      flexDirection="column"
    >
      <Typography
        variant="caption"
        component="div"
      >
        {getTime()}
      </Typography>
      <Typography
        variant="caption"
        component="div"
      >
        {user?.email}
      </Typography>
    </Box>
  );
}

export default HeaderInfo;
