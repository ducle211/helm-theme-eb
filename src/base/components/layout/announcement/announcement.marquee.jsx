/* eslint-disable */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Marquee from "react-double-marquee";
import { Box, makeStyles, IconButton } from "@material-ui/core";
import Cookie from "../../../../services/util/handleCookie";
import VolumeUpIcon from "@material-ui/icons/VolumeUp";
import VolumeOffIcon from "@material-ui/icons/VolumeOff";

const useStyles = makeStyles((theme) => ({
  announcement: {
    whiteSpace: "nowrap",
    fontWeight: "normal",
  },
}));

const MarqueeAnnouncement = ({ announcements, delay }) => {
  const classes = useStyles();

  const defaultValue = "false";
  const bannerVisibleCookieName = "isBannerAnnouncementVisible";
  const isBannerVisibleFromCookie = Cookie.getItem(
    bannerVisibleCookieName,
    defaultValue
  );
  const [isBannerVisible, setBannerVisible] = useState(true);

  const toggleBannerAnnouncementVisible = () => {
    const value = !isBannerVisible;
    Cookie.setItem(bannerVisibleCookieName, value.toString());
    setBannerVisible(value);
  };

  useEffect(() => {
    setBannerVisible(isBannerVisibleFromCookie != "false");
  }, [isBannerVisibleFromCookie]);

  const handleOnClick = (url) => {
    if (url) {
      window.open(url);
    }
  };

  return (
    <>
      <Box
        style={{ display: "flex", alignItems: "center", width: "100%" }}
        onClick={toggleBannerAnnouncementVisible}
      >
        <Box overflow="hidden" flex={1}>
          {isBannerVisible && (
            <Marquee delay={delay} direction="left" scrollWhen="always">
              {announcements?.map(({ subject, id, url }) => (
                <Box
                  key={id}
                  aria-hidden="true"
                  onClick={() => handleOnClick(url)}
                  style={{
                    cursor: url ? "pointer" : "default",
                  }}
                  className={classes.announcement}
                  component="span"
                  color="warning.main"
                >
                  {subject}
                </Box>
              ))}
            </Marquee>
          )}
        </Box>
        {(announcements?.length && (
          <>
            <IconButton
              aria-label="Refresh"
              aria-haspopup="true"
              onClick={toggleBannerAnnouncementVisible}
              color="default"
            >
              {!isBannerVisible ? <VolumeOffIcon /> : <VolumeUpIcon />}
            </IconButton>
          </>
        )) ||
          null}
      </Box>
    </>
  );
};

MarqueeAnnouncement.propTypes = {
  announcements: PropTypes.array.isRequired,
  delay: PropTypes.number,
};

MarqueeAnnouncement.defaultProps = {
  delay: 500,
};

export default MarqueeAnnouncement;
