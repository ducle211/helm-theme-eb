import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';

const PermanentAnnouncement = ({
  isVisible,
  subject,
  content,
  onClose,
}) => {
  const [open, setOpen] = React.useState(isVisible);

  const handleClose = () => {
    if (onClose instanceof Function) {
      onClose();
    }
    setOpen(false);
  };

  if (!open) return null;

  return (
    <Box
      mt={4}
      mb={4}
    >
      <Alert
        severity="info"
        onClose={handleClose}
      >
        <AlertTitle>{subject}</AlertTitle>
        {content}
      </Alert>
    </Box>
  );
};

PermanentAnnouncement.propTypes = {
  subject: PropTypes.string,
  content: PropTypes.string,
  isVisible: PropTypes.bool,
  onClose: PropTypes.func,
};

PermanentAnnouncement.defaultProps = {
  subject: '',
  content: '',
  isVisible: true,
  onClose: undefined,
};

export default PermanentAnnouncement;
