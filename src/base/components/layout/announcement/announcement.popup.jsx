import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import { Button, useTranslate } from 'react-admin';
import CloseIcon from '@material-ui/icons/Close';

const PopupAnnouncement = ({
  isVisible, subject, content, onClose,
}) => {
  const [open, setOpen] = React.useState(isVisible);
  const translate = useTranslate();

  const handleClose = () => {
    if (onClose instanceof Function) {
      const shouldClose = onClose();
      // prevent the unnecessary close event while we're trying to display multiple announcements from this popup
      if (!shouldClose) {
        return;
      }
    }
    setOpen(false);
  };

  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      scroll="paper"
    >
      <DialogTitle>{subject}</DialogTitle>
      <DialogContent dividers>
        <DialogContentText
          ref={descriptionElementRef}
          tabIndex={-1}
        >
          {content}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleClose}
          color="primary"
          startIcon={<CloseIcon />}
          label={translate('ra.action.close')}
        />
      </DialogActions>
    </Dialog>
  );
};

PopupAnnouncement.propTypes = {
  subject: PropTypes.string,
  content: PropTypes.string,
  isVisible: PropTypes.bool,
  onClose: PropTypes.func,
};

PopupAnnouncement.defaultProps = {
  subject: '',
  content: '',
  isVisible: true,
  onClose: undefined,
};

export default PopupAnnouncement;
