import { makeStyles } from '@material-ui/core/styles';

const drawerWidth = 350;
const drawerWidthXS = 280;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  overlay: {
    position: 'absolute',
    width: '100vw',
    height: '100vh',
    top: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
    zIndex: '1199',
    transition: theme.transitions.create('background-color'),
  },
  appBar: {
    backgroundColor: '#fff',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appToolbar: {
    paddingLeft: 20,
    paddingRight: 20,
    minHeight: 56,
    [theme.breakpoints.up('sm')]: {
      minHeight: 70,
    },
    [theme.breakpoints.up('md')]: {
      paddingLeft: 30,
      paddingRight: 30,
    },
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    [theme.breakpoints.down('xs')]: {
      width: `calc(100% - ${drawerWidthXS}px)`,
    },
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    flexShrink: 0,
  },
  drawerHide: {
    width: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    [theme.breakpoints.down('xs')]: {
      width: drawerWidthXS,
    },
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    overflow: 'hidden',
    marginTop: theme.spacing(19),
    marginBottom: theme.spacing(4),
    [theme.breakpoints.up('sm')]: {
      marginTop: 70,
      marginBottom: 0,
    },
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: drawerWidth,
    paddingLeft: 20,
    paddingRight: 20,
    [theme.breakpoints.up('md')]: {
      paddingLeft: 30,
      paddingRight: 30,
    },
  },
  title: {
    color: '#000',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    [theme.breakpoints.down('md')]: {
      flex: 'unset',
    },
  },
  announcementWrapper: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    flex: 2,
    overflow: 'hidden',
  },
}));

export default useStyles;
