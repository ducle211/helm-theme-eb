import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';

const AppContent = ({ children }) => <Box height="100%">{children}</Box>;

AppContent.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AppContent;
