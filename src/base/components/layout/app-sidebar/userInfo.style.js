import { makeStyles } from '@material-ui/core';
import { grey, orange } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  crUserInfo: {
    backgroundColor: 'rgba(0,0,0,.08)',
    paddingTop: 9,
    paddingBottom: 9,
    minHeight: 56,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.breakpoints.up('sm')]: {
      paddingTop: 10,
      paddingBottom: 10,
      minHeight: 70,
    },
  },
  profilePic: {
    height: 40,
    width: 40,
    fontSize: 24,
    backgroundColor: orange[500],
    [theme.breakpoints.up('xl')]: {
      height: 45,
      width: 45,
    },
  },
  userInfo: {
    width: 'calc(100% - 75px)',
  },
  userName: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    fontSize: 18,
    [theme.breakpoints.up('xl')]: {
      fontSize: 20,
    },
    color: 'white',
  },
  designation: {
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  pointer: {
    cursor: 'pointer',
  },
  adminRoot: {
    color: grey[500],
    fontSize: 16,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
}));

export default useStyles;
