import {
  Collapse,
  Icon,
  IconButton,
  ListItem,
  ListItemText,
  Badge,
} from '@material-ui/core';
import Box from '@material-ui/core/Box';
import ViewListIcon from '@material-ui/icons/ViewList';
import clsx from 'clsx';
import { kebabCase } from 'lodash';
import Type from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useTranslate } from 'react-admin';
import { useLocation } from 'react-router-dom';
import useStyles from './sidebarGroup.style';
import SidebarItem from './sidebarItem';

const SidebarGroup = props => {
  const {
    item, metric,
  } = props;
  const classes = useStyles(props);
  const { pathname } = useLocation();
  const [open, setOpen] = useState(false);
  const translate = useTranslate();

  useEffect(() => {
    // Initial open if url contains any child
    if (
      item?.options?.children?.some(
        child => child.name === pathname.split('/')[1].toLowerCase(),
      )
    ) {
      setOpen(true);
    }
  }, [pathname, item]);

  let noticeCount = 0;
  if (item?.options?.children?.length) {
    item?.options?.children.forEach(r => {
      noticeCount += metric[r.name]?.noticeCount || 0;
    });
  }

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <>
    { item.options.children.filter(r => r.list && !r.canNotViewOnBo).length > 0 && (
      <ListItem
        button
        component="li"
        className={clsx(classes.navItem, open && 'open')}
        onClick={handleClick}
      >
        {item.icon && (
          <Box
            component="span"
            mr={6}
          >
            <Badge
              color="secondary"
              variant="dot"
              invisible={noticeCount <= 0}
            >
              <Icon
                color="action"
                className={clsx('nav-item-icon', classes.listIcon)}
              >
                {item.icon ? React.createElement(item.icon) : <ViewListIcon />}
              </Icon>
            </Badge>
          </Box>
        )}
        <ListItemText
          classes={{
            primary: clsx('nav-item-text', classes.listItemText),
          }}
          primary={translate(`resources.group.${kebabCase(item.name)}.name`)}
        />
        <Box
          p={0}
          clone
        >
          <IconButton disableRipple>
            <Icon
              className="nav-item-icon-arrow"
              color="inherit"
            >
              {open ? 'expand_more' : 'chevron_right'}
            </Icon>
          </IconButton>
        </Box>
      </ListItem>
    )}

      {item.options?.children && (
        <Collapse
          in={open}
          className="collapse-children"
        >
          {item.options.children
            .filter(r => r.list && !r.canNotViewOnBo)
            .sort((a, b) => a.index - b.index)
            .map(child => (
              <SidebarItem
                metric={metric}
                key={child.id}
                item={child}
                level={1}
              />
            ))}
        </Collapse>
      )}
    </>
  );
};

SidebarGroup.propTypes = {
  item: Type.shape({
    id: Type.string,
    title: Type.string,
    icon: Type.func,
    options: Type.shape({
      children: Type.array,
    }),
    name: Type.string,
  }).isRequired,
  metric: Type.object,
};
SidebarGroup.defaultProps = {
  metric: {},
};

export default React.memo(SidebarGroup);
