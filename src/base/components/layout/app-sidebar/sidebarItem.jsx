/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { Icon, ListItem, ListItemText, Badge, useMediaQuery } from '@material-ui/core';
import { withRouter } from 'react-router';
import clsx from 'clsx';
import Type from 'prop-types';
import ViewListIcon from '@material-ui/icons/ViewList';
import { useTranslate } from 'react-admin';
import { useDispatch } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { kebabCase } from 'lodash';
import useStyles from './sidebarItem.style';
import { toggleNavCollapsed } from '../../../../services/redux/setting/setting.actions';

const SidebarItem = props => {
  const classes = useStyles(props);
  const {
    item, metric,
  } = props;
  const { location: { pathname } } = props;
  const translate = useTranslate();
  const mobileScreen = useMediaQuery(theme => theme.breakpoints.down('xs'));
  const dispatch = useDispatch();
  const realResource = item.name.includes('/') ? `${item.name.split('/')[1]}` : kebabCase(item.name);

  const handleClick = () => {
    props.history.push(`/${item.name}`);
    if (mobileScreen) {
      dispatch(toggleNavCollapsed(false));
    }
  };

  const renderIcon = icon => {
    if (!icon) return <ViewListIcon />;
    if (typeof icon !== 'string') return React.createElement(item.icon);
    return icon;
  };

  const StyledBadge = withStyles(theme => ({
    badge: {
      right: -3,
      top: 10,
      border: `2px solid ${theme.palette.sidebar.bgColor}`,
    },
  }))(Badge);

  const activeSideBar = () => {
    if (item.name.includes('/')) { // if it have prefix
      return pathname.split('/')[2] === item.name.split('/')[1];
    }

    return item.name === pathname.split('/')[1]?.toLowerCase();
  };

  return (
    <ListItem
      button
      className={clsx(classes.navItem, 'nav-item', {
        active: activeSideBar(),
      })}
      onClick={handleClick}
    >
      <Box
        component="span"
        mr={6}
      >
        <StyledBadge
          max={99}
          color="secondary"
          badgeContent={metric[item.name]?.noticeCount}
        >
          <Icon
            className={clsx(classes.listIcon, 'nav-item-icon')}
            color="action"
          >
            {renderIcon(item.icon)}
          </Icon>
        </StyledBadge>
      </Box>
      <ListItemText
        primary={
          item?.name
            ? translate(`resources.${realResource}.name`)
            : translate('ra.page.dashboard')
        }
        classes={{
          primary: clsx(classes.lisItemText, 'nav-item-text'),
        }}
      />
    </ListItem>
  );
};

SidebarItem.propTypes = {
  item: Type.shape({
    name: Type.string.isRequired,
    icon: Type.any, // Material-UI Icon
  }),
  metric: Type.object,
};

SidebarItem.defaultProps = {
  item: {
    name: 'Single Item',
    icon: ViewListIcon,
  },
  metric: {},
};

export default withRouter(React.memo(SidebarItem));
