import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  sidebarBg: {
    backgroundColor: theme.palette.sidebar.bgColor,
  },
  drawerScrollAppSidebar: {
    paddingTop: 8,
    paddingBottom: 20,
    height: 'calc(100vh - 58px) !important',

    [theme.breakpoints.up('xl')]: {
      height: 'calc(100vh - 65px) !important',
    },
  },
  sidebarStandard: {
    overflow: 'scroll',
    height: '100%',
  },
  drawerDesktop: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
}));
export default useStyles;
