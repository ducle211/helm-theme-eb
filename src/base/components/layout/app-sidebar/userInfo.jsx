import Avatar from '@material-ui/core/Avatar';
import Box from '@material-ui/core/Box';
import { grey } from '@material-ui/core/colors';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'clsx';
import React from 'react';
import { useLogout, MenuItemLink } from 'react-admin';
import { useAuthUser } from '../../../hooks';
import useStyles from './userInfo.style';

const UserInfo = props => {
  const user = useAuthUser();
  const logout = useLogout();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const getUserAvatar = () => {
    if (user.displayName) {
      return user.displayName.charAt(0).toUpperCase();
    }
    if (user.email) {
      return user.email.charAt(0).toUpperCase();
    }
    return 'A';
  };

  const classes = useStyles(props);

  return (
    <Box
      px={{
        xs: 4,
        xl: 7,
      }}
      className={clsx(classes.crUserInfo, 'cr-user-info')}
    >
      <Box
        display="flex"
        alignItems="center"
      >
        {user.photoURL ? (
          <Avatar
            className={classes.profilePic}
            src={user.photoURL}
          />
        ) : (
          <Avatar className={classes.profilePic}>{getUserAvatar()}</Avatar>
        )}
        <Box
          ml={4}
          className={clsx(classes.userInfo, 'user-info')}
        >
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <Box
              mb={0}
              className={clsx(classes.userName)}
            >
              {user?.group?.name && `${user?.group?.name} - `}
              {user?.displayName}
            </Box>
            <Box
              ml={3}
              className={classes.pointer}
              color="white"
            >
              <ExpandMoreIcon onClick={handleClick} />
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItemLink
                  to="/profile"
                  primaryText="My profile"
                />
                <MenuItemLink
                  to="/change-password"
                  primaryText="Change Password"
                />
                <MenuItem
                  onClick={() => {
                    logout({}, '/', false);
                  }}
                >
                  Logout
                </MenuItem>
              </Menu>
            </Box>
          </Box>
          <Box
            color={grey.A200}
            className={classes.designation}
          >
            {user?.role?.name}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default UserInfo;
