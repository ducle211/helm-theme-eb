import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  navItem: {
    height: 64,
    fontWeight: 700,
    cursor: 'pointer',
    textDecoration: 'none !important',
    paddingLeft:
      theme.direction === 'ltr' ? props => 24 + 50 * props.level : 12,
    paddingRight:
      theme.direction === 'rtl' ? props => 24 + 50 * props.level : 12,
    '&.nav-item-header': {
      textTransform: 'uppercase',
    },
    '&.active': {
      backgroundColor: 'rgba(0,0,0,.08)',
      transition: 'border-radius .15s cubic-bezier(0.4,0.0,0.2,1)',
      '& .nav-item-text': {
        color: `${theme.palette.primary.main}!important`,
      },
      '& .nav-item-icon': {
        color: `${theme.palette.primary.main}!important`,
      },
    },

    '&:hover, &:focus': {
      '& .nav-item-text': {
        color: '#fff',
      },

      '& .nav-item-icon': {
        color: '#fff',
      },

      '& .nav-item-icon-arrow': {
        color: '#fff',
      },
    },
    '& .nav-item-icon': {
      color: '#808183',
    },
    '& .nav-item-text': {
      textTransform: 'capitalize',
      color: '#808183',
      fontSize: 18,
    },
  },
  '@media (max-width: 1919px)': {
    navItem: {
      height: 48,
      paddingLeft:
        theme.direction === 'ltr' ? props => 55 * props.level : 12,
      paddingRight:
        theme.direction === 'rtl' ? props => 55 * props.level : 12,

      '& .nav-item-text': {
        fontSize: 16,
      },
    },
  },
  listIcon: {
    fontSize: 18,
    [theme.breakpoints.up('xl')]: {
      fontSize: 20,
    },
  },
  listItemText: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
}));
export default useStyles;
