import React from 'react';
import PropTypes from 'prop-types';
import CmtHorizontal from '../../../../theme/@coremat/CmtNavigation/Horizontal';
import CmtVertical from '../../../../theme/@coremat/CmtNavigation/Vertical';
import useNavigation from '../../../hooks/useNavigation';
import useSite from '../../../hooks/useSite';

const AppSidebar = ({ vertical }) => {
  useSite();
  const { navigationMenus } = useNavigation(true);

  const Component = vertical ? CmtVertical : CmtHorizontal;

  return <Component menuItems={navigationMenus} />;
};

export default AppSidebar;

AppSidebar.defaultProps = {
  vertical: false,
};

AppSidebar.propTypes = {
  vertical: PropTypes.bool,
};
