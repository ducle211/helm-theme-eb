import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  navItem: {
    height: 48,
    paddingLeft:
      theme.direction === 'ltr' ? props => 17 + 50 * props.level : 17,
    paddingRight:
      theme.direction === 'rtl' ? props => 17 + 50 * props.level : 17,
    width: '100%',

    [theme.breakpoints.up('xl')]: {
      height: 64,
      paddingLeft:
        theme.direction === 'ltr' ? props => 24 + 50 * props.level : 24,
      paddingRight:
        theme.direction === 'rtl' ? props => 24 + 50 * props.level : 24,
    },

    '& .nav-item-text': {
      fontWeight: 'normal',
      fontSize: 16,
      color: theme.palette.sidebar.textColor,

      [theme.breakpoints.up('xl')]: {
        fontSize: 18,
      },
    },

    '& .nav-item-icon': {
      color: theme.palette.sidebar.textColor,
    },

    '& .nav-item-icon-arrow': {
      color: theme.palette.sidebar.textColor,
    },

    '&.open, &:hover, &:focus': {
      '& .nav-item-text': {
        fontWeight: 'normal',
        color: '#fff',
      },

      '& .nav-item-icon': {
        color: '#fff',
      },

      '& .nav-item-icon-arrow': {
        color: '#fff',
      },
    },
  },
  listItemText: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  listIcon: {
    fontSize: 18,
    [theme.breakpoints.up('xl')]: {
      fontSize: 20,
    },
  },
}));
export default useStyles;
