import { Typography } from '@material-ui/core';
import React from 'react';
import CmtFooter from '../../../theme/@coremat/CmtLayouts/Horizontal/Footer';

function Footer() {
  return (
    <CmtFooter>
      <Typography
        align="center"
        variant="caption"
      >
        {`© Naga Gaming ${new Date().getFullYear()}. All right reserved.`}
      </Typography>
    </CmtFooter>
  );
}

export default Footer;
