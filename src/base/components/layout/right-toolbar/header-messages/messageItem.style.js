import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  textBase: {
    fontSize: 16,
    [theme.breakpoints.up('xl')]: {
      fontSize: 18,
    },
  },
  avatar: {
    width: 40,
    height: 40,
    [theme.breakpoints.up('xl')]: {
      width: 60,
      height: 60,
    },
  },
  minWidth0: {
    minWidth: 0,
  },
  listItemRoot: {
    display: 'flex',
    padding: '0px 0px 4px',
    '&:last-child': {
      paddingBottom: 0,
    },
  },
}));

export default useStyles;
