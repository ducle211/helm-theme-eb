import React from 'react';
import Box from '@material-ui/core/Box';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';
import ListItem from '@material-ui/core/ListItem';

import useStyles from './messageItem.style';

const HeaderMessageItem = props => {
  const {
    item, listStyle,
  } = props;
  const classes = useStyles(props);

  return (
    <ListItem className={(classes.listItemRoot, `${listStyle}`)}>
      <Box mr={4}>
        <ListItemAvatar className={classes.minWidth0}>
          <Avatar
            className={classes.avatar}
            src={item.image}
          />
        </ListItemAvatar>
      </Box>
      <Box className={classes.textBase}>
        <Box
          component="span"
          display="block"
          fontSize={{
            xs: 16,
            xl: 18,
          }}
        >
          {item.name}
        </Box>
        <Box
          component="span"
          color="grey.500"
          display="block"
        >
          {item.message}
        </Box>
      </Box>
    </ListItem>
  );
};

export default HeaderMessageItem;

HeaderMessageItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    message: PropTypes.string,
    image: PropTypes.string,
  }).isRequired,
  listStyle: PropTypes.string,
};

HeaderMessageItem.defaultProps = {
  listStyle: '',
};
