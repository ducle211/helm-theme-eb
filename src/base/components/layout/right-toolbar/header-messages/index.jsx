import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import SmsIcon from '@material-ui/icons/Sms';
import Popover from '@material-ui/core/Popover';
import List from '@material-ui/core/List';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Hidden from '@material-ui/core/Hidden';
import clsx from 'clsx';

// import IntlMessages from '../../utility/IntlMessages';
import HeaderMessageItem from './messageItem';
import useStyles from './index.style';

const messages = [
  {
    id: 201,
    image: '/images/avatar/A19.jpg',
    message: 'Hey man! Whatsapp?',
    name: 'Angelina Joew',
  },
  {
    id: 202,
    image: '/images/avatar/A15.jpg',
    message: 'I am fine, what about you?',
    name: 'John Matthew',
  },
  {
    id: 203,
    image: '/images/avatar/A21.jpg',
    message: 'Call me when you are free!',
    name: 'George Bailey',
  },
  {
    id: 204,
    image: '/images/avatar/A25.jpg',
    message: 'Send your contact details!',
    name: 'Maria Lee',
  },
];

const HeaderMessages = props => {
  const [anchorMessages, setAnchorMessages] = React.useState(null);

  const onClickMessagesButton = event => {
    setAnchorMessages(event.currentTarget);
  };

  const classes = useStyles(props);

  return (
    <>
      <IconButton
        className={clsx(classes.notiBtn, 'notiBtn')}
        aria-label="show 4 new mails"
        color="inherit"
        onClick={onClickMessagesButton}
      >
        <Badge
          className={classes.badgeStyle}
          badgeContent={messages.length}
          color="secondary"
        >
          <SmsIcon className={clsx(classes.smsIcon, 'smsIcon')} />
        </Badge>
        <Hidden mdUp>
          <Box
            ml={4}
            fontSize={16}
            color="text.secondary"
            component="span"
          >
            {/* <IntlMessages id='dashboard.messages' /> */}
            Messages
          </Box>
        </Hidden>
      </IconButton>

      <Popover
        anchorEl={anchorMessages}
        id="app-message"
        className={classes.crPopover}
        open={Boolean(anchorMessages)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        onClose={() => setAnchorMessages(null)}
      >
        <Box>
          <Box
            px={5}
            py={3}
          >
            <Box component="h5">
              {/* <IntlMessages id='dashboard.messages' />({messages.length}) */}
              Messages(4)
            </Box>
          </Box>
          <Box className="scroll-submenu">
            <List
              className={classes.listRoot}
              onClick={() => {
                setAnchorMessages(null);
              }}
            >
              {messages.map(item => (
                <HeaderMessageItem
                  listStyle={classes.listStyle}
                  key={item.id}
                  item={item}
                />
              ))}
            </List>
          </Box>
          <Box mt={2}>
            <Button
              className={classes.btnPopover}
              variant="contained"
              color="primary"
            >
              {/* <IntlMessages id='common.viewAll' /> */}
              View all
            </Button>
          </Box>
        </Box>
      </Popover>
    </>
  );
};

export default HeaderMessages;
