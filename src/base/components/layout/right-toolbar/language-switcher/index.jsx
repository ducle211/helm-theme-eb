import React, { useEffect, useState } from 'react';
import { IconButton, Popover, useTheme } from '@material-ui/core';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useGetList, useLocale, useRefresh, useSetLocale, useTranslate } from 'react-admin';
import moment from 'moment';
import LanguageItem from './LanguageItem';
import { useLanguageContext } from '../../../../context/language';
import { useLocalStorage } from '../../../../hooks/useLocalStorage';
import './asset/sprite-flags-24x24.css';
import useStyles from './index.style';
import 'moment/locale/th';
import 'moment/locale/zh-cn';
import CmtCard from '../../../../../theme/@coremat/CmtCard';
import CmtCardHeader from '../../../../../theme/@coremat/CmtCard/CmtCardHeader';
import CmtList from '../../../../../theme/@coremat/CmtList';

let firstLoad = true;
const WaLanguage = {};

const LanguageSwitcher = props => {
  const classes = useStyles(props);
  const [anchorEl, setAnchorEl] = useState(null);
  const theme = useTheme();

  const locale = useLocale();
  const setLocale = useSetLocale();
  const translate = useTranslate();
  const { setLanguage } = useLanguageContext();
  const refresh = useRefresh();

  const { setLocalStorage } = useLocalStorage('waLocale');

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'language' : undefined;

  const switchLanguage = language => {
    setLocale(language.locale);
    setLocalStorage(language.locale);
    handleClose();
  };

  const {
    data, error,
  } = useGetList('language', {
    page: 1,
    perPage: 100,
  });

  if (!error && data) {
    Object.keys(data).forEach(langId => {
      WaLanguage[data[langId].locale] = data[langId];
    });
  }

  useEffect(() => {
    const text = translate('ra.boolean.true');
    if (text === '' && firstLoad && locale) {
      firstLoad = false;
      setLocale(locale);
    }
  });

  useEffect(() => {
    if (WaLanguage?.[locale]) {
      setLanguage(WaLanguage?.[locale]);
      moment.locale(locale);
    }
  }, [WaLanguage?.[locale]]);

  // Refresh to change languages for report modules
  useEffect(() => {
    refresh();
  }, [locale]);

  return (
    <>
      <IconButton
        size="small"
        onClick={handleClick}
        data-tut="reactour__localization"
      >
        <i
          className={`flag flag-24 flag-${WaLanguage?.[locale]?.icon || 'us'}`}
        />
      </IconButton>

      <Popover
        className={classes.popoverRoot}
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <CmtCard className={classes.cardRoot}>
          <CmtCardHeader
            title={translate('ra.switch.language')}
            separator={{
              color: theme.palette.borderColor.dark,
              borderWidth: 1,
              borderStyle: 'solid',
            }}
          />
          <PerfectScrollbar className={classes.perfectScrollbarLanguage}>
            <CmtList
              data={Object.values(WaLanguage)}
              renderRow={(item, index) => (
                <LanguageItem
                  key={index}
                  language={item}
                  onClick={switchLanguage}
                />
              )}
            />
          </PerfectScrollbar>
        </CmtCard>
      </Popover>
    </>
  );
};

export default LanguageSwitcher;

// const LanguageSwitcher = props => {
//   const { iconOnly } = props;
//   const locale = useLocale();
//   const setLocale = useSetLocale();
//   const translate = useTranslate();
//   const { setLanguage } = useLanguageContext();

//   const { setLocalStorage } = useLocalStorage('waLocale');
//   const [anchorElLng, setAnchorElLng] = React.useState(null);

//   const onClickMenu = event => {
//     setAnchorElLng(event.currentTarget);
//   };

//   const {
//     data, error,
//   } = useGetList('language', {
//     page: 1,
//     perPage: 100,
//   });

//   if (!error && data) {
//     Object.keys(data).forEach(langId => {
//       WaLanguage[data[langId].locale] = data[langId];
//     });
//   }

//   const changeLanguage = language => {
//     setLocale(language.locale);
//     setLocalStorage(language.locale);
//     setAnchorElLng(null);
//   };

//   useEffect(() => {
//     const text = translate('ra.boolean.true');
//     if (text === '' && firstLoad && locale) {
//       firstLoad = false;
//       setLocale(locale);
//     }
//   });

//   useEffect(() => {
//     if (WaLanguage?.[locale]) {
//       setLanguage(WaLanguage?.[locale]);
//       moment.locale(locale);
//     }
//   }, [WaLanguage?.[locale]]);

//   const classes = useStyles(props);

//   return (
//     <Box>
//       <IconButton
//         className={clsx(
//           classes.langBtn,
//           {
//             langIconOnly: iconOnly,
//           },
//           'langBtn',
//         )}
//         aria-label="account of current user"
//         aria-controls="language-switcher"
//         aria-haspopup="true"
//         onClick={onClickMenu}
//         color="inherit"
//       >
//         {!iconOnly ? (
//           <>
//             <Box
//               component="span"
//               mr={{
//                 xs: 2,
//                 md: 3,
//               }}
//               height={48}
//               width={48}
//               display="flex"
//               alignItems="center"
//               justifyContent="center"
//               borderRadius="50%"
//               className={classes.overflowHidden}
//             >
//               <i
//                 className={`flag flag-24 flag-${WaLanguage?.[locale]?.icon}`}
//               />
//             </Box>
//             <Box
//               component="span"
//               display="inline-block"
//               verticalalign="middle"
//             >
//               {WaLanguage?.[locale]?.name}
//             </Box>
//           </>
//         ) : (
//           <Box>
//             <i className={`flag flag-24 flag-${WaLanguage?.[locale]?.icon}`} />
//           </Box>
//         )}
//       </IconButton>
//       <Menu
//         anchorEl={anchorElLng}
//         id="language-switcher"
//         keepMounted
//         open={Boolean(anchorElLng)}
//         onClose={() => setAnchorElLng(null)}
//       >
//         {Object.values(WaLanguage).map(language => (
//           <MenuItem
//             key={language.locale}
//             onClick={() => {
//               changeLanguage(language);
//             }}
//           >
//             <Box
//               width={160}
//               display="flex"
//               flexDirection="row"
//               alignItems="center"
//             >
//               <i className={`flag flag-24 flag-${language.icon}`} />
//               <Box
//                 component="h4"
//                 ml={4}
//                 mb={0}
//                 mt={0}
//                 fontSize={{
//                   xs: 14,
//                   xl: 16,
//                 }}
//               >
//                 {language.name}
//               </Box>
//             </Box>
//           </MenuItem>
//         ))}
//       </Menu>
//     </Box>
//   );
// };

// export default LanguageSwitcher;

// LanguageSwitcher.defaultProps = {
//   iconOnly: false,
// };

// LanguageSwitcher.propTypes = {
//   iconOnly: PropTypes.bool,
// };
