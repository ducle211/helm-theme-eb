import React from 'react';
import Box from '@material-ui/core/Box';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import PropTypes from 'prop-types';
import { ListItem } from '@material-ui/core';

import useStyles from './notificationItem.style';

const NotificationItem = props => {
  const { item } = props;

  const classes = useStyles(props);
  return (
    <ListItem>
      <Box mr={4}>
        <ListItemAvatar className={classes.minWidth0}>
          <Avatar
            className={classes.avatar}
            alt="Remy Sharp"
            src={item.image}
          />
        </ListItemAvatar>
      </Box>
      <Box
        component="p"
        className={classes.textBase}
        color="grey.500"
      >
        <Box
          mr={2}
          component="span"
          display="inline-block"
          color="text.primary"
        >
          {item.name}
        </Box>
        {item.message}
      </Box>
    </ListItem>
  );
};

export default NotificationItem;

NotificationItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    message: PropTypes.string,
    image: PropTypes.string,
  }).isRequired,
};
