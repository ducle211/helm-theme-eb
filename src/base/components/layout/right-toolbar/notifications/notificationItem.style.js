import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  textBase: {
    fontSize: 16,
    [theme.breakpoints.up('xl')]: {
      fontSize: 18,
    },
  },
  avatar: {
    width: 40,
    height: 40,
    [theme.breakpoints.up('xl')]: {
      width: 60,
      height: 60,
    },
  },
  minWidth0: {
    minWidth: 0,
  },
}));

export default useStyles;
