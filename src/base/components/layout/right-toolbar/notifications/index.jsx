import React from 'react';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import clsx from 'clsx';
import { Popover,
  List,
  Box,
  Button,
  Hidden,
  Badge,
  IconButton } from '@material-ui/core';

// import IntlMessages from '../../utility/IntlMessages';
import NotificationItem from './notificationItem';
import useStyles from './index.style';

const notification = [
  {
    id: '1000',
    name: 'Aysha Julka',
    image: '/images/avatar/A1.jpg',
    message: 'commented on your profile picture.',
  },
  {
    id: '1001',
    name: 'Ayra Rovishi',
    image: '/images/avatar/A2.jpg',
    message: 'added to their stories.',
  },
  {
    id: '1002',
    name: 'Sapna Awasthi',
    image: '/images/avatar/A3.jpg',
    message: 'commented on your profile picture.',
  },
  {
    id: '1003',
    name: 'Sami Rudri',
    image: '/images/avatar/A4.jpg',
    message: 'tagged you in a picture.',
  },
  {
    id: '1004',
    name: 'Brian Lara',
    image: '/images/avatar/A5.jpg',
    message: 'marked himself safe during earth quake.',
  },
  {
    id: '1005',
    name: 'Rickey Ponting',
    image: '/images/avatar/A6.jpg',
    message: 'commented on your profile picture.',
  },
  {
    id: '1007',
    name: 'Smriti Mandhana',
    image: '/images/avatar/A8.jpg',
    message: 'changed her profile picture.',
  },
  {
    id: '1008',
    name: 'Aysha Julka',
    image: '/images/avatar/A9.jpg',
    message: 'changed her profile picture.',
  },
];

const Notifications = props => {
  const [anchorNotification, setAnchorNotification] = React.useState(null);

  const onClickNotificationButton = event => {
    setAnchorNotification(event.currentTarget);
  };

  const classes = useStyles(props);

  return (
    <>
      <IconButton
        className={clsx(classes.notiBtn, 'notiBtn')}
        aria-label="show 17 new notifications"
        color="inherit"
        onClick={onClickNotificationButton}
      >
        <Badge
          className={classes.badge}
          badgeContent={notification.length}
          color="secondary"
        >
          <NotificationsActiveIcon
            className={clsx(classes.notiIcon, 'notiIcon')}
          />
        </Badge>
        <Hidden mdUp>
          <Box
            ml={4}
            fontSize={16}
            color="text.secondary"
            component="span"
          >
            {/* <IntlMessages id='common.notifications' /> */}
            Notifications
          </Box>
        </Hidden>
      </IconButton>

      <Popover
        anchorEl={anchorNotification}
        id="language-switcher"
        className={classes.crPopover}
        keepMounted
        open={Boolean(anchorNotification)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        onClose={() => setAnchorNotification(null)}
      >
        <Box>
          <Box
            px={5}
            py={3}
          >
            <Box component="h5">
              {/* <IntlMessages id="common.notifications" />({notification.length}) */}
              Notications(8)
            </Box>
          </Box>
          <Box className="scroll-submenu">
            <List
              className={classes.list}
              onClick={() => {
                setAnchorNotification(null);
              }}
            >
              {notification.map(item => (
                <NotificationItem
                  listStyle={classes.notificationItem}
                  key={item.id}
                  item={item}
                />
              ))}
            </List>
          </Box>
          <Box mt={2}>
            <Button
              className={classes.btnPopover}
              variant="contained"
              color="primary"
            >
              {/* <IntlMessages id="common.viewAll" /> */}
              View all
            </Button>
          </Box>
        </Box>
      </Popover>
    </>
  );
};

export default Notifications;
