import { Box, IconButton, Menu, MenuItem } from '@material-ui/core';
import { MoreVert as MoreIcon, RefreshSharp } from '@material-ui/icons';
import clsx from 'clsx';
import * as React from 'react';
import { useRefresh } from 'react-admin';
import useStyles from './index.style';
import LanguageSwitcher from './language-switcher';

const RightToolbar = () => {
  const classes = useStyles();
  const refresh = useRefresh();
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }

  function handleMobileMenuOpen(event) {
    setMobileMoreAnchorEl(event.currentTarget);
  }

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem className={classes.menuItemRoot}>
        <LanguageSwitcher />
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <Box className={classes.sectionDesktop}>
        <LanguageSwitcher />
        <IconButton
          aria-label="Refresh"
          aria-haspopup="true"
          onClick={refresh}
          color="default"
          className={classes.margin}
        >
          <RefreshSharp />
        </IconButton>
      </Box>
      <Box className={classes.sectionMobile}>
        <IconButton
          aria-label="Refresh"
          aria-haspopup="true"
          onClick={refresh}
          className={clsx(classes.iconMobile, classes.margin)}
          color="default"
        >
          <RefreshSharp />
        </IconButton>
        <IconButton
          aria-label="show more"
          className={clsx(classes.iconMobile)}
          aria-controls={mobileMenuId}
          aria-haspopup="true"
          onClick={handleMobileMenuOpen}
          color="default"
        >
          <MoreIcon />
        </IconButton>
      </Box>
      {renderMobileMenu}
    </>
  );
};

export default RightToolbar;
