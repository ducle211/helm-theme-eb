import React from 'react';
import { ReferenceInput as RaReferenceInput } from 'react-admin';
import { useForm } from 'react-final-form';
import PropTypes from 'prop-types';

const ReferenceInput = ({
  children,
  onChange: onChangeOrigin,
  afterOnChange,
  ...rest
}) => {
  const form = useForm();

  return (
    <RaReferenceInput
      {...rest}
      onChange={event => {
        onChangeOrigin(event);
        afterOnChange(event, form);
      }}
    >
      {children}
    </RaReferenceInput>
  );
};

ReferenceInput.propTypes = {
  onChange: PropTypes.func,
  afterOnChange: PropTypes.func,
  children: PropTypes.node,
};

ReferenceInput.defaultProps = {
  onChange: f => f,
  afterOnChange: f => f,
  children: null,
};

export default ReferenceInput;
