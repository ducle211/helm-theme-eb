import '@toast-ui/editor/dist/toastui-editor.css';
import Editor from 'react-simple-code-editor';
import 'codemirror/lib/codemirror.css';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useInput } from 'react-admin';
import { FormControl, FormHelperText } from '@material-ui/core';
import { highlight, languages } from 'prismjs/components/prism-core';
import 'prismjs/components/prism-clike';
import 'prismjs/components/prism-javascript';
import 'prismjs/themes/prism-coy.css';

const CodeEditorInput = props => {
  const {
    record, helperText, placeholder,
  } = props;
  const [value, setValue] = useState(record?.gameConfig || '');
  const { input: { onChange } } = useInput(props);

  useEffect(() => {
    onChange(value);
  }, [value]);

  // How to use
  // <CodeEditorInput source="gameConfig" />

  return (
    <FormControl>
      <Editor
        value={value}
        onValueChange={v => setValue(v)}
        highlight={v => highlight(v, languages.js)}
        padding={10}
        style={{
          fontFamily: '"Fira code", "Fira Mono", monospace',
          fontSize: 12,
          border: 'solid 1px black',
          borderRadius: '4px',
        }}
        placeholder={placeholder}
      />
      <FormHelperText>{helperText}</FormHelperText>
    </FormControl>
  );
};

CodeEditorInput.propTypes = {
  record: PropTypes.object,
  placeholder: PropTypes.string,
  helperText: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
};

CodeEditorInput.defaultProps = {
  record: {},
  placeholder: '',
  helperText: '',
};

export default CodeEditorInput;
