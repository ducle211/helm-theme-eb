import {
  AutocompleteInput as RaAutocompleteInput,
  AutocompleteArrayInput as RaAutocompleteArrayInput,
  TextInput as RaTextInput,
  NumberInput as RaNumberInput,
  PasswordInput as RaPasswordInput,
  DateInput as RaDateInput,
  DateTimeInput as RaDateTimeInput,
  SelectInput as RaSelectInput,
  SelectArrayInput as RaSelectArrayInput,
  NullableBooleanInput as RaNullableBooleanInput,
} from 'react-admin';
import { withStandardInputProps } from '../../../hoc/with-standard-input-props';

export const AutocompleteInput = withStandardInputProps(RaAutocompleteInput);
export const AutocompleteArrayInput = withStandardInputProps(
  RaAutocompleteArrayInput,
);
export const TextInput = withStandardInputProps(RaTextInput);
export const NumberInput = withStandardInputProps(RaNumberInput);
export const PasswordInput = withStandardInputProps(RaPasswordInput);
export const DateInput = withStandardInputProps(RaDateInput);
export const DateTimeInput = withStandardInputProps(RaDateTimeInput);
export const SelectInput = withStandardInputProps(RaSelectInput);
export const SelectArrayInput = withStandardInputProps(RaSelectArrayInput);
export const NullableBooleanInput = withStandardInputProps(RaNullableBooleanInput);

export { default as TuiEditorInput } from './tuiEditor.input';
export { default as BooleanInput } from './boolean.input';
export { default as Checkbox } from './checkbox.input';
export { default as CodeEditorInput } from './codeEditor.input';
export { default as PrizeTableInput } from './prizeTable.input';
export { default as CheckBoxGroup } from './checkBoxGroup.input';
export { default as IntegerInput } from './integer.input';
export { default as JsonInput } from './json.input';
