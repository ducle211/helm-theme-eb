import React, { useEffect } from 'react';
import {
  FormControl,
  FormLabel,
  FormGroup,
  FormControlLabel,
  FormHelperText,
  Checkbox,
  Card,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useInput, useTranslate } from 'react-admin';
import queryString from 'querystring';
import { useHistory } from 'react-router-dom';
import { startCase } from 'lodash';

function CheckBoxGroup(props) {
  const history = useHistory();
  const translate = useTranslate();
  const {
    choices = [], resource, source, label,
  } = props;

  const {
    input: {
      onChange, value,
    },
  } = useInput(props);

  const paramsFilter = useSelector(
    state => state.admin.resources[resource]?.list?.params,
  );

  // Display based on url
  useEffect(() => {
    const { filter } = queryString.parse(history.location.search.substring(1)) || {};
    const objFilter = filter ? JSON.parse(filter) : {};
    const paramValues = objFilter?.[source] || paramsFilter?.filter?.[source];

    if (paramValues) {
      onChange(paramValues?.split([',']));
    }
  }, []);

  function handleToggle(e) {
    if (value?.indexOf(e.target.name) === -1) {
      onChange([...value, e.target.name]);
    } else {
      onChange([...value?.filter(v => v !== e.target.name)]);
    }
  }

  return (
    <FormControl
      component="fieldset"
      margin="dense"
    >
      <Card
        variant="outlined"
        style={{
          boxShadow: 'unset',
          padding: '10px',
          paddingBottom: 0,
        }}
      >
        <FormLabel
          component="legend"
          style={{
            marginBottom: '16px',
          }}
        />
        {startCase(translate(label))}
        <FormGroup>
          {choices?.map(choice => (
            <FormControlLabel
              key={choice.id}
              control={(
                <Checkbox
                  checked={value?.includes(choice.id)}
                  onChange={handleToggle}
                  name={choice.id}
                />
              )}
              label={startCase(translate(choice.name))}
            />
          ))}
        </FormGroup>
        <FormHelperText />
      </Card>
    </FormControl>
  );
}

CheckBoxGroup.propTypes = {
  choices: PropTypes.array.isRequired,
  resource: PropTypes.string.isRequired,
  source: PropTypes.string.isRequired,
  label: PropTypes.string,
};

CheckBoxGroup.defaultProps = {
  label: '',
};

export default CheckBoxGroup;
