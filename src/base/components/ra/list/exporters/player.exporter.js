import { downloadCSV } from 'react-admin';
import jsonExport from 'jsonexport/dist';

const playerExporter = async (
  records,
  fetchRelatedRecords,
) => {
  const groups = await fetchRelatedRecords(records, 'groupId', 'group');
  const brands = await fetchRelatedRecords(records, 'brandId', 'brand');

  const playersForExport = records.map(p => ({
    id: p.id,
    group: groups[p.groupId]?.name,
    brand: brands[p.brandId]?.name,
    playerId: p.playerId,
    nativeId: p.nativeId,
    type: p.typeOfPlayer,
    country: p.country?.name,
    currency: p.currency?.name,
    created: p.created,
    updated: p.updated,
  }));
  jsonExport(playersForExport, {}, (err, csv) => {
    downloadCSV(csv, 'Players');
  });
};

export default playerExporter;
