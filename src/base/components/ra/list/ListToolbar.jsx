/* eslint-disable react/require-default-props */
import * as React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Toolbar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(
  theme => ({
    toolbar: {
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'flex-start',
      paddingRight: 0,
      [theme.breakpoints.up('xs')]: {
        paddingLeft: 0,
      },
      [theme.breakpoints.down('xs')]: {
        paddingLeft: theme.spacing(2),
        backgroundColor: theme.palette.background.paper,
      },
    },
    actions: {
      paddingTop: theme.spacing(3),
      minHeight: theme.spacing(5),
      [theme.breakpoints.down('xs')]: {
        padding: theme.spacing(1),
        backgroundColor: theme.palette.background.paper,
      },
    },
  }),
  {
    name: 'RaListToolbar',
  },
);

const ListToolbar = props => {
  const {
    filters, actions, searchPanel, isDisplayFilterPanel: isDisplayFilterPanelOrigin, ...rest
  } = props;

  const [isDisplayFilterPanel, setIsDisplayFilterPanel] = useState(isDisplayFilterPanelOrigin);
  const classes = useStyles(props);

  const toggleFilterPanel = () => {
    setIsDisplayFilterPanel(prevState => !prevState);
  };

  return (
    <Toolbar className={classes.toolbar}>
      {actions
        && React.cloneElement(actions, {
          ...rest,
          className: classes.actions,
          filters,
          ...actions.props,
          toggleFilterPanel,
          isDisplayFilterPanel,
        })}

      {/* searchPanel of report list guesser */}
      {searchPanel && searchPanel}
      {filters && isDisplayFilterPanel
        && React.cloneElement(filters, {
          ...rest,
          context: 'form',
        })}
      <span />
    </Toolbar>
  );
};

ListToolbar.propTypes = {
  classes: PropTypes.object,
  filters: PropTypes.element,
  // @ts-ignore
  actions: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  // @ts-ignore
  exporter: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  searchPanel: PropTypes.element,
  isDisplayFilterPanel: PropTypes.bool,
};

export default React.memo(ListToolbar);
