/* eslint-disable */
import * as React from "react";
import PropTypes from "prop-types";
import {
  useCheckMinimumRequiredProps,
  useListController,
  ListContextProvider,
} from "ra-core";

import { TitlePropType } from "ra-ui-materialui/lib/layout/Title";

import ListView from "./ListView";

import useLoading from "../../../hooks/useLoading";
const List = ({ searchPanel, ...props}) => {
  useCheckMinimumRequiredProps("List", ["children"], props);
  const controllerProps = useListController(props);
  const { toggleLoading } = useLoading();

  React.useEffect(() => {
    toggleLoading(controllerProps.loading);
    return () => toggleLoading(false);
  }, [controllerProps.loading]);

  return (
    <ListContextProvider value={controllerProps}>
      <ListView {...props} {...controllerProps} searchPanel={searchPanel} />
    </ListContextProvider>
  );
};

List.propTypes = {
  // the props you can change
  // @ts-ignore-line
  actions: PropTypes.oneOfType([PropTypes.bool, PropTypes.element]),
  aside: PropTypes.element,
  // @ts-ignore-line
  bulkActionButtons: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
  children: PropTypes.element,
  classes: PropTypes.object,
  className: PropTypes.string,
  filter: PropTypes.object,
  filterDefaultValues: PropTypes.object,
  filters: PropTypes.element,
  // @ts-ignore-line
  pagination: PropTypes.oneOfType([PropTypes.element, PropTypes.bool]),
  perPage: PropTypes.number.isRequired,
  // @ts-ignore-line
  sort: PropTypes.shape({
    field: PropTypes.string,
    order: PropTypes.string,
  }),
  title: TitlePropType,
  // the props managed by react-admin
  authProvider: PropTypes.func,
  hasCreate: PropTypes.bool.isRequired,
  hasEdit: PropTypes.bool.isRequired,
  hasList: PropTypes.bool.isRequired,
  hasShow: PropTypes.bool.isRequired,
  location: PropTypes.any,
  match: PropTypes.any,
  path: PropTypes.string,
  resource: PropTypes.string.isRequired,
  isDisplayFilterPanel: PropTypes.bool,
};

List.defaultProps = {
  filter: {},
  perPage: 10,
  isDisplayFilterPanel: false,
};

export default List;
