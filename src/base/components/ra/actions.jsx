/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import { TopToolbar, ListButton, EditButton, ShowButton } from 'react-admin';

const Actions = ({
  basePath, data, hasEdit, hasList, hasShow,
}) => (
  <TopToolbar>
    {hasList && (
      <ListButton
        basePath={basePath}
      />
    )}

    {hasShow && (
      <ShowButton
        basePath={basePath}
        record={data}
      />
    )}

    {hasEdit && (
      <EditButton
        basePath={basePath}
        record={data}
      />
    )}
  </TopToolbar>
);

Actions.propTypes = {
  basePath: PropTypes.string,
  data: PropTypes.object,
  hasEdit: PropTypes.bool,
  hasList: PropTypes.bool,
  hasShow: PropTypes.bool,
};

export default Actions;
