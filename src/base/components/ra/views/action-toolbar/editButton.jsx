/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import ContentCreate from '@material-ui/icons/Create';
import { Link, useLocation } from 'react-router-dom';
import { linkToRecord } from 'ra-core';
import { Button } from 'react-admin';

const defaultIcon = <ContentCreate />;
const stopPropagation = e => e.stopPropagation();

function EditButton({
  basePath = '',
  label = 'ra.action.edit',
  record,
  icon = defaultIcon,
  ...rest
}) {
  const { pathname } = useLocation();
  const tab = pathname.split('/').pop();
  let to = `${linkToRecord(basePath, record?.id)}`;
  if (tab !== 'show') to = `${linkToRecord(basePath, record?.id)}/${tab}`;
  return (
    <Button
      component={Link}
      to={to}
      label={label}
      onClick={stopPropagation}
      {...rest}
    >
      {icon}
    </Button>
  );
}

EditButton.propTypes = {
  basePath: PropTypes.string,
  icon: PropTypes.element,
  label: PropTypes.string,
  record: PropTypes.any,
};

export default EditButton;
