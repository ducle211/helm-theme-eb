/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import ImageEye from '@material-ui/icons/RemoveRedEye';
import { Link, useLocation } from 'react-router-dom';
import { linkToRecord } from 'ra-core';
import { Button } from 'react-admin';

const defaultIcon = <ImageEye />;
// useful to prevent click bubbling in a datagrid with rowClick
const stopPropagation = e => e.stopPropagation();

function ShowButton({
  basePath = '',
  label = 'ra.action.show',
  record,
  icon = defaultIcon,
  ...rest
}) {
  const { pathname } = useLocation();
  const tab = pathname.split('/').pop();
  let to = `${linkToRecord(basePath, record?.id)}/show`;
  if (tab !== record?.id) to = `${linkToRecord(basePath, record?.id)}/show/${tab}`;
  return (
    <Button
      component={Link}
      to={to}
      label={label}
      onClick={stopPropagation}
      {...rest}
    >
      {icon}
    </Button>
  );
}

ShowButton.propTypes = {
  basePath: PropTypes.string,
  icon: PropTypes.element,
  label: PropTypes.string,
  record: PropTypes.any,
};

const PureShowButton = memo(
  ShowButton,
  (props, nextProps) => (props.record && nextProps.record
    ? props.record.id === nextProps.record.id
    : props.record == nextProps.record) && // eslint-disable-line
    props.basePath === nextProps.basePath,
);

export default PureShowButton;
