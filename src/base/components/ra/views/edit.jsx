import Type from 'prop-types';
import React from 'react';
import { Edit as RAEdit, getResources } from 'react-admin';
import { useTranslate } from 'ra-core';
import { useSelector } from 'react-redux';
import { WealthToolbar } from '../../guesser/wealth-toolbar';
import { useError } from '../../../hooks';
import Actions from './action-toolbar';

const Edit = props => {
  const resources = useSelector(getResources);
  const {
    resource,
    children,
    alwaysEnableSaveButton, // eslint-disable-line react/prop-types
    onCustomSave,
  } = props;

  const { notifyError } = useError();
  const translate = useTranslate();
  const { options: { hasDelete } } = resources.find(r => r.name === resource);

  const onFailure = error => {
    notifyError(error);
  };

  const translatedResourceName = translate(`resources.${resource}.name`);
  const successMessage = translate('ra.notification.updated', {
    smart_name: translatedResourceName,
  });

  return (
    <RAEdit
      {...props}
      undoable={false}
      onFailure={onFailure}
      successMessage={successMessage}
    >
      {React.cloneElement(children, {
        toolbar: (
          <WealthToolbar
            hasDelete={hasDelete}
            onCustomSave={onCustomSave}
            alwaysEnableSaveButton={alwaysEnableSaveButton}
          />
        ),
      })}
    </RAEdit>
  );
};

Edit.propTypes = {
  resource: Type.string,
  children: Type.node,
  actions: Type.node,
  onCustomSave: Type.func,
};

Edit.defaultProps = {
  resource: '',
  children: <></>,
  actions: <Actions />,
  onCustomSave: undefined,
};

export default Edit;
