/* eslint-disable no-param-reassign */
import React from 'react';
import {
  useCreateController,
  useRedirect,
  Create as RACreate,
} from 'react-admin';
import Type from 'prop-types';
import useBulk from '../../../hooks/useBulk';
import { useError } from '../../../hooks';
import { WealthToolbar } from '../../guesser/wealth-toolbar';
import Actions from './action-toolbar';

const Create = props => {
  const {
    children,
    redirect: redirectPath,
    writableFields,
    disabledBtnSave, // eslint-disable-line react/prop-types
    alwaysEnableSaveButton,
    onCustomSave,
    ...rest
  } = props;
  const { basePath } = useCreateController(props);
  const {
    isClonable,
    completeCloningCurrentRecord,
  } = useBulk(writableFields);
  const redirect = useRedirect();
  const { notifyError } = useError();

  const onSuccess = ({ data }) => {
    if (isClonable()) {
      completeCloningCurrentRecord();
      return;
    }
    redirect(redirectPath, basePath, data.id, data);
  };

  const onFailure = error => {
    notifyError(error);
  };

  return (
    <RACreate
      {...rest}
      onSuccess={onSuccess}
      onFailure={onFailure}
    >
      {React.cloneElement(children, {
        toolbar: (
          <WealthToolbar
            disabledBtnSave={disabledBtnSave}
            onCustomSave={onCustomSave}
            alwaysEnableSaveButton={alwaysEnableSaveButton}
          />
        ),
      })}
    </RACreate>
  );
};

Create.propTypes = {
  children: Type.oneOfType([Type.element, Type.arrayOf(Type.element)])
    .isRequired,
  actions: Type.oneOfType([Type.element, Type.arrayOf(Type.element)]),
  redirect: Type.string, // "edit"|"show"|"list"|<custom route>
  writableFields: Type.array,
  onCustomSave: Type.func,
  alwaysEnableSaveButton: Type.bool,
};

Create.defaultProps = {
  actions: <Actions />,
  redirect: 'show',
  writableFields: [],
  onCustomSave: undefined,
  alwaysEnableSaveButton: undefined,
};

export default Create;
