/* eslint-disable */
import { TableCell, TableSortLabel, Tooltip } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import classnames from "classnames";
import PropTypes from "prop-types";
import { FieldTitle, useResourceContext, useTranslate } from "ra-core";
import React, { memo } from "react";

const doNoThing = () => {};

// remove the sort icons when not active
const useStyles = makeStyles(
  {
    icon: {
      display: "none",
    },
    active: {
      "& $icon": {
        display: "inline",
      },
    },
  },
  { name: "RaDatagridHeaderCell" }
);

export const DatagridHeaderCell = (props) => {
  const {
    className,
    classesObj: classesOverride,
    field,
    currentSort,
    updateSort,
    disableSort,
    ...rest
  } = props;
  const resource = useResourceContext(props);
  const classes = useStyles(props);
  const translate = useTranslate();
  const FieldTitleRef = React.forwardRef((props, ref) => (
    <FieldTitle
      innerRef={ref}
      label={field.props.label}
      source={field.props.source}
      resource={resource}
    />
  ));
  return (
    <TableCell
      className={classnames(className, field.props.headerClassName)}
      align={field.props.textAlign}
      variant="head"
      {...rest}
    >
      <Tooltip
        title={translate(field.props.tooltip || field.props.label || "")}
        placement="top-start"
        disableHoverListener={!field.props.tooltip && !field.props.label}
      >
        {field.props.sortable !== false &&
        (field.props.sortBy || field.props.source) ? (
          <TableSortLabel
            active={
              currentSort.field === (field.props.sortBy || field.props.source)
            }
            direction={currentSort.order === "ASC" ? "asc" : "desc"}
            data-sort={field.props.sortBy || field.props.source} // @deprecated. Use data-field instead.
            data-field={field.props.sortBy || field.props.source}
            data-order={field.props.sortByOrder || "ASC"}
            onClick={disableSort ? doNoThing : updateSort }
            classes={classes}
          >
            <FieldTitle
              label={field.props.label}
              source={field.props.source}
              resource={resource}
            />
          </TableSortLabel>
        ) : (
          <FieldTitleRef />
        )}
      </Tooltip>
    </TableCell>
  );
};

DatagridHeaderCell.propTypes = {
  className: PropTypes.string,
  classesObj: PropTypes.object,
  field: PropTypes.element,
  currentSort: PropTypes.shape({
    sort: PropTypes.string,
    order: PropTypes.string,
  }).isRequired,
  disableSort: PropTypes.bool,
  resource: PropTypes.string,
  updateSort: PropTypes.func.isRequired,
};

export default memo(
  DatagridHeaderCell,
  (props, nextProps) =>
    props.updateSort === nextProps.updateSort &&
    props.currentSort.field === nextProps.currentSort.field &&
    props.currentSort.order === nextProps.currentSort.order &&
    props.resource === nextProps.resource &&
    props.disableSort === nextProps.disableSort
);
