/* eslint-disable */
import React from "react";
import { isValidElement, Children, cloneElement, useCallback } from "react";
import PropTypes from "prop-types";
import { sanitizeListRestProps, useListContext, useVersion } from "ra-core";
import {
  Checkbox,
  Table,
  TableCell,
  TableHead,
  TableRow,
} from "@material-ui/core";
import classnames from "classnames";
import { DatagridLoading, DatagridBody, PureDatagridBody } from "react-admin";
import { get, isEmpty } from "lodash";
import DatagridHeaderCell from "./DatagridHeaderCell";
import useStyles from "./index.style";
import EmptyTable from "../../guesser/report/emptyTable";

const Datagrid = React.forwardRef((props, ref) => {
  const classes = useStyles(props);
  const {
    optimized = false,
    body = optimized ? (
      <PureDatagridBody classes={classes} />
    ) : (
      <DatagridBody classes={classes} />
    ),
    children,
    classes: classesOverride,
    className,
    expand,
    hasBulkActions = false,
    hover,
    isRowSelectable,
    resource,
    rowClick,
    rowStyle,
    size = "small",
    refManyEditAddButton,
    defaultColumns,
    ...rest
  } = props;

  const {
    basePath,
    currentSort,
    data,
    ids,
    loaded,
    onSelect,
    onToggleItem,
    selectedIds,
    setSort,
    total,
  } = useListContext(props);
  const version = useVersion();

  const updateSort = useCallback(
    (event) => {
      event.stopPropagation();
      const newField = event.currentTarget.dataset.field;
      const newOrder =
        currentSort.field === newField
          ? currentSort.order === "ASC"
            ? "DESC"
            : "ASC"
          : event.currentTarget.dataset.order;

      setSort(newField, newOrder);
    },
    [currentSort.field, currentSort.order, setSort]
  );

  const handleSelectAll = useCallback(
    (event) => {
      if (event.target.checked) {
        const all = ids.concat(selectedIds.filter((id) => !ids.includes(id)));
        onSelect(
          isRowSelectable ? all.filter((id) => isRowSelectable(data[id])) : all
        );
      } else {
        onSelect([]);
      }
    },
    [data, ids, onSelect, isRowSelectable, selectedIds]
  );

  /**
   * Depend on `field` to get real source name
   * @param {*} field 
   * @returns source or null
   */
  const getRealSourceName = (field) => {
    const reference = get(field, 'props.reference');
    const source = get(field, 'props.source');
    
    if (reference && reference?.split('.')?.length > 1) {
      return reference;
    }

    if (source) {
      return source;
    }

    return null;
  };

  /**
   * Purpose: Check a field which is a param
   * if type of this field is Array (Many-to-many), we DO NOT perform sort function
   */
  const checkDisableSort = (field) => {
    if (!data || isEmpty(data) || Object.values(data).length === 0) {
      return false;
    }

    const realSourceName = getRealSourceName(field);
    if (!realSourceName) {
      return false;
    }

    // This case to prevent filter feature
    const firstRecord = Object.values(data)[0];
    if (Array.isArray(firstRecord[realSourceName])) {
      return true;
    }

    return false;
  };

  /**
   * if loaded is false, the list displays for the first time, and the dataProvider hasn't answered yet
   * if loaded is true, the data for the list has at least been returned once by the dataProvider
   * if loaded is undefined, the Datagrid parent doesn't track loading state (e.g. ReferenceArrayField)
   */
  if (loaded === false) {
    return (
      <DatagridLoading
        classes={classes}
        className={className}
        expand={expand}
        hasBulkActions={hasBulkActions}
        nbChildren={React.Children.count(children)}
        size={size}
      />
    );
  }

  /**
   * Once loaded, the data for the list may be empty. Instead of
   * displaying the table header with zero data rows,
   * the datagrid displays nothing in this case.
   */
  if (loaded && (ids.length === 0 || total === 0)) {
    return <EmptyTable />;
  }

  const all = isRowSelectable
    ? ids.filter((id) => isRowSelectable(data[id]))
    : ids;

  /**
   * After the initial load, if the data for the list isn't empty,
   * and even if the data is refreshing (e.g. after a filter change),
   * the datagrid displays the current data.
   */
  return (
    <Table
      ref={ref}
      className={classnames(classes.table, className)}
      size={size}
      {...sanitizeListRestProps(rest)}
      stickyHeader
    >
      <TableHead className={classes.thead}>
        <TableRow className={classnames(classes.row, classes.headerRow)}>
          {expand && (
            <TableCell
              padding="none"
              className={classnames(classes.headerCell, classes.expandHeader)}
            />
          )}
          {hasBulkActions && (
            <TableCell padding="checkbox" className={classes.headerCell}>
              <Checkbox
                className="select-all"
                color="primary"
                checked={
                  selectedIds.length > 0 &&
                  all.length > 0 &&
                  all.every((id) => selectedIds.includes(id))
                }
                onChange={handleSelectAll}
              />
            </TableCell>
          )}
          {Children.map(children, (field, index) =>
            isValidElement(field) ? (
              <DatagridHeaderCell
                className={classes.headerCell}
                classesObj={classes}
                currentSort={currentSort}
                field={field}
                disableSort={checkDisableSort(field)}
                key={field.props.source || index}
                resource={resource}
                updateSort={updateSort}
              />
            ) : null
          )}
        </TableRow>
      </TableHead>
      {cloneElement(
        body,
        {
          basePath,
          className: classes.tbody,
          expand,
          rowClick,
          data,
          hasBulkActions,
          hover,
          ids,
          onToggleItem,
          resource,
          rowStyle,
          selectedIds,
          isRowSelectable,
          version,
        },
        children
      )}
    </Table>
  );
});

Datagrid.propTypes = {
  basePath: PropTypes.string,
  body: PropTypes.element,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object,
  className: PropTypes.string,
  currentSort: PropTypes.shape({
    field: PropTypes.string,
    order: PropTypes.string,
  }),
  data: PropTypes.object,
  // @ts-ignore
  expand: PropTypes.oneOfType([PropTypes.element, PropTypes.elementType]),
  hasBulkActions: PropTypes.bool,
  hover: PropTypes.bool,
  ids: PropTypes.arrayOf(PropTypes.any),
  loading: PropTypes.bool,
  onSelect: PropTypes.func,
  onToggleItem: PropTypes.func,
  resource: PropTypes.string,
  rowClick: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  rowStyle: PropTypes.func,
  selectedIds: PropTypes.arrayOf(PropTypes.any),
  setSort: PropTypes.func,
  total: PropTypes.number,
  version: PropTypes.number,
  isRowSelectable: PropTypes.func,
};

export default Datagrid;
