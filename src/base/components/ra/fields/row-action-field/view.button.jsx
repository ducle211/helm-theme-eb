/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/require-default-props */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { Tooltip, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from 'react-router-dom';
import { linkToRecord } from 'ra-core';
import classnames from 'classnames';
import { useTranslate } from 'react-admin';

const useStyles = makeStyles(() => ({
  viewButton: {
    color: '#228B22',
    '&:hover': {
      backgroundColor: fade('#228B22', 0.12),
      // Reset on mouse devices
      '@media (hover: none)': {
        backgroundColor: 'transparent',
      },
    },
  },
}));

const defaultIcon = <SearchIcon />;
// useful to prevent click bubbling in a datagrid with rowClick
const stopPropagation = e => e.stopPropagation();

function ViewButton({
  basePath = '',
  record,
  icon = defaultIcon,
  className,
  ...rest
}) {
  const style = useStyles();
  const translate = useTranslate();
  const to = `${linkToRecord(basePath, record?.id)}/show`;
  return (
    <Tooltip title={translate('ra.action.viewDetail')}>
      <IconButton
        component={Link}
        to={to}
        onClick={stopPropagation}
        className={classnames(className, style.viewButton)}
        {...rest}
      >
        {icon}
      </IconButton>
    </Tooltip>
  );
}

ViewButton.propTypes = {
  basePath: PropTypes.string,
  icon: PropTypes.element,
  label: PropTypes.string,
  record: PropTypes.any,
  className: PropTypes.any,
};

const PureViewButton = memo(
  ViewButton,
  (props, nextProps) => (props.record && nextProps.record
    ? props.record.id === nextProps.record.id
      : props.record == nextProps.record) && // eslint-disable-line
    props.basePath === nextProps.basePath,
);

export default PureViewButton;
