import React from 'react';
import PropTypes from 'prop-types';
import DeleteButton from './delete-button/delete.button';

function RowActionField({
  hasDelete, extraActions, ...props
}) {
  return (
    <>
      {hasDelete && <DeleteButton {...props} />}
      {extraActions && React.cloneElement(extraActions, props)}
    </>
  );
}

RowActionField.propTypes = {
  record: PropTypes.object,
  label: PropTypes.string,
  hasDelete: PropTypes.bool,
  extraActions: PropTypes.element,
};

RowActionField.defaultProps = {
  record: {},
  label: 'Action',
  hasDelete: false,
  extraActions: null,
};

export default RowActionField;
