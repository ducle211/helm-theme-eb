import * as React from 'react';
import { Box, Link } from '@material-ui/core';
import PropTypes from 'prop-types';

const IdField = props => {
  const {
    record, source = 'id', hasShow, resource,
  } = props;

  const id = record?.[source] || record.record?.[source];
  const content = id ? `#${id?.slice(0, 4)}` : 'N/A';

  return (
    <Box>
      {(hasShow && resource) ? (
        <Link
          href={`/#/${resource}/${id}/show`}
        >
          {content}
        </Link>
      ) : content}
    </Box>
  );
};

IdField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string,
  hasShow: PropTypes.bool,
  resource: PropTypes.string,
};
IdField.defaultProps = {
  record: {},
  source: 'id',
  hasShow: false,
  resource: undefined,
};

export default IdField;
