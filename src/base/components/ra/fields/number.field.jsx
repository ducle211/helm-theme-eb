import React from 'react';
import PropTypes from 'prop-types';
import { NumberField as RANumberField } from 'react-admin';

const NumberField = props => {
  const {
    record, source, currency, ...rest
  } = props;

  if (!record[source]) {
    record[source] = 0;
  }

  return (
    <RANumberField
      {...rest}
      record={record}
      source={source}
      {...(!!currency && {
        options: {
          style: 'currency',
          currency,
        },
      })}
    />
  );
};

NumberField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  currency: PropTypes.string,
  addLabel: PropTypes.bool,
};

NumberField.defaultProps = {
  record: {},
  currency: undefined,
  addLabel: true,
};

export default NumberField;
