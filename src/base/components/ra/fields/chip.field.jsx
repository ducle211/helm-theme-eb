import React from 'react';
import PropTypes from 'prop-types';
import { ChipField as RAChipField } from 'react-admin';

const ChipField = props => {
  const {
    record, source,
  } = props;

  if (!record[source]) return null;

  return <RAChipField {...props} />;
};

ChipField.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
};

ChipField.defaultProps = {
  record: {},
};

export default ChipField;
