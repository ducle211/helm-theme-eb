import React from 'react';
import PropTypes from 'prop-types';
import NumberField from './number.field';

const CurrencyField = props => {
  const {
    record, currency, ...rest
  } = props;

  return (
    <NumberField
      {...rest}
      record={record}
      currency={currency || record.currency?.symbol || 'THB'}
    />
  );
};

CurrencyField.propTypes = {
  record: PropTypes.object,
  currency: PropTypes.string,
  addLabel: PropTypes.bool,
};

CurrencyField.defaultProps = {
  record: {},
  currency: undefined,
  addLabel: true,
};

export default CurrencyField;
