/* eslint-disable react/prop-types */
import * as React from 'react';
import Tooltip from '@material-ui/core/Tooltip';

const TooltipWrapper = React.forwardRef(({
  children, ...props
}, ref) => (
  <div ref={ref}>{React.cloneElement(children, props)}</div>
));

const CustomTooltip = ({
  // eslint-disable-next-line

  children,
  ...props
}) => (
  <Tooltip {...props}>
    <TooltipWrapper>{children}</TooltipWrapper>
  </Tooltip>
);

export default CustomTooltip;
