const express = require("express");
const path = require("path");
const { createProxyMiddleware } = require("http-proxy-middleware");
const app = express();

app.use(
  "/api",
  createProxyMiddleware({
    pathRewrite: { "^/api": "" },
    target: process.env.WEALTH_API_URL,
    changeOrigin: true,
  })
);

app.use(
  "/socket.io/",
  createProxyMiddleware({
    target: process.env.WEALTH_API_URL,
    changeOrigin: true,
    ws: true,
  })
);

app.use(express.static(path.join(__dirname, "..", "build")));
app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});
app.listen(parseInt(process.env.PORT, 10) || 3000);
